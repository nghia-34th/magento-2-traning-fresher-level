<?php

namespace Packt\HelloWorld\Model;

class Cron
{
    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    /** @var \Psr\Log\LoggerInterface $logger */
    protected $logger;

    /** @var \Magento\Framework\ObjectManagerInterface */
    protected $objectManger;

    public function checkSubscriptions(){
        $subscription = $this->objectManger->create('Packt\HelloWorld\Model\Subscription');

        $subscription->setFirstname('Cron');
        $subscription->setLastname('Job');
        $subscription->setEmail('cron.job@example.co');
        $subscription->setMessage('Created from cron');

        $subscription->save();

        $this->logger->debug('Test subscription added');
    }
}
