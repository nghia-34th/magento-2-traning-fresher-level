<?php

namespace Packt\HelloWorld\Controller\Index;

class Collection extends \Magento\Framework\App\Action\Action
{

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $productCollection = $this->_objectManager
            ->create('Magento\Catalog\Model\ResourceModel\Product\Collection')
            ->addAttributeToSelect([
                'name',
                'price',
                'image'
            ])
            ->addAttributeToFilter('name', ['like' => '%sport%']);

        $output = [];

        foreach ($productCollection as $product) {
            array_push($output, $product->debug());
        }

        dd($productCollection->getSelect()->__toString());
    }
}
