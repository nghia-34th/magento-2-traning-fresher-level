<?php

namespace Packt\HelloWorld\Controller\Index;

class Subscription extends \Magento\Framework\App\Action\Action
{

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $subscription = $this->_objectManager->create('Packt\HelloWorld\Model\Subscription');

        $subscription->setFirstname('John');
        $subscription->setLastname('Doe');
        $subscription->setEmail('john.doe@example.com');
        $subscription->setMessage('hello magento');

        $subscription->save();

        $this->getResponse()->setBody('success');
    }
}
