<?php

namespace Packt\HelloWorld\Controller\Index;

use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Event extends Action
{
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Product $product,
        Category $category)
    {
        $this->pageFactory = $pageFactory;
        $this->product = $product;
        $this->category = $category;
        parent::__construct($context);
    }

    public function execute()
    {
        $view = $this->pageFactory->create();

        //ADDITIONAL PARAMETER TO THE EVENT
        $data = [
            'product' => $this->product->load(50),
            'category' => $this->category->load(10)
        ];
        //FIRE THE EVENT
        $this->_eventManager->dispatch('helloworld_register_visit', $data);

        return $view;
    }
}
