<?php

namespace Magenest\CustomerAttribute\Setup;

use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $customerSetupFactory;

    public function __construct(CustomerSetupFactory $customerSetupFactory)
    {
        $this->customerSetupFactory = $customerSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        $customerSetup->addAttribute(Customer::ENTITY,
        'avatar', [
            'label' => 'Customer Avatar',
            'type' => 'varchar',
            'input' => 'image',
            'required' => false,
            'visible' => true,
            'position' => 10
        ]);
        $avatarAttribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'avatar');
        $avatarAttribute->setData('used_in_form', ['adminhtml_customer']);
        $avatarAttribute->save();

        $setup->endSetup();
    }
}
