<?php

namespace Magenest\CustomerAttribute\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class CustomerBeforeSave implements ObserverInterface
{
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function execute(Observer $observer)
    {
        $custom_attribute = $observer->getCustomer()->getCustomAttributes();

        if (isset($custom_attribute['avatar'])) {
            $avatar = $custom_attribute['avatar'];
            $avatar_url = $avatar->getValue();
            $avatar->setValue($avatar_url);
        }

        if (isset($custom_attribute['customer_telephone'])) {
            $phone = $custom_attribute['customer_telephone'];
            $phone_value = $phone->getValue();
            $code = substr($phone_value,0,2);
            if ($code === '84') {
                $position = strpos($phone_value, '84');
                $length = strlen($code);
                $phone_value = substr_replace($phone_value, '0',$position, $length);
            }
            $phone->setValue($phone_value);
        }
    }
}
