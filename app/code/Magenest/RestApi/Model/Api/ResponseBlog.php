<?php

namespace Magenest\RestApi\Model\Api;

use Magento\Framework\DataObject;

class ResponseBlog extends DataObject implements \Magenest\RestApi\Api\ResponseBlogInterface
{
    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->_getData(self::DATA_ID);
    }

    /**
     * @inheritDoc
     */
    public function getAuthorId()
    {
        return $this->_getData(self::DATA_AUTHOR_ID);
    }

    /**
     * @inheritDoc
     */
    public function getTitle()
    {
        return $this->_getData(self::DATA_TITLE);
    }

    /**
     * @inheritDoc
     */
    public function getDescription()
    {
        return $this->_getData(self::DATA_DESCRIPTION);
    }

    /**
     * @inheritDoc
     */
    public function getContent()
    {
        return $this->_getData(self::DATA_CONTENT);
    }

    /**
     * @inheritDoc
     */
    public function getUrlRewrite()
    {
        return $this->_getData(self::DATA_URL_REWRITE);
    }

    /**
     * @inheritDoc
     */
    public function getStatus()
    {
        return $this->_getData(self::DATA_STATUS);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt()
    {
        return $this->_getData(self::DATA_CREATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedAt()
    {
        return $this->_getData(self::DATA_UPDATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function setId(int $id)
    {
        return $this->setData(self::DATA_ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function setAuthorId(int $author_id)
    {
        return $this->setData(self::DATA_ID, $author_id);
    }

    /**
     * @inheritDoc
     */
    public function setTitle(string $title)
    {
        return $this->setData(self::DATA_TITLE, $title);
    }

    /**
     * @inheritDoc
     */
    public function setDescription(string $description)
    {
        return $this->setData(self::DATA_DESCRIPTION, $description);
    }

    /**
     * @inheritDoc
     */
    public function setContent(string $content)
    {
        return $this->setData(self::DATA_CONTENT, $content);
    }

    /**
     * @inheritDoc
     */
    public function setUrlRewrite(string $url_rewrite)
    {
        return $this->setData(self::DATA_URL_REWRITE, $url_rewrite);
    }
}
