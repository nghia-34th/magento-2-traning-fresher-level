<?php

namespace Magenest\RestApi\Model\Api;

use Exception;
use Magento\Framework\Exception\NoSuchEntityException;
use Magenest\RestApi\Api\RequestBlogInterfaceFactory;
use Magenest\RestApi\Api\ResponseBlogInterfaceFactory;
use Magenest\Movie\Model\ResourceModel\Blog\CollectionFactory;
use Magenest\Movie\Model\Blog;

class BlogRepository implements \Magenest\RestApi\Api\BlogRepositoryInterface
{
    public function __construct(
        CollectionFactory $blogCollectionFactory,
        RequestBlogInterfaceFactory $requestBlogFactory,
        ResponseBlogInterfaceFactory $responseBlogFactory,
        Blog $blog
    ) {
        $this->collection = $blogCollectionFactory;
        $this->blog = $blog;
        $this->request = $requestBlogFactory;
        $this->response = $responseBlogFactory;
    }

    /**
     * @inheritDoc
     */
    public function getBlog(int $id)
    {
        $collection = $this->collection->create();
        $blog = $collection->addFilter('id', $id)->getFirstItem();
        if (!$blog->getId()) {
            throw new NoSuchEntityException(__('Blog not found.'));
        }
        return $blog;
    }

    /**
     * @inheritDoc
     */
    public function setBlog(array $blogs)
    {
        $collection = $this->collection->create();
        foreach ($blogs as $blog) {
            if ($blog['url_rewrite'] ) {
                $id = $blog['id'] ?? null;
                $url_rewrite = $blog['url_rewrite'];
                $this->checkDuplicateUrlKey($url_rewrite, $collection, $id);
            }
            $model = $this->blog->setData($blog->getData());
            try {
                $model->save();
                return 'Blog successfully set.';
            } catch (Exception $e) {
                return 'Failed to create/edit a blog.';
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function deleteBlog(int $id)
    {
        $blog = $this->blog->load($id);
        try {
            $blog->delete();
            return 'Blog successfully deleted.';
        } catch (Exception $e) {
            return 'Failed to delete blog.';
        }
    }

    private function checkDuplicateUrlKey($url_rewrite, $collection, $id = null)
    {
        $duplicates = $collection->addFieldToFilter('url_rewrite', $url_rewrite)->getData();
        if ($duplicates) {
            foreach ($duplicates as $duplicate) {
                if ($duplicate['id'] != $id) {
                    throw new NoSuchEntityException(__('Duplicate url_rewrite key detected.'));
                }
            }
        }
    }
}
