<?php

namespace Magenest\RestApi\Api;

interface ResponseBlogInterface
{
    const DATA_ID = 'id';
    const DATA_AUTHOR_ID = 'author_id';
    const DATA_TITLE = 'title';
    const DATA_DESCRIPTION = 'description';
    const DATA_CONTENT = 'content';
    const DATA_URL_REWRITE = 'url_rewrite';
    const DATA_STATUS = 'status';
    const DATA_CREATED_AT = 'created_at';
    const DATA_UPDATED_AT = 'updated_at';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return int
     */
    public function getAuthorId();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @return string
     */
    public function getContent();

    /**
     * @return string
     */
    public function getUrlRewrite();

    /**
     * @return int
     */
    public function getStatus();

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @return string
     */
    public function getUpdatedAt();

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id);

    /**
     * @param int $author_id
     * @return $this
     */
    public function setAuthorId(int $author_id);

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title);

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description);

    /**
     * @param string $content
     * @return $this
     */
    public function setContent(string $content);

    /**
     * @param string $url_rewrite
     * @return $this
     */
    public function setUrlRewrite(string $url_rewrite);
}
