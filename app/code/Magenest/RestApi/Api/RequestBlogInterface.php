<?php

namespace Magenest\RestApi\Api;

interface RequestBlogInterface
{
    const DATA_ID = 'id';
    const DATA_AUTHOR_ID = 'author_id';
    const DATA_TITLE = 'title';
    const DATA_DESCRIPTION = 'description';
    const DATA_CONTENT = 'content';
    const DATA_URL_REWRITE = 'url_rewrite';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return int
     */
    public function getAuthorId();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @return string
     */
    public function getContent();

    /**
     * @return string
     */
    public function getUrlRewrite();

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id);

    /**
     * @param int $author_id
     * @return $this
     */
    public function setAuthorId(int $author_id);

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title);

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description);

    /**
     * @param string $content
     * @return $this
     */
    public function setContent(string $content);

    /**
     * @param string $url_rewrite
     * @return $this
     */
    public function setUrlRewrite(string $url_rewrite);
}
