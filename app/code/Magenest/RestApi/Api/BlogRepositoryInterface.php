<?php

namespace Magenest\RestApi\Api;

use Magento\Tests\NamingConvention\true\mixed;

interface BlogRepositoryInterface
{
    /**
     * Return a filtered blog.
     *
     * @param int $id
     * @return \Magenest\RestApi\Api\ResponseBlogInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBlog(int $id);

    /**
     * Set data for the blog.
     *
     * @param \Magenest\RestApi\Api\RequestBlogInterface[] $blogs
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @return mixed
     */
    public function setBlog(array $blogs);

    /**
     * Delete blog.
     *
     * @param int $id
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @return mixed
     */
    public function deleteBlog(int $id);
}
