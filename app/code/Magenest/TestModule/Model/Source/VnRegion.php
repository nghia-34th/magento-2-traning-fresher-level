<?php

namespace Magenest\TestModule\Model\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class VnRegion extends AbstractSource
{
    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions(): array
    {
        return [
            [
                'value' => null,
                'label' => __('--Please Select--')
            ],
            [
                'value' => 1,
                'label' => __('North')
            ],
            [
                'value' => 2,
                'label' => __('Central')
            ],
            [
                'value' => 3,
                'label' => __('South')
            ]
        ];
    }
}
