<?php

namespace Magenest\Pin\Api\Data;

interface PurchasedInterface
{
    const PURCHASED_ID = 'id';
    const CUSTOMER_ID = 'customer_id';
    const PIN_CODE = 'pin_code';
    const PIN_FILE = 'pin_file';
    const ORDER_ID = 'order_id';
    const ORDER_INCREMENT_ID = 'order_increment_id';
    const ORDER_ITEM_ID = 'order_item_id';
    const STATUS = 'status';
    const PRODUCT_NAME = 'product_name';
    const PRODUCT_SKU = 'product_sku';
    const CREATION_TIME = 'created_at';
    const UPDATE_TIME = 'updated_at';

    public function getPurchasedId();

    public function getPinCode();

    public function getCustomerId();

    public function getCreatedAt();

    public function getUpdatedAt();

    public function getPinFile();

    public function getOrderId();

    public function getOrderIncrementId();

    public function getOrderItemId();

    public function getStatus();

    public function getProductName();

    public function getProductSku();

    public function setPurchasedId($id);

    public function setPinCode($pinCode);

    public function setCustomerId($customerId);

    public function setCreatedAt($creationTime);

    public function setUpdatedAt($updateTime);

    public function setPinFile($pinFile);

    public function setOrderId($orderId);

    public function setOrderIncrementId($orderIncrementId);

    public function setOrderItemId($orderItemId);

    public function setStatus($status);

    public function setProductName($productName);

    public function setProductSku($productSku);
}
