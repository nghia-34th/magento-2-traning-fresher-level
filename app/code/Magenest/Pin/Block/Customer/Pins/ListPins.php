<?php

namespace Magenest\Pin\Block\Customer\Pins;

use Magenest\Pin\Api\Data\PurchasedInterface;
use Magenest\Pin\Model\ResourceModel\Purchased\Collection as PurchasedCollection;

/**
 * Block to display downloadable links bought by customer
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class ListPins extends \Magento\Framework\View\Element\Template
{
    const URL_DOWNLOAD = 'pin/customer/download';

    const LIMIT = 'limit';
    /**
     * @var \Magento\Customer\Model\SessionFactory
     */
    protected $currentCustomer;

    /**
     * @var \Magenest\Pin\Model\ResourceModel\Purchased\CollectionFactory
     */
    protected $_purchasedCollectionFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magenest\Pin\Model\ResourceModel\Purchased\CollectionFactory $purchasedCollectionFactory
     * @param \Magento\Customer\Model\SessionFactory $currentCustomer
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magenest\Pin\Model\ResourceModel\Purchased\CollectionFactory $purchasedCollectionFactory,
        \Magento\Customer\Model\SessionFactory $currentCustomer,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->currentCustomer = $currentCustomer;
        $this->_purchasedCollectionFactory = $purchasedCollectionFactory;
        $this->getPurchaseds();
    }

    /**
     * @return mixed
     *
     */
    public function getCustomerId()
    {
        return $this->currentCustomer->create()->getCustomer()->getId();
    }

    /**
     * @return $this|null
     */
    public function getPurchaseds()
    {
        $purchased = $this->_purchasedCollectionFactory
            ->create()
            ->addFieldToFilter('customer_id', $this->getCustomerId())
            ->addFieldToFilter('status', 1)
            ->addOrder(
                PurchasedInterface::CREATION_TIME,
                'DESC'
            );
        $purchased->setPageSize($this->getLimit());
        $purchased->setCurPage($this->getPage());
        if ($purchased->getSize() > 0) {
            return $purchased;
        } else {
            return [];
        }
    }

    /**
     * Enter description here...
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $purchaseds = $this->getPurchaseds();
        if ($purchaseds == null) {
            $purchaseds = $this->_purchasedCollectionFactory->create();
        }
        $pager = $this->getLayout()->createBlock(
            'Magento\Theme\Block\Html\Pager',
            'pin.customer.products.pager'
        )->setCollection(
            $purchaseds
        )->setPath('pin/customer/pins');
        $this->setChild('pager', $pager);
        if ($this->getPurchaseds() != null) {
            $this->getPurchaseds()->load();
        }
        return $this;
    }

    public function getIdentities()
    {
        return [\Magenest\Pin\Model\Purchased::CACHE_TAG . '_' . 'list'];
    }

    public function getDownloadLink($id, $type)
    {
        return $this->getUrl(self::URL_DOWNLOAD, ['id' => $id, 'type' => $type, '_secure' => true]);
    }

    public function getLimit()
    {
        return $this->_request->getParam(self::LIMIT) != '' ? $this->_request->getParam(self::LIMIT) : 10;
    }

    public function getPage()
    {
        return $this->_request->getParam('p') != '' ? $this->_request->getParam('p') : 1;
    }
}
