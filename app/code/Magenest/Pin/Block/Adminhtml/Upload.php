<?php
namespace Magenest\Pin\Block\Adminhtml;

use \Magento\Backend\Block\Widget\Grid\Container as GridContainer;

class Upload extends GridContainer
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->buttonList->update('add', 'label', 'Upload');
    }

    protected function _prepareLayout()
    {
        $this->setChild(
            'grid',
            $this->getLayout()->createBlock('Magenest\Pin\Block\Adminhtml\Upload\Grid', 'pin.upload.grid')
        );
        return parent::_prepareLayout();
    }
}
