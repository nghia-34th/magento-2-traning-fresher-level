<?php

namespace Magenest\Pin\Block\Adminhtml\Sales\Items\Column\Pin;

/**
 * Sales Order items name column renderer
 */

use Magento\Backend\Helper\Data;

class Code extends \Magento\Sales\Block\Adminhtml\Items\Column\DefaultColumn
{
    const URL = 'pin/file/download';
    const LICENSE = 'license';
    const DATABASE = 'database';
    /**
     * Option factory
     *
     * @var \Magento\Catalog\Model\Product\OptionFactory
     */
    protected $_optionFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magenest\Pin\Model\PurchasedFactory
     */
    protected $_orderPurchase;

    /**
     * @var Data
     */
    protected $_helper;

    /**
     * Name constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Model\Product\OptionFactory $optionFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magenest\Pin\Model\PurchasedFactory $purchasedFactory
     * @param Data $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\Product\OptionFactory $optionFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magenest\Pin\Model\PurchasedFactory $purchasedFactory,
        Data $helper,
        array $data
    ) {
        parent::__construct($context, $stockRegistry, $stockConfiguration, $registry, $optionFactory, $data);
        $this->_productFactory = $productFactory;
        $this->_orderPurchase = $purchasedFactory;
        $this->_helper = $helper;
    }

    /**
     * Truncate string
     *
     * @param string $value
     * @param int $length
     * @param string $etc
     * @param string &$remainder
     * @param bool $breakWords
     * @return string
     */
    public function truncateString($value, $length = 80, $etc = '...', &$remainder = '', $breakWords = true)
    {
        return $this->filterManager->truncate(
            $value,
            ['length' => $length, 'etc' => $etc, 'remainder' => $remainder, 'breakWords' => $breakWords]
        );
    }

    /**
     * Add line breaks and truncate value
     *
     * @param string $value
     * @return array
     */
    public function getFormattedOption($value)
    {
        $remainder = '';
        $value = $this->truncateString($value, 55, '', $remainder);
        $result = ['value' => nl2br($value), 'remainder' => nl2br($remainder)];

        return $result;
    }

    /**
     * Check show game code
     *
     * @param $productId
     * @return bool
     */
    public function checkLicense($productId)
    {
        $check = false;
        $collection = $this->_productFactory->create()->load($productId);
        $typeProduct = $collection->getTypeId();
        if ($typeProduct == self::LICENSE) {
            $check = true;
        }

        return $check;
    }

    /**
     * Get Text code
     *
     * @param $itemId
     * @return string
     */
    public function getTextLicense($itemId)
    {
        $code = '';
        $collection = $this->_orderPurchase->create()->getCollection()->addFieldToFilter('order_item_id', $itemId)->getFirstItem();
        $code = $collection->getPinCode();

        return $code;
    }

    /**
     * Get File Code
     *
     * @param $itemId
     * @return string
     */
    public function render($itemId)
    {
        $collection = $this->_orderPurchase->create()->getCollection()->addFieldToFilter('order_item_id', $itemId)->getFirstItem();
        $id = 0;
        $nameFile = $collection->getFileExtension();
        if ($nameFile == self::DATABASE) {
            $id = $collection->getPinFileId();
        }
        return $this->getUrl(self::URL, ['name' => $nameFile, 'file_id' => $id]);
    }

    public function render1($purchased)
    {
        $collection = $this->_orderPurchase->create()->load($purchased->getId());
        $id = 0;
        $nameFile = $collection->getFileExtension();
        if ($nameFile == self::DATABASE) {
            $id = $collection->getPinFileId();
        }
        return $this->getUrl(self::URL, ['name' => $nameFile, 'file_id' => $id]);
    }

    /**
     * Check file type
     *
     * @param $itemId
     * @return string
     */
    public function fileType($itemId)
    {
        $collections = $this->_orderPurchase->create()->getCollection()->addFieldToFilter('order_item_id', $itemId);
        foreach ($collections as $collection) {
            if ($collection->getPinCode()) {
                $code[] = 'text';
            } else {
                $code[] = 'file';
            }
        }
        return $code;
    }

    public function getAll($itemId)
    {
        $collections = $this->_orderPurchase->create()->getCollection()
            ->addFieldToFilter('order_item_id', $itemId);
        return $collections;
    }
}
