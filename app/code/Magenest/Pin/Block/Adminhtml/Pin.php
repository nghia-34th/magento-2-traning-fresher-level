<?php
namespace Magenest\Pin\Block\Adminhtml;

class Pin extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->removeButton('add');
    }

    /**
     * Prepare button and grid
     *
     * @return \Magenest\Salesforce\Block\Adminhtml\Report\Grid
     */
    protected function _prepareLayout()
    {
        $this->setChild(
            'grid',
            $this->getLayout()->createBlock('Magenest\Pin\Block\Adminhtml\Pin\Grid', 'pin.pin.grid')
        );
        return parent::_prepareLayout();
    }
}
