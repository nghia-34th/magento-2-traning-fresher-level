<?php
namespace Magenest\Pin\Block\Adminhtml;

class PinFile extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->removeButton('add');
    }

    /**
     * Prepare button and grid
     *
     * @return \Magenest\Salesforce\Block\Adminhtml\Report\Grid
     */
    protected function _prepareLayout()
    {
        $this->setChild(
            'grid',
            $this->getLayout()->createBlock('Magenest\Pin\Block\Adminhtml\PinFile\Grid', 'pin.pinfile.grid')
        );
        return parent::_prepareLayout();
    }
}
