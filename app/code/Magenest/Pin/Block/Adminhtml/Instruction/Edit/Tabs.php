<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 27/07/2016
 * Time: 23:38
 */
namespace Magenest\Pin\Block\Adminhtml\Instruction\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('post_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Instruction'));
    }
}
