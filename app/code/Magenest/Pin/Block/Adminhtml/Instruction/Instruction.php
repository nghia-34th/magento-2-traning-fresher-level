<?php
/**
 * Created by PhpStorm.
 * User: canh
 * Date: 25/01/2016
 * Time: 09:33
 */
namespace Magenest\Pin\Block\Adminhtml\Instruction;

class Instruction extends \Magento\Backend\Block\Widget\Grid\Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'Magenest_Pin';
        $this->_controller = 'adminhtml_instruction';
        $this->_headerText = __('Pin');
        $this->_addButtonLabel = __('Add Instruction');
        parent::_construct();
    }

    protected function _prepareLayout()
    {
        $this->setChild(
            'grid',
            $this->getLayout()->createBlock('Magenest\Pin\Block\Adminhtml\Instruction\Grid', 'pin.instruction.grid')
        );
        return parent::_prepareLayout();
    }
}
