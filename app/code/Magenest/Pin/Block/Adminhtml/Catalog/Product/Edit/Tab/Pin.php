<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 02/10/2015
 * Time: 11:35
 */

namespace Magenest\Pin\Block\Adminhtml\Catalog\Product\Edit\Tab;

class Pin extends \Magento\Backend\Block\Widget implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var string
     */
    protected $_template = 'product/edit/tab/pin.phtml';

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \Magenest\Pin\Helper\Data
     */
    protected $_dataHelper;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Pin\Helper\Data $dataHelper,
        array $data = []
    ) {
        $this->_registry = $registry;
        $this->_dataHelper = $dataHelper;
        parent::__construct($context);
    }

    /**
     * Return Tab label
     *
     * @return string
     * @api
     */
    public function getTabLabel()
    {
        return __("Game license");
    }

    /**
     * Return Tab title
     *
     * @return string
     * @api
     */
    public function getTabTitle()
    {
        return __("Game license");
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     * @api
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     * @api
     */
    public function isHidden()
    {
        if ($this->isPinProduct()) {
            return false;
        } else {
            return true;
        }
    }

    public function isPinProduct()
    {
        /** @var  $product  \Magento\Catalog\Model\Product */
        $product = $this->_registry->registry('current_product');

        $typeInstance = $product->getTypeInstance();
        $typeId = $product->getTypeId();
        if ($typeId == 'license') {
            return true;
        } else {
            return false;
        }
    }
}
