<?php
/**
 * @TODO: 20102015
 */
namespace Magenest\Pin\Block\Adminhtml\Upload\Edit;

/**
 * Adminhtml blog post edit form block
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getUrl('pin/upload/save'),//@TODO: Them action
                    'method' => 'post',
                    'enctype' => 'multipart/form-data',
                ],
            ]
        );
        // fieldset for file uploading
        $fieldsets['upload'] = $form->addFieldset('upload_file_fieldset', ['legend' => __('File to Import')]);
        $fieldsets['upload']->addField(
            \Magento\ImportExport\Model\Import::FIELD_NAME_SOURCE_FILE,
            'file',
            [
                'name' => \Magento\ImportExport\Model\Import::FIELD_NAME_SOURCE_FILE,
                'label' => __('Import csv file'),
                'title' => __('Select File to Import'),
                'required' => true,
                'class' => 'input-file',
                'after_element_html' => '
                    <script>
                        require([
                             "jquery",
                        ], function($){
                            $(document).ready(function () {
                                $("#import_file").attr( "accept", ".xlsx, .xls, .csv" );
                            });
                          });
                   </script>'
            ]
        );
        $fieldsets['upload']->addField(
            'note',
            'note',
            [
                'after_element_html' => $this->getDownloadSampleFileHtml(),
            ]
        );
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Get download sample file html
     *
     * @return string
     */
    protected function getDownloadSampleFileHtml()
    {
        $linkDownloadSample = $this->getDownloadSampleFile();

        $html = "<div id='sample-file-span' class='no-display' style='display: inline;margin-left: calc( (100%) * .5);'><a id='sample-file-link' href='{$linkDownloadSample}'>"
            . __('Download Sample File')
            . "</a></div>";
        return $html;
    }

    public function getDownloadSampleFile()
    {
        return $this->getUrl('pin/upload/sample');
    }
}
