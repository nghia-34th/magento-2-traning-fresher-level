<?php
/**
 * @TODO: 20102015
 */
namespace Magenest\Pin\Block\Adminhtml\Upload;

use \Magento\Backend\Block\Widget\Form\Container;
use \Magento\Backend\Block\Widget\Context;

class Edit extends Container
{
    protected $_coreRegistry = null;

    public function __construct(
        Context $context,
        array $data = []
    ) {
    
        parent::__construct($context, $data);
    }

    public function _construct()
    {
        parent::_construct();

        $this->buttonList->remove('back');
        $this->buttonList->remove('reset');
        $this->buttonList->update('save', 'label', __('Upload'));
        $this->buttonList->update('save', 'id', 'upload_button');
//        $this->buttonList->update('save', 'data_attribute', '');

        $this->_objectId = 'id';
        $this->_blockGroup = 'Magenest_Pin';
        $this->_controller = 'adminhtml_upload';
    }
}
