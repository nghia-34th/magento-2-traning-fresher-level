<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 22/04/2017
 * Time: 09:42
 */

namespace Magenest\Pin\Block\Adminhtml;

use \Magento\Backend\Block\Widget\Grid\Container as GridContainer;

class License extends GridContainer
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
    }

    protected function _prepareLayout()
    {
        $this->setChild(
            'grid',
            $this->getLayout()->createBlock('Magenest\Pin\Block\Adminhtml\Licence\Grid', 'pin.import.grid')
        );
        return parent::_prepareLayout();
    }
}
