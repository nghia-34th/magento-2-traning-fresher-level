<?php
namespace Magenest\Pin\Block\Adminhtml;

class Pincode extends \Magento\Backend\Block\Widget\Container
{
//    protected $template = 'pin/pincode.phtml';


    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_blockGroup = 'Magenest_Pin';
        $this->_controller = 'adminhtml_pincode_import';
        $this->_headerText = __('Pin');
        $this->_addButtonLabel = __('Import text License');
        parent::_construct();
    }

    /**
     * Prepare button and grid
     *
     * @return \Magenest\Pin\Block\Adminhtml\Pincode\Grid
     */
    protected function _prepareLayout()
    {
        $this->setChild(
            'grid',
            $this->getLayout()->createBlock('Magenest\Pin\Block\Adminhtml\Pincode\Grid', 'pin.pincode.grid')
        );
        return parent::_prepareLayout();
    }
}
