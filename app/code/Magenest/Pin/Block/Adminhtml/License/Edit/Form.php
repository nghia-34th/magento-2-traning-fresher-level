<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 22/04/2017
 * Time: 09:40
 */

namespace Magenest\Pin\Block\Adminhtml\License\Edit;

/**
 * Adminhtml post edit form block
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    const FILE_OPTION = 2;

    const TEXT_OPTION = 1;
    const MAX_SIZE = 64000;
    const FILE_EXTENSION = 'file_extension';
    const DATABASE = 'database';

//    protected $_template = "Magenest_Pin::pin/license/form.phtml";

    /**
     * @var \Magenest\Pin\Model\PinFactory;
     */
    protected $_pinFactory;

    /**
     * @var \Magenest\Pin\Model\BlobFactory
     */
    protected $_blobFactory;
    /**
     * @var \Magenest\Pin\Block\Adminhtml\License
     */
    private $license;

    public function __construct(
        \Magenest\Pin\Model\BlobFactory            $blobFactory,
        \Magenest\Pin\Model\PinFactory             $pinFactory,
        \Magento\Backend\Block\Template\Context    $context,
        \Magento\Framework\Registry                $registry,
        \Magenest\Pin\Block\Adminhtml\License\Edit $license,
        \Magento\Framework\Data\FormFactory        $formFactory,
        array                                      $data
    ) {
        $this->_blobFactory = $blobFactory;
        $this->_pinFactory = $pinFactory;
        $this->license = $license;

        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return array
     */
    public function getOptionArray()
    {
        return [
            self::TEXT_OPTION => __('Text'),
            self::FILE_OPTION => __('Image'),
        ];
    }

    /**
     * @return mixed
     */
    public function getPinId()
    {
        return $this->getRequest()->getParam('id');
    }

    /**
     * @return \Magenest\Pin\Model\Pin
     */
    public function getPin()
    {
        $pinModel = $this->_pinFactory->create()->load($this->getPinId());

        return $pinModel;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getImageElement()
    {
        $pin = $this->getPin();
        $src = '<p>Null</p>';
        if ($pin[self::FILE_EXTENSION] == 'text') {
            $src = '';
        }
        if ($pin[self::FILE_EXTENSION] == 'system') {
            $mediaDirectory = $this->_storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            );
            $imageUrl = $mediaDirectory . 'pin/file/' . $pin->getOriFileName();
            $src = '<img src="' . $imageUrl . '" />';
        }
        if ($pin[self::FILE_EXTENSION] == self::DATABASE) {
            $blob = $this->_blobFactory->create()->load($pin->getId());
            $imageSrc = 'data:image/jpeg;base64,' . base64_encode($blob->getPinFile());
            $src = '<img src="' . $imageSrc . '" width=50 height=50 />';
        }
        return $src;
    }

    /**
     * @return Form
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $pin = $this->getPin();
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getUrl('pin/license/save'),//@TODO: Them action
                    'method' => 'post',
                    'enctype' => 'multipart/form-data',
                ],
            ]
        );
        // fieldset for file uploading
        $fieldsets['upload'] = $form->addFieldset('edit_pin', ['legend' => __('Pin')]);
        $fieldsets['upload']->addField(
            'id',
            'text',
            [
                'name' => 'id',
                'label' => __('Pin ID'),
                'value' => $pin->getId(),
                'required' => true,
                'readonly' => true
            ]
        );
        $fieldsets['upload']->addField(
            'product_name',
            'text',
            [
                'name' => 'product_name',
                'label' => __('Product Name'),
                'value' => $pin->getProductName(),
                'required' => true,
                'readonly' => true
            ]
        );
        $fieldsets['upload']->addField(
            'url_path',
            'hidden',
            [
                'name' => 'url_path',
                'value' => $this->license->getPathUrl(),
            ]
        );

        $fieldsets['upload']->addField(
            'comment',
            'text',
            [
                'name' => 'comment',
                'label' => __('Comment'),
                'value' => $pin->getComments()
            ]
        );

        $fieldsets['upload']->addField(
            'save_option',
            'select',
            [
                'name' => 'save_option',
                'label' => __('Save as'),
                'values' => $this->getOptionArray(),
                'value' => $pin->getPinType(),
                'component' => 'Magenest_Pin/js/edit/select-type'
            ]
        );

        $fieldsets['upload']->addField(
            'pin_code',
            'text',
            [
                'name' => 'pin_code',
                'label' => __('Pin Code'),
                'value' => $pin->getCode(),
                'required' => false
            ]
        );

        $image = $this->getImageElement();

        $fieldsets['upload']->addField(
            'pin_file',
            'file',
            [
                'name' => 'pin_file_after',
                'label' => __('Pin File'),
                'before_element_html' => $image,
                'width' => '100',
                'height' => '100',
                'after_element_html' => '
                    <script>
                        require([
                             "jquery",
                        ], function($){
                            $(document).ready(function () {
                                $( "#pin_file" ).attr( "accept", "image/x-png,image/gif,image/jpeg,image/jpg,image/png" );
                                $("#pin_file").change(function() {  
                        
                                  fileSize = this.files[0].size;
                                    if (fileSize > 64000) {
                                         alert("File must not exceed 64kB !");
                                         this.value="";
                                    }
                                    })
                                });
                            });
                   </script>'
            ]
        );

        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}