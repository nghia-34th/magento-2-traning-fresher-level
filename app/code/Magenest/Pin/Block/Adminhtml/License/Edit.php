<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 22/04/2017
 * Time: 09:41
 */

namespace Magenest\Pin\Block\Adminhtml\License;

use \Magento\Backend\Block\Widget\Form\Container;
use \Magento\Backend\Block\Widget\Context;

class Edit extends Container
{
    protected $_coreRegistry = null;

    public function __construct(
        Context           $context,
        array             $data = []
    ) {
        parent::__construct($context, $data);

    }

    public function _construct()
    {
        parent::_construct();
        $this->buttonList->remove('reset');
        $this->buttonList->remove('delete');
        $this->buttonList->update('back', 'onclick', 'setLocation(\'' . $this->getPathUrl() . '\')');
//        $this->buttonList->update('save', 'onclick', 'setLocation(\'' . $this->getBackUrl() . '\')');


        $this->_objectId = 'id';
        $this->_blockGroup = 'Magenest_Pin';
        $this->_controller = 'adminhtml_license';
    }

    public function getPathUrl()
    {
        // lay url cua previous page
        $url = $this->getRequest()->getServer('HTTP_REFERER');
        if ($url == null) {
            $url = $this->getUrl('pin/license/index');
        }
        return $url;
    }
}