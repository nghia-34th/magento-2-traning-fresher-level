<?php
namespace Magenest\Pin\Block\Adminhtml;

use \Magento\Backend\Block\Widget\Grid\Container as GridContainer;

class Import extends GridContainer
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->buttonList->update('add', 'label', 'Import License');
        $this->buttonList->update('add', 'onclick', "window.open('{$this->getUrl('pin/upload/index')}')");
    }

    protected function _prepareLayout()
    {
        $this->setChild(
            'grid',
            $this->getLayout()->createBlock('Magenest\Pin\Block\Adminhtml\Import\Grid', 'pin.import.grid')
        );
        return parent::_prepareLayout();
    }
}
