<?php

namespace Magenest\Pin\Model;

use Magenest\Pin\Api\Data\PurchasedInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Purchased extends \Magento\Framework\Model\AbstractModel implements PurchasedInterface, IdentityInterface
{

    const STATUS_COMPLETE = 1;
    const STATUS_PENDING = 0;
    const STATUS_EXPIRED = 2;
    const CACHE_TAG = 'pin_purchased';

    protected function _construct()
    {
        $this->_init('Magenest\Pin\Model\ResourceModel\Purchased');
    }

    public function getCreatedAt()
    {
        return $this->getData(self::CREATION_TIME);
    }

    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    public function getPinCode()
    {
        return $this->getData(self::PIN_CODE);
    }

    public function getOrderIncrementId()
    {
        return $this->getData(self::ORDER_INCREMENT_ID);
    }

    public function getPurchasedId()
    {
        return $this->getData(self::PURCHASED_ID);
    }

    public function getOrderItemId()
    {
        return $this->getData(self::ORDER_ITEM_ID);
    }

    public function getPinFile()
    {
        return $this->getData(self::PinFile);
    }

    public function getProductName()
    {
        return $this->getData(self::PRODUCT_NAME);
    }

    public function getProductSku()
    {
        return $this->getData(self::PRODUCT_SKU);
    }

    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    public function setCreatedAt($creationTime)
    {
        return $this->setData(self::CREATION_TIME, $creationTime);
    }

    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    public function setUpdatedAt($updateTime)
    {
        return $this->setData(self::UPDATE_TIME, $updateTime);
    }

    public function setPurchasedId($purchasedId)
    {
        return $this->setData(self::PURCHASED_ID, $purchasedId);
    }

    public function setPinCode($pinCode)
    {
        return $this->setData(self::PIN_CODE, $pinCode);
    }

    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    public function setOrderIncrementId($orderIncrementId)
    {
        return $this->setData(self::ORDER_INCREMENT_ID, $orderIncrementId);
    }

    public function setOrderItemId($orderItemId)
    {
        return $this->setData(self::ORDER_ITEM_ID, $orderItemId);
    }

    public function setPinFile($pinFile)
    {
        return $this->setData(self::PIN_FILE, $pinFile);
    }

    public function setProductName($productName)
    {
        return $this->setData(self::PRODUCT_NAME, $productName);
    }

    public function setProductSku($productSku)
    {
        return $this->setData(self::PRODUCT_SKU, $productSku);
    }

    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }
    /**
     * Check order id
     * @return $this
     * @throws \Exception
     */
    public function beforeSave()
    {
        if (null == $this->getOrderId()) {
            throw new \Exception(__('Order id cannot be null'));
        }
        return parent::beforeSave();
    }

    /**
     * @param $order_item_id
     * @return bool
     */
    public function getIsOrderItemProcessed($order_item_id)
    {
        $isProcessed = false;

        $collection = $this->getCollection()->getIsOrderItemProcessed($order_item_id);

        if ($collection->getSize() > 0) {
            $isProcessed = true;
        }

        return $isProcessed;
    }

}
