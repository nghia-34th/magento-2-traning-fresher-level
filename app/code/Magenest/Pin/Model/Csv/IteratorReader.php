<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 21/12/2018
 * Time: 11:16
 */

namespace Magenest\Pin\Model\Csv;

class IteratorReader
{
    /**
     * @var null
     */
    protected $filePath = null;

    /**
     * @var null
     */
    protected $file = null;

    /**
     * @var null
     */
    protected $currentRow = null;

    public function initialize($filePath)
    {
        $this->file = fopen($filePath, 'r');
    }

    /**
     * @throws \Exception
     */
    public function nextRow()
    {
        if ($this->file === null) {
            throw new \Exception('CSV File Was Not Initialized');
        }
        $line = fgetcsv($this->file);
        $this->currentRow = $line;
        return $this->currentRow;
    }

    public function getCurrentRow()
    {
        return $this->currentRow;
    }

    public function __destruct()
    {
        if ($this->file != null) {
            fclose($this->file);
        }
    }

    public function reset()
    {
        $this->filePath = null;
        $this->file = null;
        $this->currentRow = null;
    }
}
