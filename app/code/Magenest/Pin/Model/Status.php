<?php

namespace Magenest\Pin\Model;

class Status
{
    /**
     * PIN Status values
     */
    const STATUS_ENABLED = 1;

    const STATUS_DISABLED = 2;

    const STATUS_DELIVERED = 3;

    const STATUS_SOLD = 4;

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public function getOptionArray()
    {
        return [self::STATUS_ENABLED => __('Enabled'),
            self::STATUS_DISABLED => __('Disabled'),
            self::STATUS_DELIVERED => __('Delivered'),
            self::STATUS_SOLD=> __('Sold')];
    }

    /**
     * Retrieve option array with empty value
     *
     * @return string[]
     */
    public function getAllOptions()
    {
        $result = [];

        foreach ($this->getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     * @return string
     */
    public function getOptionText($optionId)
    {
        $options = $this->getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}