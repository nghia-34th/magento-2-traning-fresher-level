<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_magento232_dev extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_magento232_dev
 */

namespace Magenest\Pin\Model\Mail;


class TransportBuilderCheckVersion
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Instance name to create
     *
     * @var string
     */
    protected $_instanceNameForM232 = null;

    /**
     * Instance name to create
     *
     * @var string
     */
    protected $_instanceNameForM233 = null;
    protected $_productMetadata;

    /**
     * Factory constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\App\ProductMetadataInterface $productMetadata
     * @param string $instanceNameForM232
     * @param string $instanceNameForM233
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        $instanceNameForM232 = '\\Magenest\\Pin\\Model\\Mail\\TransportBuilderFor232',
        $instanceNameForM233 = '\\Magenest\\Pin\\Model\\Mail\\TransportBuilderFor233'
    ) {
        $this->_objectManager = $objectManager;
        $this->_instanceNameForM232 = $instanceNameForM232;
        $this->_instanceNameForM233 = $instanceNameForM233;
        $this->_productMetadata = $productMetadata;
    }

    /**
     * Create class instance with specified parameters
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data = [])
    {
        $version = $this->_productMetadata->getVersion();
        if ($version >= "2.3.3") {
            return $this->_objectManager->create($this->_instanceNameForM233, $data);
        } elseif ($version <= "2.3.2") {
            return $this->_objectManager->create($this->_instanceNameForM232, $data);
        }
    }
}
