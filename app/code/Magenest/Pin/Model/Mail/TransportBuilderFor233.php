<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_magento232_dev extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_magento232_dev
 */

namespace Magenest\Pin\Model\Mail;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\TemplateTypesInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Mail\Address;
use Magento\Framework\Mail\AddressConverter;
use Magento\Framework\Mail\AddressFactory;
use Magento\Framework\Mail\EmailMessageInterface;

//use Magento\Framework\Mail\EmailMessageInterfaceFactory;
use Magento\Framework\Mail\Exception\InvalidArgumentException;
use Magento\Framework\Mail\MessageInterface;
use Magento\Framework\Mail\MessageInterfaceFactory;
use Magento\Framework\Mail\MimeInterface;

//use Magento\Framework\Mail\MimeMessageInterfaceFactory;
//use Magento\Framework\Mail\MimePartInterfaceFactory;
use Magento\Framework\Mail\Template\FactoryInterface;
use Magento\Framework\Mail\Template\SenderResolverInterface;
use Magento\Framework\Mail\TransportInterface;
use Magento\Framework\Mail\TransportInterfaceFactory;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Phrase;

class TransportBuilderFor233 extends \Magento\Framework\Mail\Template\TransportBuilder
{
    /**
     * Template Identifier
     *
     * @var string
     */
    protected $templateIdentifier;

    /**
     * Template Model
     *
     * @var string
     */
    protected $templateModel;

    /**
     * Template Variables
     *
     * @var array
     */
    protected $templateVars;

    /**
     * Template Options
     *
     * @var array
     */
    protected $templateOptions;

    /**
     * Mail Transport
     *
     * @var TransportInterface
     */
    protected $transport;

    /**
     * Template Factory
     *
     * @var FactoryInterface
     */
    protected $templateFactory;

    /**
     * Object Manager
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * Message
     *
     * @var EmailMessageInterface
     */
    protected $message;

    /**
     * Sender resolver
     *
     * @var SenderResolverInterface
     */
    protected $_senderResolver;

    /**
     * @var TransportInterfaceFactory
     */
    protected $mailTransportFactory;

    /**
     * Param that used for storing all message data until it will be used
     *
     * @var array
     */
    private $messageData = [];

    /**
     * @var EmailMessageInterfaceFactory
     */
    private $emailMessageInterfaceFactory;

    /**
     * @var MimeMessageInterfaceFactory
     */
    private $mimeMessageInterfaceFactory;

    /**
     * @var MimePartInterfaceFactory
     */
    private $mimePartInterfaceFactory;

    /**
     * @var AddressConverter|null
     */
    private $addressConverter;

    private $_attachments;

    /**
     * TransportBuilder constructor
     *
     * @param FactoryInterface $templateFactory
     * @param MessageInterface $message
     * @param SenderResolverInterface $senderResolver
     * @param ObjectManagerInterface $objectManager
     * @param TransportInterfaceFactory $mailTransportFactory
     * @param MessageInterfaceFactory|null $messageFactory
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        FactoryInterface $templateFactory,
        MessageInterface $message,
        SenderResolverInterface $senderResolver,
        ObjectManagerInterface $objectManager,
        TransportInterfaceFactory $mailTransportFactory,
        MessageInterfaceFactory $messageFactory = null
    ) {
        $this->templateFactory = $templateFactory;
        $this->objectManager = $objectManager;
        $this->_senderResolver = $senderResolver;
        $this->mailTransportFactory = $mailTransportFactory;
    }

    /**
     * Add cc address
     *
     * @param array|string $address
     * @param string $name
     *
     * @return \Magento\Framework\Mail\Template\TransportBuilder
     */
    public function addCc($address, $name = '')
    {
        $this->addAddressByType('cc', $address, $name);

        return $this;
    }

    /**
     * Add to address
     *
     * @param array|string $address
     * @param string $name
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function addTo($address, $name = '')
    {
        $this->addAddressByType('to', $address, $name);

        return $this;
    }

    /**
     * Add bcc address
     *
     * @param array|string $address
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function addBcc($address)
    {
        $this->addAddressByType('bcc', $address);

        return $this;
    }

    /**
     * Set Reply-To Header
     *
     * @param string $email
     * @param string|null $name
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setReplyTo($email, $name = null)
    {
        $this->addAddressByType('replyTo', $email, $name);

        return $this;
    }

    /**
     * Set mail from address
     *
     * @param string|array $from
     *
     * @return $this
     * @throws InvalidArgumentException
     * @see setFromByScope()
     *
     * @deprecated 102.0.1 This function sets the from address but does not provide
     * a way of setting the correct from addresses based on the scope.
     */
    public function setFrom($from)
    {
        return $this->setFromByScope($from);
    }

    /**
     * Set mail from address by scopeId
     *
     * @param string|array $from
     * @param string|int $scopeId
     *
     * @return $this
     * @throws InvalidArgumentException
     * @throws MailException
     * @since 102.0.1
     */
    public function setFromByScope($from, $scopeId = null)
    {
        $result = $this->_senderResolver->resolve($from, $scopeId);
        $this->addAddressByType('from', $result['email'], $result['name']);

        return $this;
    }

    /**
     * Set template identifier
     *
     * @param string $templateIdentifier
     *
     * @return $this
     */
    public function setTemplateIdentifier($templateIdentifier)
    {
        $this->templateIdentifier = $templateIdentifier;

        return $this;
    }

    /**
     * Set template model
     *
     * @param string $templateModel
     *
     * @return $this
     */
    public function setTemplateModel($templateModel)
    {
        $this->templateModel = $templateModel;
        return $this;
    }

    /**
     * Set template vars
     *
     * @param array $templateVars
     *
     * @return $this
     */
    public function setTemplateVars($templateVars)
    {
        $this->templateVars = $templateVars;

        return $this;
    }

    /**
     * Set template options
     *
     * @param array $templateOptions
     * @return $this
     */
    public function setTemplateOptions($templateOptions)
    {
        $this->templateOptions = $templateOptions;

        return $this;
    }


    /**
     * Get mail transport
     *
     * @return TransportInterface
     * @throws LocalizedException
     */
    public function getTransport()
    {
        try {
            $this->prepareMessage();
            $mailTransport = $this->mailTransportFactory->create(['message' => clone $this->message]);
        } catch (\Exception $e) {
            $error = $e->getMessage();
        } finally {
            $this->reset();
        }
        return $mailTransport;
    }

    /**
     * @param $params
     * @param bool|\Magento\Framework\Mail\TransportInterface $transport
     * @return $this
     * @throws \Exception
     */
    public function createAttachment($params, $transport = false)
    {
        $type = isset($params['type']) ? $params['type'] : \Zend_Mime::TYPE_OCTETSTREAM;

        if ($transport === false) {
            $this->message->createAttachment(
                $params['body'],
                $type,
                \Zend_Mime::DISPOSITION_ATTACHMENT,
                \Zend_Mime::ENCODING_BASE64,
                $params['name']
            );
        } else {
            $this->addAttachment($params, $transport);
        }

        return $this;
    }

    /**
     * @param $params
     * @param $transport \Magento\Framework\Mail\TransportInterface
     * @throws \Exception
     */
    public function addAttachment($params, $transport)
    {
        /*$zendPart = $this->createZendMimePart($params);
        $parts = $transport->getMessage()->getBody()->addPart($zendPart);
        $transport->getMessage()->setBody($parts);*/
    }

    protected function createZendMimePart($params)
    {
        if (class_exists('Zend\Mime\Mime') && class_exists('Zend\Mime\Part')) {
            $type = isset($params['type']) ? $params['type'] : \Zend\Mime\Mime::TYPE_OCTETSTREAM;
            if (isset($params['body'])) {
                $part = new \Zend\Mime\Part($params['body']);
            } else {
                $part = new \Zend\Mime\Part(null);
            }
            if (isset($params['name'])) {
                $part->filename = $params['name'];
            } else {
                $part->filename = null;
            }
            $part->type = $type;
            $part->disposition = \Zend\Mime\Mime::DISPOSITION_ATTACHMENT;
            $part->encoding = \Zend\Mime\Mime::ENCODING_BASE64;
            return $part;
        } else {
            throw new \Exception("Missing Zend Framework Source");
        }
    }


    public function prepareMessage()
    {
        $this->emailMessageInterfaceFactory = $this->objectManager->get('Magento\Framework\Mail\EmailMessageInterfaceFactory');
        $this->mimeMessageInterfaceFactory = $this->objectManager->get('Magento\Framework\Mail\MimeMessageInterfaceFactory');
        $this->mimePartInterfaceFactory = $this->objectManager->get('Magento\Framework\Mail\MimePartInterfaceFactory');
        //return $this->prepareMessage();

        $template = $this->getTemplate();
        $content = $template->processTemplate();
        switch ($template->getType()) {
            case TemplateTypesInterface::TYPE_TEXT:
                $part['type'] = MimeInterface::TYPE_TEXT;
                break;

            case TemplateTypesInterface::TYPE_HTML:
                $part['type'] = MimeInterface::TYPE_HTML;
                break;

            default:
                throw new LocalizedException(
                    new Phrase('Unknown template type')
                );
        }

        $mimePart = $this->mimePartInterfaceFactory->create(['content' => $content]);
        $data = array($mimePart);
        if ($this->_attachments) {
            foreach ($this->_attachments as $attachment) {
                if (strpos($attachment['name'], ".txt") === false) {
                    $attachmentName = null;
                    if (isset($attachment['name'])) {
                        $attachmentName = $attachment['name'];
                    }
                    $part = array(
                        'content' => $attachment['body'],
                        'type' => MessageInterface::TYPE_HTML,
                        'fileName' => $attachmentName,
                        'disposition' => MimeInterface::DISPOSITION_ATTACHMENT,
                        'encoding' => \Zend\Mime\Mime::ENCODING_BASE64,
                    );
                    $mimePartLast = $this->mimePartInterfaceFactory->create($part);
                    array_push($data, $mimePartLast);
                }
            }
        }
        $mimeMessage = $this->mimeMessageInterfaceFactory->create(
            ['parts' => $data]
        );
        $this->messageData['body'] = $mimeMessage;
//        $this->messageData['to'] = [$addressTo];

        $this->messageData['subject'] = html_entity_decode(
            (string)$template->getSubject(),
            ENT_QUOTES
        );
        $this->message = $this->emailMessageInterfaceFactory->create($this->messageData);
        return $this;
    }

    /**
     * Reset object state
     *
     * @return \Magento\Framework\Mail\Template\TransportBuilder
     */
    public function reset()
    {
        $this->messageData = [];
        $this->templateIdentifier = null;
        $this->templateVars = null;
        $this->templateOptions = null;
        return $this;
    }

    /**
     * Handles possible incoming types of email (string or array)
     *
     * @param string $addressType
     * @param string|array $email
     * @param string|null $name
     *
     * @return void
     * @throws InvalidArgumentException
     */
    private function addAddressByType(string $addressType, $email, $name = null): void
    {
        $this->addressConverter = $this->objectManager->get('Magento\Framework\Mail\AddressConverter');
        if (is_string($email)) {
            $this->messageData[$addressType][] = $this->addressConverter->convert($email, $name);
            return;
        }
        $convertedAddressArray = $this->addressConverter->convertMany($email);
        if (isset($this->messageData[$addressType])) {
            $this->messageData[$addressType] = array_merge(
                $this->messageData[$addressType],
                $convertedAddressArray
            );
        }
    }

    public function setAttachment($attachments)
    {
        $this->_attachments = $attachments;
    }
}
