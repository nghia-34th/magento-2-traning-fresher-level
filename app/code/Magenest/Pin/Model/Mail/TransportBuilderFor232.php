<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_magento232_dev extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_magento232_dev
 */

namespace Magenest\Pin\Model\Mail;


class TransportBuilderFor232 extends \Magento\Framework\Mail\Template\TransportBuilder
{
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param $params
     * @param bool|\Magento\Framework\Mail\TransportInterface $transport
     * @return $this
     * @throws \Exception
     */
    public function createAttachment($params, $transport = false)
    {
        $type = isset($params['type']) ? $params['type'] : \Zend_Mime::TYPE_OCTETSTREAM;

        if ($transport === false) {
            $this->message->createAttachment(
                $params['body'],
                $type,
                \Zend_Mime::DISPOSITION_ATTACHMENT,
                \Zend_Mime::ENCODING_BASE64,
                $params['name']
            );
        } else {
            $this->addAttachment($params, $transport);
        }

        return $this;
    }

    /**
     * @param $params
     * @param $transport \Magento\Framework\Mail\TransportInterface
     * @throws \Exception
     */
    public function addAttachment($params, $transport)
    {
        $zendPart = $this->createZendMimePart($params);
        $parts = $transport->getMessage()->getBody()->addPart($zendPart);
        $transport->getMessage()->setBody($parts);
    }

    protected function createZendMimePart($params)
    {
        if (class_exists('Zend\Mime\Mime') && class_exists('Zend\Mime\Part')) {
            $type = isset($params['type']) ? $params['type'] : \Zend\Mime\Mime::TYPE_OCTETSTREAM;

            if (isset($params['body'])) {
                $part = new \Zend\Mime\Part($params['body']);
            } else {
                $part = new \Zend\Mime\Part(null);
            }
            if (isset($params['name'])) {
                $part->filename = $params['name'];
            } else {
                $part->filename = null;
            }
            $part->type = $type;
            $part->disposition = \Zend\Mime\Mime::DISPOSITION_ATTACHMENT;
            $part->encoding = \Zend\Mime\Mime::ENCODING_BASE64;
            return $part;
        } else {
            throw new \Exception("Missing Zend Framework Source");
        }
    }

    public function prepare()
    {
        return $this->prepareMessage();
    }

    public function reset()
    {
        return parent::reset(); // TODO: Change the autogenerated stub
    }
}
