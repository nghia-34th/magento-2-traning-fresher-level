<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 27/07/2016
 * Time: 14:56
 */

namespace Magenest\Pin\Model;

use Magenest\Pin\Model\Instruction;


class Instructions extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * @var \Magenest\Pin\Model\Instruction
     */
    protected $_instruction;

    /**
     * Instructions constructor.
     * @param \Magenest\Pin\Model\Instruction $instruction
     */
    public function __construct(
        Instruction $instruction
    ) {
        $this->_instruction = $instruction;
    }

    /**
     * @return \string[]
     */
    public function getAvailableTemplate()
    {
        $listInstructions = $this->_instruction->getCollection()->getData();

        $result = [];
        foreach ($listInstructions as $listInstruction) {
            $result[] =
                [
                    'value' => $listInstruction['id'],
                    'label' => $listInstruction['name'],
                ];
        }

        return $result;
    }

    /**
     * @param bool $withEmpty
     * @return array|\string[]
     */
    public function getAllOptions($withEmpty = true)
    {
        $options = $this->getAvailableTemplate();

        if ($withEmpty) {
            array_unshift(
                $options,
                array(
                    'value' => '0',
                    'label' => '-- Please Select --',
                )
            );
        }

        return $options;
    }
}
