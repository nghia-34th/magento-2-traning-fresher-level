<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 02/10/2015
 * Time: 11:10
 */

namespace Magenest\Pin\Model;

use Magenest\Pin\Model\ResourceModel\Pin as Resource;
use Magenest\Pin\Model\ResourceModel\Pin\Collection;

class Pin extends \Magento\Framework\Model\AbstractModel
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;
    const STATUS_DELIVERED = 3;
    const STATUS_SOLD = 4;
    const SEND_PIN_WHEN_ORDER_ITEM_STATUS_IS_INVOICE = 9;
    const SEND_PIN_WHEN_ORDER_ITEM_STATUS_IS_PENDING = 1;

    const TYPE_TEXT = 1;
    const TYPE_FILE = 2;
    /**
     * @var string
     */
    protected $_eventPrefix = 'pin_pin';
    /**
     * @var string
     */
    protected $_eventObject = 'pin';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param Resource $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        Resource $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('Magenest\Pin\Model\ResourceModel\Pin');
        $this->setIdFieldName('id');
    }

    public function getIsPinProduct($productId)
    {
        $resource = $this->_getResource();
        $isPinProduct = $resource->getIsPinProduct($productId);

        return $isPinProduct;
    }

    /**
     * if type is 0 then get all the pin text
     * type is 1 get all the PIN file
     * if type is not set then get all
     * @param $productId
     * @param $limit
     * @param int $type
     * @return mixed
     */
    public function getAvailablePin($productId, $limit, $type = 0)
    {
        $resource = $this->getCollection();

        return $resource->getAvailablePin($productId, null, $limit);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getImgDb($name)
    {
        return $this->getResource()->getImgDb($name);
    }
}
