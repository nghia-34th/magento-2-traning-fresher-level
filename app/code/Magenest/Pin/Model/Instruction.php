<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 27/07/2016
 * Time: 15:17
 */
namespace Magenest\Pin\Model;

use Magenest\Pin\Model\ResourceModel\Instruction as Resource;
use Magenest\Pin\Model\ResourceModel\Instruction\Collection;

class Instruction extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'pin_instruction';

    /**
     * Instruction constructor.
     *
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param Resource $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        Resource $resource,
        Collection $resourceCollection,
        array $data = []
    ) {

        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function getDataCollection()
    {
        $collection = $this->getCollection()->addFieldToSelect('*');

        return $collection;
    }
}
