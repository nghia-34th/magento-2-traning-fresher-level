<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 19/12/2018
 * Time: 11:36
 */
namespace Magenest\Pin\Model\Observer\Pin;

use Magenest\Pin\Model\Pin;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class OriPinFile implements ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        try {
            /** @var  $pin Pin*/
            $pin = $observer->getEvent()->getPin();
            if ($pin->isObjectNew() && !$pin->getOriFileName()) {
                $pin->setOriFileName($pin->getId());
                $pin->save();
            }
        } catch (\Exception $exception) {
            $this->logger->critical($exception->getMessage());
        }
    }
}
