<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 23/11/2015
 * Time: 14:05
 */

namespace Magenest\Pin\Model\Observer\Order;

use Magenest\Pin\Helper\Data as PinHelper;
use Magenest\Pin\Model\BlobFactory;

class AbstractObserver
{
    /**
     * @var \Magento\Framework\Model\Context
     */
    protected $_context;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magenest\Pin\Model\PurchasedFactory|\Magento\Downloadable\Model\Link\PurchasedFactory
     */
    protected $_purchasedFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magenest\Pin\Model\PinFactory
     */
    protected $_pinFactory;

    /**
     * @var PinHelper
     */
    protected $_helper;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    protected $_file;

    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $_fileFactory;

    /**
     * @var BlobFactory
     */
    protected $_blobFactory;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magenest\Pin\Model\PurchasedFactory $purchasedFactory
     * @param \Magenest\Pin\Model\PinFactory $pinFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param PinHelper $helper
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\Filesystem\Driver\File $file
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param BlobFactory $blobFactory
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magenest\Pin\Model\PurchasedFactory $purchasedFactory,
        \Magenest\Pin\Model\PinFactory $pinFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        PinHelper $helper,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Filesystem\Driver\File $file,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magenest\Pin\Model\BlobFactory $blobFactory,
        \Magento\Framework\Event\ManagerInterface $eventManager
    ) {
        $this->_fileFactory = $fileFactory;
        $this->_file = $file;
        $this->_scopeConfig = $scopeConfig;
        $this->_purchasedFactory = $purchasedFactory;
        $this->_pinFactory = $pinFactory;
        $this->_productFactory = $productFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_context = $context;
        $this->_helper = $helper;
        $this->_logger = $context->getLogger();
        $this->_orderFactory = $orderFactory;
        $this->_blobFactory = $blobFactory;
        $this->_eventManager = $eventManager;
    }
}
