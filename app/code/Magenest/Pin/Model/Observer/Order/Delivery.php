<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 28/12/2018
 * Time: 13:35
 */

namespace Magenest\Pin\Model\Observer\Order;

use Magenest\Pin\Model\Pin;
use Magenest\Pin\Model\Purchased;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;

class Delivery extends \Magenest\Pin\Model\Observer\Order\AbstractObserver implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        try {
            $order = $observer->getEvent()->getOrder();
            if (!($order instanceof Order) || !$order->getId()) {
                return;
            }
            if ($order->getState() == Order::STATE_CANCELED || $order->getState() == Order::STATE_CLOSED) {
                return;
            }

            $configStatus = $this->_scopeConfig->getValue(
                "pin/pin/pin_order_item_status",
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $order->getStoreId()
            );

            $purchaseCollection = $this->getAllowToSendKeyPurchaseCollection($order, $configStatus);
            foreach ($purchaseCollection as $purchase) {
                try {
                    $this->_deliveryPin($purchase);
                    if ($this->isPurchaseDelivered($purchase)) {
                        $this->sendLicenseEmail($order, $purchase);
                        $emailFlag = 1;
                    }
                } catch (\Exception $exception) {
                    $this->_logger->critical($exception->getMessage());
                }
            }
            if (isset($emailFlag)) {
                $order->setData('sent_pin_email', 'Yes');
                $order->save();
            }
        } catch (\Exception $exception) {
            $this->_logger->critical($exception->getMessage());
        }
    }

    protected function isPurchaseDelivered(Purchased $purchased)
    {
        if (($purchased->getPinCode() || $purchased->getPinFileId()) && !$purchased->getOrigData('pin_file_id')) {
            return true;
        }
        return false;
    }

    protected function sendLicenseEmail(Order $order, Purchased $purchase)
    {
        $customerEmail = $order->getBillingAddress()->getEmail();
        $customerName = $order->getBillingAddress()->getFirstname() . ' Delivery.php' . $order->getBillingAddress()->getLastname();
        $storeId = $order->getStoreId();
        if ($purchase->getPinCode()) {
            $body = $purchase->getPinCode();
            $name = $purchase->getProductName() . '.txt';
        } else {
            $blob = $this->_blobFactory->create()->load($purchase->getPinFileId());
            $code = $this->_pinFactory->create()->load($purchase->getPinFileId());
            $name = $code->getOriFileName();
            $body = $blob->getPinFile();
        }
        $purchaseItemsToDelivered = [];
        $purchaseItemsToDelivered[] = ['body' => $body, 'name' => $name];
        $this->_helper->sendNotificationEmail($storeId, $customerName, $customerEmail, $purchaseItemsToDelivered, $purchase->getProductId(), $order->getId());
    }

    /**
     * @param Order $order
     * @param $configStatus
     * @return array|\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    protected function getAllowToSendKeyPurchaseCollection(Order $order, $configStatus)
    {
        $allowedPurchaseStatus = [Purchased::STATUS_PENDING];
        $allowOrderItemId = [];
        /** @var  $item Order\Item */
        foreach ($order->getItems() as $item) {
            if ($item->getStatusId() == $configStatus) {
                $allowOrderItemId[] = $item->getId();
            }
        }
        if (count($allowOrderItemId) > 0) {
            $purchaseCollection = $this->_purchasedFactory->create()->getCollection()
                ->addFieldToFilter('order_item_id', ['in' => $allowOrderItemId])
                ->addFieldToFilter('order_id', $order->getId())
                ->addFieldToFilter('status', ['in' => $allowedPurchaseStatus]);
            return $purchaseCollection;
        }
        return [];
    }

    /**
     * Delivery
     *
     * @param $purchasedPIN
     */
    protected function _deliveryPin($purchasedPIN)
    {
        //is the a pin for this item if there is not then ignore it
        $isDeliveryYet = false;

        if ($purchasedPIN->getData('pin_code') || $purchasedPIN->getFileExtension()) {
            $isDeliveryYet = true;
        }

        if (!$isDeliveryYet) {
            //todo send email to notify customer
            try {
                $product = $this->_productFactory->create()->load($purchasedPIN->getProductId());
                $productId = $product->getId();
                $instructionId = $product->getInstructions();

                /** @var  $availablePINs \Magenest\Pin\Model\ResourceModel\Pin\Collection */
//                $availablePINs = $pinModel->getAvailablePin($productId, null, 1);
                $availablePINs = $this->_pinFactory->create()->getCollection()
                    ->addFieldToFilter('order_item_id', $purchasedPIN->getOrderItemId())
                    ->addFieldToFilter('product_id', $productId)
                    ->addFieldToFilter('status', \Magenest\Pin\Model\Status::STATUS_DELIVERED);
                if ($availablePINs->getSize() == 0) {
                    $availablePINs = $this->_pinFactory->create()->getCollection()
                        ->addFieldToFilter('product_id', $productId)
                        ->addFieldToFilter('status', \Magenest\Pin\Model\Status::STATUS_ENABLED);
                }
                if ($availablePINs->getSize() > 0) {
                    $deliveryPin = $availablePINs->getFirstItem();
                    if ($deliveryPin->getCode()) {
                        $purchasedPIN->setPinFileId($deliveryPin->getId());
                        $purchasedPIN->setPinCode($deliveryPin->getCode());
                        $purchasedPIN->setFileExtension('text')->save();
                    } elseif ($deliveryPin->getData('ori_file_name')) {
                        $purchasedPIN->setPinFileId($deliveryPin->getId());
                        $purchasedPIN->setFileExtension($deliveryPin->getData('ori_file_name'));
                        if ($deliveryPin->getData('file_extension') == 'database') {
                            $purchasedPIN->setFileExtension('database');
                        }
                    }
                    $purchasedPIN->setInstructionId($instructionId);
                    $purchasedPIN->setStatus(Purchased::STATUS_COMPLETE);
                    $purchasedPIN->save();
                    //set that the pin is used
                    $id = $deliveryPin->getData('id');
//                $deliveryPin=$this->_pinFactory->create()->load($deliveryPin->getData('id'));
                    $pin = $this->_pinFactory->create()->load($id);
                    $pin->setOrderItemId($purchasedPIN->getOrderItemId());
                    $pin->setStatus(\Magenest\Pin\Model\Status::STATUS_SOLD);
                    $pin->save();
                    $this->_eventManager->dispatch('save_qty_product', ['product' => $product]);
                }
            } catch (\Exception $e) {
                $this->_logger->debug($e->getMessage());
            }
        }
    }
}
