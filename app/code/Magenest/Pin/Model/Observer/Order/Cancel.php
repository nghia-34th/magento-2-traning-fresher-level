<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 24/12/2018
 * Time: 14:19
 */

namespace Magenest\Pin\Model\Observer\Order;

use Magenest\Pin\Model\Pin;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;

class Cancel implements ObserverInterface
{
    /**
     * @var \Magenest\Pin\Model\PinFactory
     */
    protected $pinFactory;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @param \Magenest\Pin\Model\PinFactory $pinFactory
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magenest\Pin\Model\PinFactory $pinFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->logger = $logger;
        $this->pinFactory = $pinFactory;
    }

    public function execute(Observer $observer)
    {
        try {
            /* @var $order \Magento\Sales\Model\Order */

            $order = $observer->getEvent()->getOrder();
            if (!($order instanceof Order) || !$order->getId()) {
                //order not saved in the database
                return $this;
            }
            if ($this->isJustCancel($order)) {
                $this->changeOrderedKeyToAvailable($order);
            }
        } catch (\Exception $exception) {
            $this->logger->critical($exception->getMessage());
        }
    }

    private function isJustCancel(Order $order)
    {
        return $order->getState() == Order::STATE_CANCELED && $order->getOrigData('state') != $order->getState();
    }

    private function changeOrderedKeyToAvailable(Order $order)
    {
        $items = $order->getAllItems();
        $itemIds = [];
        foreach ($items as $item) {
            $itemIds[] = $item->getId();
        }
        if (count($itemIds) > 0) {
            $collection = $this->pinFactory->create()->getCollection()
                ->addFieldToFilter('order_item_id', ['in' => $itemIds])
                ->addFieldToFilter('status', Pin::STATUS_DELIVERED);
            foreach ($collection as $pin) {
                $pin->setStatus(Pin::STATUS_ENABLED);
                $pin->setOrderItemId(null);
                $pin->save();
            }
        }
    }
}
