<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 23/11/2015
 * Time: 13:52
 */

namespace Magenest\Pin\Model\Observer\Order;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Quote extends \Magenest\Pin\Model\Observer\Order\AbstractObserver implements ObserverInterface
{
    const LICENSE = 'license';
    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        foreach ($order->getAllItems() as $item) {
            /* @var $item \Magento\Sales\Model\Order\Item */
            if ($item->getProductType() == self::LICENSE
                || $item->getRealProductType() == self::LICENSE
            ) {
                $this->_checkoutSession->setHasPinProducts(true);
                break;
            }
        }
    }
}
