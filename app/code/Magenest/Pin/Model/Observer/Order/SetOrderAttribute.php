<?php
/**
 * Created by PhpStorm.
 * User: ninhvu
 * Date: 04/05/2018
 * Time: 08:35
 */
namespace Magenest\Pin\Model\Observer\Order;

class SetOrderAttribute implements \Magento\Framework\Event\ObserverInterface
{
    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /* @var $order \Magento\Sales\Model\Order */
        $order = $observer->getOrder();
        if ($order->getSentPinEmail()=="") {
            $order->setSentPinEmail("No");
        }
        return $this;
    }
}
