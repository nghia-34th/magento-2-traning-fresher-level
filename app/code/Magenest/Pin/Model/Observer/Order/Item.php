<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 23/11/2015
 * Time: 13:51
 */

namespace Magenest\Pin\Model\Observer\Order;

use Magenest\Pin\Model\Pin;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Item extends \Magenest\Pin\Model\Observer\Order\AbstractObserver implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        try {
            /** @var  $orderItem  \Magento\Sales\Model\Order\Item */
            $orderItem = $observer->getEvent()->getItem();
            if (!$orderItem->getId()) {
                //order not saved in the database
                return $this;
            }

            /** @var  $product \Magento\Catalog\Model\Product */
            $product = $orderItem->getProduct();

            $productId = $product->getId();
            $productName = $product->getName();
            $productSku = $product->getSku();
            $qty = $orderItem->getQtyOrdered();

            $pinModel = $this->_pinFactory->create();

            $purchasedModel = $this->_purchasedFactory->create();
            $isPinProduct = false;

            /** @var  $order \Magento\Sales\Model\Order */
            $order = $orderItem->getOrder();
            $customerId = $order->getCustomerId();

            if (!$customerId) {
                $customerId = 0;
            }

            /** @var  $customer \Magento\Customer\Model\Customer */
            $customer = $order->getCustomer();
            if ($customer) {
                $customerName = $customer->getName();
                $customerEmail = $customer->getEmail();
            }
            //Todo delete these comment
            //is product is pin product
            //check whether have record in purchased for this order item yet
            //if it is pin product then get the qty of item
            //get the select qty
            //get the available pin base on qty and product id
            //
            $isPinProduct = ($product->getTypeId() == 'license') ? true : false;

            if ($isPinProduct) {
                $isProcessed = $purchasedModel->getIsOrderItemProcessed($orderItem->getId());

                if (!$isProcessed) {
                    //set a placeholder
                    for ($i = 0; $i < $qty; $i++) {
                        $purchaseObj = $this->_purchasedFactory->create();

                        $purchaseObj->setData(
                            ['order_id' => $orderItem->getOrder()->getId(),
                                'order_increment_id' => $orderItem->getOrder()->getIncrementId(),
                                'order_item_id' => $orderItem->getId(),
                                'status' => 0,
                                'customer_id' => $customerId,
                                'product_id' => $productId,
                                'product_name' => $productName,
                                'product_sku' => $productSku
                            ]
                        )->save();
                        $pinObj = $this->_pinFactory->create()->getCollection()->addFieldToFilter('status', Pin::STATUS_ENABLED)
                            ->addFieldToFilter("product_id", $productId)->getFirstItem();
                        if ($pinObj->getId()) {
                            $pinObj->setStatus(Pin::STATUS_DELIVERED);
                            $pinObj->setOrderItemId($orderItem->getId());
                            $pinObj->save();
                        }
                    }
                }
            }
            $this->_eventManager->dispatch('save_qty_product', ['product' => $product]);
        } catch (\Exception $exception) {
            $this->_logger->critical($exception->getMessage());
        }
    }
}
