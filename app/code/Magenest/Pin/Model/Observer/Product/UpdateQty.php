<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 03/01/2019
 * Time: 11:36
 */

namespace Magenest\Pin\Model\Observer\Product;

use Magenest\Pin\Model\Pin;
use Magento\Catalog\Model\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class UpdateQty implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var \Magenest\Pin\Model\PinFactory
     */
    protected $pinFactory;
    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $stockRegistry;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magenest\Pin\Model\PinFactory $pinFactory
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\Framework\App\RequestInterface $request
     * @param LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magenest\Pin\Model\PinFactory $pinFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Framework\App\RequestInterface $request,
        LoggerInterface $logger
    ) {
        $this->request = $request;
        $this->stockRegistry = $stockRegistry;
        $this->pinFactory = $pinFactory;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
    }

    public function execute(Observer $observer)
    {
        try {
            $product = $observer->getProduct();
            if (!($product instanceof Product)) {
                return;
            }
            if ($product->getTypeId() == 'license' && $this->isAutoUpdateQtyEnable()) {
                $this->updateProductQty($product);
            }
        } catch (\Exception $exception) {
            $this->logger->critical($exception->getMessage());
        }
    }

    protected function isAutoUpdateQtyEnable()
    {
        return $this->scopeConfig->getValue('pin/inventory/auto_update') == 1;
    }

    public function updateProductQty(Product $product)
    {
        if ($this->isSalableQtyUsed()) {
            $qty = $this->getAmountOfAvailableAndPlacedOrderKey($product);
        } else {
            $qty = $this->getAmountOfAvailableKey($product);
        }
        $stockItem = $this->stockRegistry->getStockItemBySku($product->getSku());
        $stockItem->setQty($qty);
        $stockItem->setIsInStock((bool)$qty);
        $this->stockRegistry->updateStockItemBySku($product->getSku(), $stockItem);
        $this->changeQtyRequest($stockItem);
        $product->setStockData($stockItem->getStoredData());
    }

    private function changeQtyRequest($stockItem)
    {
        $requestParam = $this->request->getParams();
        $productData = $this->request->getParam('product', []);
        $productData['quantity_and_stock_status'] = [];
        $requestParam['product'] = $productData;
        $this->request->setParams($requestParam);
    }

    private function isSalableQtyUsed()
    {
        if (class_exists('\Magento\InventorySales\Model\GetProductSalableQty')) {
            return true;
        }
        return false;
    }

    private function getAmountOfAvailableKey($product)
    {
        $pinCollection = $this->pinFactory->create()
            ->getCollection()
            ->addFieldToFilter('product_id', $product->getId())
            ->addFieldToFilter('status', Pin::STATUS_ENABLED);
        return $pinCollection->getSize();
    }

    private function getAmountOfAvailableAndPlacedOrderKey($product)
    {
        $pinCollection = $this->pinFactory->create()
            ->getCollection()
            ->addFieldToFilter('product_id', $product->getId())
            ->addFieldToFilter('status', ['in' => [Pin::STATUS_ENABLED, Pin::STATUS_DELIVERED]]);
        return $pinCollection->getSize();
    }
}
