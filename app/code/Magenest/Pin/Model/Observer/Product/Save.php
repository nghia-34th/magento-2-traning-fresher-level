<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 02/10/2015
 * Time: 11:05
 */

namespace Magenest\Pin\Model\Observer\Product;

use Composer\Package\Archiver\ZipArchiver;
use Magento\Catalog\Model\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magenest\Pin\Model\PinFactory;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem\Driver\File as DriverFile;
use Psr\Log\LoggerInterface;
use Magenest\Pin\Helper\File as FileHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\CatalogInventory\Api\StockRegistryInterface;


class Save implements ObserverInterface
{

    const LICENSE = 'license';
    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var PinFactory
     */
    protected $_pinFactory;

    /**
     * @var DriverFile
     */
    protected $driveFile;

    /**
     * @var FileHelper
     */
    protected $_fileHelper;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var
     */
    protected $varDirectory = null;

    /**
     * @var StockRegistryInterface
     */
    protected $_stock;

    /**
     * @var \Magento\Framework\File\Csv
     */
    protected $csvReader;

    /**
     * @var \Magenest\Pin\Model\Csv\IteratorReader
     */
    protected $iteratorReader;

    /**
     * @var \Magento\Framework\Event\Manager
     */
    protected $eventManager;

    /**
     * Save constructor.
     *
     * @param StockRegistryInterface $stock
     * @param DriverFile $driveFile
     * @param PinFactory $pinFactory
     * @param RequestInterface $request
     * @param LoggerInterface $logger
     * @param FileHelper $fileHelper
     * @param ScopeConfigInterface $scopeConfig
     * @param ManagerInterface $messageManager
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param \Magento\Framework\File\Csv $csv
     * @param \Magenest\Pin\Model\Csv\IteratorReader $iteratorReader
     * @param \Magento\Framework\Event\Manager $manager
     */
    public function __construct(
        StockRegistryInterface $stock,
        DriverFile $driveFile,
        PinFactory $pinFactory,
        RequestInterface $request,
        LoggerInterface $logger,
        FileHelper $fileHelper,
        ScopeConfigInterface $scopeConfig,
        ManagerInterface $messageManager,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Framework\File\Csv $csv,
        \Magenest\Pin\Model\Csv\IteratorReader $iteratorReader,
        \Magento\Framework\Event\Manager $manager
    ) {
        $this->csvReader = $csv;
        $this->_pinFactory = $pinFactory;
        $this->_request = $request;
        $this->driveFile = $driveFile;
        $this->_fileHelper = $fileHelper;
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
        $this->messageManager = $messageManager;
        $this->_stock = $stock;
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->iteratorReader = $iteratorReader;
        $this->eventManager = $manager;
    }

    /**
     * @param $observer \Magento\Framework\Event\Observer;
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var $product \Magento\Catalog\Model\Product */
        $product = $observer->getProduct();
        $productId = $product->getId();
        if (!$productId) {
            return;
        }
        $isPinProduct = $product->getTypeId();
        $params = $this->_request->getParam('product');
        if ($isPinProduct == self::LICENSE) {
            try {
                if (isset($params['game_license'])) {
                    $pins = $params['game_license'];
                    if (is_array($pins)) {
                        foreach ($pins as $pin) {
                            try {
                                $this->createTextKey($product, $pin);
                            } catch (\Exception $exception) {
                                $this->messageManager->addErrorMessage($exception->getMessage());
                            }
                        }
                    }
                }
                if (isset($params['image_import'])) {
                    $pins = $params['image_import'];
                    foreach ($pins as $pin) {
                        try {
                            $this->createImageKey($product, $pin);
                        } catch (\Exception $exception) {
                            $this->messageManager->addErrorMessage($exception->getMessage());
                        }
                    }
                }
                if (isset($params['zip_import'])) {
                    $zips = $params['zip_import'];
                    foreach ($zips as $zip) {
                        try {
                            $this->createKeyFromZipFile($product, $zip);
                        } catch (\Exception $exception) {
                            $this->messageManager->addErrorMessage($exception->getMessage());
                        }
                    }
                }
                if (isset($params['csv_import'])) {
                    foreach ($params['csv_import'] as $csv) {
                        try {
                            $this->importKeyByCsvFile($product, $csv);
                        } catch (\Exception $exception) {
                            $this->messageManager->addErrorMessage($exception->getMessage());
                        }
                    }

                }
                $this->eventManager->dispatch('save_qty_product', ['product' => $product]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $this->logger->critical($e->getMessage());
            }
        }
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _setQty($product)
    {
        $pinCollection = $this->_pinFactory->create()
            ->getCollection()
            ->addFieldToFilter('product_id', $product->getId())
            ->addFieldToFilter('status', 1);
        $qty = (int)$pinCollection->getSize();

        $stockItem = $this->_stock->getStockItemBySku($product->getSku());
        $stockItem->setQty($qty);
        $stockItem->setIsInStock((bool)$qty); // this line
        $this->_stock->updateStockItemBySku($product->getSku(), $stockItem);
    }

    /**
     * @param $product Product
     * @param $data
     * @throws \Exception
     */
    private function createTextKey($product, $data)
    {
        if ($data['pin_code']) {
            $pinModel = $this->_pinFactory->create();
            $data = [
                'product_name' => $product->getName(),
                'status' => 1,
                'product_id' => $product->getId(),
                'comments' => $data['pin_supplier'],
                'pin_type' => 1,
                'file_extension' => 'text',
                'code' => $data['pin_code']
            ];
            $pinModel->setData($data)->save();
        } else {
            $this->messageManager->addErrorMessage(__("Entry text code! "));
        }
    }

    /**
     * @param $product Product
     * @param $pin
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    private function createImageKey($product, $pin)
    {
        try {
            $pinModel = $this->_pinFactory->create();
            $productName = $product->getName();
            $productId = $product->getId();
            $data = [
                'product_name' => $productName,
                'status' => 1,
                'product_id' => $productId,
                'comments' => $pin['pin_supplier'],
                'pin_type' => 1
            ];
            if (isset($pin['pin_file'][0]['file'])) {
                $fileName = $pin['pin_file'][0]['file'];
                $data['ori_file_name'] = $fileName;
                $tmpPath = $this->_fileHelper->getMediaDir()->getAbsolutePath('pin/tmp');

                $tmpFilePath = $this->_fileHelper->getFilePath($tmpPath, $fileName);
                if (getimagesize($tmpFilePath)) {  // Check if file is an image
                    $imageFile['tmp_name'] = $tmpFilePath;
                    $imageFile['type'] = 'image/jpeg';
                    $imageFile['name'] = $pin['pin_file'][0]['name'];
                    $imageFile['error'] = '0';
                    $imageFile['size'] = $pin['pin_file'][0]['size'];

                    $fileUpload = $this->_fileUploaderFactory->create(['fileId' => $imageFile]);

                    $this->varDirectory = $this->_filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);

                    $uploadDir = $this->varDirectory->getAbsolutePath('importexport/');

                    $result = $fileUpload->save($uploadDir);
                    $filePath = $result['path'] . $result['file'];

                    $fileRead = new  \Magento\Framework\Filesystem\File\Read($filePath, $this->driveFile);

                    $content = $fileRead->readAll();
                    $data['pin_file'] = $content;
                    $data['file_extension'] = 'database';
                    $data['pin_type'] = 2;
                    $data['product_name'] = $productName;
                    $pinModel->setData($data)->save();
                } else {
                    $this->messageManager->addErrorMessage(__('Error in ' . $fileName . ' : The format file used is not supported to create key codes!'));
                }
            }
        } catch (AlreadyExistsException $exception) {
            $this->handleException($exception, $fileName);
        }
    }

    /**
     * @param $product Product
     * @param $zip
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws LocalizedException
     */
    private function createKeyFromZipFile($product, $zip)
    {
        if (isset($zip['pin_file'])) {
            $fileName = $zip['pin_file'][0]['file'];
//            $filePath = $this->uploadZipFileToFolder($zip);
            $tmpPath = $this->_fileHelper->getMediaDir()->getAbsolutePath('pin/tmp');
            $filePath = $this->_fileHelper->getFilePath($tmpPath, $fileName);
            $zipReader = new \ZipArchive();
            if ($zipReader->open($filePath) == true) {
                $extracUrl = $this->getExtractZipFolder();
                $zipReader->extractTo($extracUrl);
                $zipReader->close();
                if (is_file($filePath)) {
                    unlink($filePath);
                }

                if (is_dir($extracUrl)) {
                    if ($dh = opendir($extracUrl)) {
                        while (($file = readdir($dh)) !== false) {
                            if ($file != '.' && $file != '..' && is_dir($extracUrl . '/' . $file)) {
                                throw new LocalizedException(__("Invalid Zip Format"));
                            }
                            if (isset($zip['pin_supplier'])) {
                                $this->createImageKeyFormZipFile($product, $file, $extracUrl, $zip['pin_supplier']);
                            } else {
                                $this->createImageKeyFormZipFile($product, $file, $extracUrl, null);
                            }
                        }
                        closedir($dh);
                    }
                } else {
                    $this->messageManager->addErrorMessage(__("Please check The Zip File format"));
                }
                try {
                    $this->deleteDirectory($extracUrl);
                } catch (\Exception $exception) {
                    $this->logger->critical($exception->getMessage());
                }
            }
        }
    }

    private function deleteDirectory($dirPath)
    {
        $files = glob($dirPath . '/*');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
            if (is_dir($file)) {
                $this->deleteDirectory($file);
            }
        }
        rmdir($dirPath);
    }

    public function getExtractZipFolder()
    {
        if ($this->varDirectory === null) {
            $this->varDirectory = $this->_filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        }
        return $this->varDirectory->getAbsolutePath("importexport/" . time() . 'zip');
    }

    private function uploadZipFileToFolder($zipData)
    {
        $fileName = $zipData['pin_file'][0]['file'];
        $ext = pathinfo($fileName, PATHINFO_EXTENSION);
        if ($ext != 'zip') {
            throw new LocalizedException(__("$fileName is not a valid file"));
        }
        $tmpPath = $this->_fileHelper->getMediaDir()->getAbsolutePath('pin/tmp');
        $tmpFilePath = $this->_fileHelper->getFilePath($tmpPath, $fileName);
        $zipFile['tmp_name'] = $tmpFilePath;
        $zipFile['type'] = 'application/zip';
        $zipFile['name'] = $zipData['pin_file'][0]['name'];
        $zipFile['error'] = '0';
        $zipFile['size'] = $zipData['pin_file'][0]['size'];
        $uploader = $this->_fileUploaderFactory->create(['fileId' => $zipFile]);
        $uploader->setAllowedExtensions(array(
            'zip'
        ));
        $uploader->setAllowRenameFiles(false);
        $uploader->setFilesDispersion(false);
        $uploadDir = $this->getUploadDir();
        $result = $uploader->save($uploadDir);
        $filePath = $result['path'] . $result['file'];
        return $filePath;
    }

    /**
     * @param $product Product
     * @param $fileName
     * @param $directory
     * @param $comment
     */
    private function createImageKeyFormZipFile($product, $fileName, $directory, $comment)
    {
        try {
            $filePath = $directory . '/' . $fileName;
            if ($fileName != '.' && $fileName != '..' && getimagesize($filePath)) {
                $fp = fopen($filePath, 'r');
                $content = fread($fp, filesize($filePath));
                fclose($fp);
                if ($content) {
                    $data = [
                        'product_name' => $product->getName(),
                        'status' => 1,
                        'product_id' => $product->getId(),
                        'comments' => $comment,
                        'pin_type' => 2,
                        'ori_file_name' => $fileName
                    ];
                    $data['pin_file'] = $content;
                    $data['file_extension'] = 'database';
                    $pinModel = $this->_pinFactory->create()->setData($data);
                    $pinModel->save();
                }
            } else {
                if ($fileName != '.' && $fileName != '..') {
                    $this->messageManager->addError('Error in' . $fileName . ' : The format fileName used is not supported to create key codes!');
                }
            }
            if (is_file($filePath)) {
                unlink($filePath);
            }
        } catch (AlreadyExistsException $exception) {
            $this->handleException($exception, $fileName);
        }
    }

    private function getUploadDir()
    {
        if ($this->varDirectory === null) {
            $this->varDirectory = $this->_filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        }
        return $this->varDirectory->getAbsolutePath('importexport/');
    }


    private function handleException(AlreadyExistsException $exception, $fileName)
    {
        if ($exception instanceof AlreadyExistsException) {
            $this->messageManager->addError(__("Key $fileName is already existed in the system "));
        }
    }

    protected function importKeyByCsvFile($product, $csv)
    {
        $comment = $csv['pin_supplier'];
        $csvName = null;
        if (isset($csv['pin_file'])) {
            $csvPinFile = $csv['pin_file'];
            if (isset($csvPinFile[0])) {
                $csvPinFile0 = $csvPinFile[0];
                if (isset($csvPinFile0['file'])) {
                    $csvName = $csv['pin_file'][0]['file'];
                }
            }
        }
        $ext = pathinfo($csvName, PATHINFO_EXTENSION);
        if ($ext != 'csv') {
            throw new LocalizedException(__("$csvName is not a valid file"));
        }
        $tmpPath = $this->_fileHelper->getMediaDir()->getAbsolutePath('pin/tmp');
        $tmpFilePath = $this->_fileHelper->getFilePath($tmpPath, $csvName);
        $this->iteratorReader->initialize($tmpFilePath);
        while ($this->iteratorReader->nextRow()) {
            $rowValue = $this->iteratorReader->getCurrentRow();
            $this->createPinByCsvRecord($product, $rowValue, $comment);
        }
        try {
            unlink($tmpFilePath);
        } catch (\Exception $exception) {

        }
    }

    protected function createPinByCsvRecord($product, $rowValue, $comment)
    {
        $data['pin_code'] = null;
        if (isset($rowValue[0])) {
            $data['pin_code'] = $rowValue[0];
        }
        $data['pin_supplier'] = $comment;
        $this->createTextKey($product, $data);
    }
}
