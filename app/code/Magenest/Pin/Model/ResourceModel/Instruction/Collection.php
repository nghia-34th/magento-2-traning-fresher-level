<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 27/07/2016
 * Time: 15:19
 */

namespace Magenest\Pin\Model\ResourceModel\Instruction;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Initialize resource
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Magenest\Pin\Model\Instruction::class, \Magenest\Pin\Model\ResourceModel\Instruction::class);
    }
}
