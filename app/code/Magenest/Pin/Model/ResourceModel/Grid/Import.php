<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_Pin extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Pin
 */

namespace Magenest\Pin\Model\ResourceModel\Grid;


class Import extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{
    public function addFieldToFilter($field, $condition = null)
    {
        if ($field == $this->_idFieldName) {
            $field = 'main_table.' . $field;
        }

        return parent::addFieldToFilter($field, $condition);
    }
}
