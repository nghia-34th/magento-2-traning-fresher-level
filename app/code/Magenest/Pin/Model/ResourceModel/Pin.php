<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 02/10/2015
 * Time: 11:11
 */

namespace Magenest\Pin\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\AbstractModel;

class Pin extends AbstractDb
{
    const DATABASE = 'database';
    /**
     * Core event manager proxy
     *
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager = null;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magenest\Pin\Model\PurchasedFactory
     */
    protected $purchasedFactory;

    /**
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Psr\Log\LoggerInterface $loggerInterface
     * @param \Magenest\Pin\Model\PurchasedFactory $purchasedFactory
     * @param null $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magenest\Pin\Model\PurchasedFactory $purchasedFactory,
        $connectionName = null
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_logger = $loggerInterface;
        $this->purchasedFactory = $purchasedFactory;
        parent::__construct($context, $connectionName);
    }

    protected function _construct()
    {
        $this->_init('magenest_pin_code', 'id');
    }

    /**
     * @param AbstractModel $object
     * @return $this
     */
    public function _afterSave(AbstractModel $object)
    {
        if ($object->getPinFile() != '' && $object->getFileExtension() == self::DATABASE) {  // Pin record was inserted
            $data = $this->_prepareDataForTable($object, $this->getTable('magenest_file_blob'));

            $data['blob_id'] = $object->getId();
            $data['pin_file'] = $object->getPinFile();

            $table = $this->getTable($this->getConnection()->getTableName('magenest_file_blob'));
            $this->getConnection()->insert($table, $data);
        }
        $purchaseds = $this->purchasedFactory->create()->getCollection()
            ->addFieldToFilter('pin_file_id', $object->getId());
        if ($purchaseds->getSize() > 0) {
            foreach ($purchaseds as $purchased) {
                if ($object->getFileExtension() == "text") {
                    $purchased->setFileExtension("text")->save();
                    $purchased->setPinCode($object->getCode())->save();
                }
                if ($object->getFileExtension() == self::DATABASE) {
                    $purchased->setFileExtension(self::DATABASE)->save();
                    $purchased->setPinCode(null)->save();
                }
            }
        }
        return parent::_afterSave($object);
    }

    public function getImgDb($name)
    {
        $mainTable = $this->getMainTable();
        $dobTable = $this->getTable('magenest_file_blob');
        $adapter = $this->_getConnection('read');
        $select = $adapter->select()->from(['main_table' => $mainTable], '*')
            ->join(['blob_table' => $dobTable], 'main_table.id = blob_table.blob_id')
            ->where('main_table.ori_file_name="' . $name . '"');

        return $adapter->fetchAssoc($select);
    }
}
