<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 02/10/2015
 * Time: 11:11
 */

namespace Magenest\Pin\Model\ResourceModel\Pin;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order
     */
    protected $orderResource;

    /**
     * Initialize resource
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Magenest\Pin\Model\Pin::class, \Magenest\Pin\Model\ResourceModel\Pin::class);
    }

    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Sales\Model\ResourceModel\Order $orderResource,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->orderResource = $orderResource;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }

    public function addProductToFilter($product_id)
    {
        $this->addFieldToFilter('main_table.product_id', $product_id);

        return $this;
    }

    protected function _initSelect()
    {
        parent::_initSelect();

        $orderResourceConfig = $this->orderResource->getConnection()->getConfig();
        $orderSchema = isset($orderResourceConfig['dbname']) ? $orderResourceConfig['dbname'] : null;
        $this->getSelect()->joinleft(
            ['sales_item' => $this->orderResource->getTable('sales_order_item')],
            'main_table.order_item_id = sales_item.item_id',
            ['order_id'],
            $orderSchema
        )
            ->joinLeft(
                ["sales_order" => $this->orderResource->getTable("sales_order_grid")],
                "sales_item.order_id = sales_order.entity_id",
                ["customer_id", "customer_email", "increment_id"],
                $orderSchema
            );

        return $this;
    }

    /**
     * if type is 0 then get all the pin text
     * type is 1 get all the PIN file
     * if type is not set then get all
     * if limit is null then do not set limit
     * @param $productId
     * @param int $type
     * @param null $limit
     * @return $this
     */
    public function getAvailablePin($productId, $type = 0, $limit = null)
    {
        /** @var  $select \Magento\Framework\DB\Select */
        $select = $this->getSelect();

        if ($type == null) {
            $select->where(
                'main_table.product_id = ?',
                $productId
            );
        }
        $select->where('status=?', 1);
        $select->order('created_at asc');
        if ($limit != null) {
            $select->limit($limit);
        }

        return $this;
    }

    public function joinTableSaleItem()
    {
        $this->getSelect()->joinleft(
            ['sales_item' => $this->orderResource->getTable('sales_order_item')],
            'main_table.order_item_id = sales_item.item_id',
            ['order_id']
        )
            ->joinLeft(
                ["sales_order" => $this->orderResource->getTable("sales_order_grid")],
                "sales_item.order_id = sales_order.entity_id",
                ["customer_id", "customer_email", "increment_id"]
            );
    }

    public function addFieldToFilter($field, $condition = null)
    {
        if ($field == "status") {
            $field = "main_table.status";
            return parent::addFieldToFilter($field, $condition);
        }

        if ($field == "product_id") {
            $field = "main_table.product_id";
            return parent::addFieldToFilter($field, $condition);
        }

        return parent::addFieldToFilter($field, $condition);
    }
}