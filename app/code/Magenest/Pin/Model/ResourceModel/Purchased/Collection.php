<?php

/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 05/10/2015
 * Time: 13:27
 */

namespace Magenest\Pin\Model\ResourceModel\Purchased;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Init resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Magenest\Pin\Model\Purchased::class, \Magenest\Pin\Model\ResourceModel\Purchased::class);
    }

    public function getIsOrderItemProcessed($order_item_id)
    {
        $select = $this->getSelect();

        $select->where("order_item_id=?", $order_item_id);

        return $this;
    }
}
