<?php
/**
 * Created by PhpStorm.
 * User: ninhvu
 * Date: 02/05/2018
 * Time: 12:53
 */

namespace Magenest\Pin\Model\ResourceModel\Import;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Initialize resource
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Magenest\Pin\Model\Import::class, \Magenest\Pin\Model\ResourceModel\Import::class);
    }

    public function joinTablePinCode()
    {
        $this->getSelect()->joinleft(
            ['magenest_pin_code' => $this->getTable('magenest_pin_code')],
            'main_table.pin_id = magenest_pin_code.id',
            ['product_id','product_name',"code","status","comments",'pin_type']
        );
    }
}
