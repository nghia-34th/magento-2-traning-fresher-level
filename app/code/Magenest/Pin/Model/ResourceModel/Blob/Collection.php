<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 14/04/2017
 * Time: 10:34
 */

namespace Magenest\Pin\Model\ResourceModel\Blob;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Initialize resource
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Magenest\Pin\Model\Blob::class, \Magenest\Pin\Model\ResourceModel\Blob::class);
    }
}
