<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 27/07/2016
 * Time: 15:19
 */

namespace Magenest\Pin\Model\ResourceModel;

class Instruction extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_pin_instruction', 'id');
    }
}
