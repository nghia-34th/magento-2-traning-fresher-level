<?php
/**
 * Created by PhpStorm.
 * User: ninhvu
 * Date: 02/05/2018
 * Time: 12:52
 */

namespace Magenest\Pin\Model\ResourceModel;

class Import extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_import_history', 'id');
    }
}
