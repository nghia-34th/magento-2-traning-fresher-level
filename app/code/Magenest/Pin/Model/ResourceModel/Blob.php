<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 14/04/2017
 * Time: 10:32
 */

namespace Magenest\Pin\Model\ResourceModel;

class Blob extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_file_blob', 'blob_id');
    }

    public function insertData($id, $content)
    {
        $data['blob_id'] = $id;
        $data['pin_file'] = $content;

        $table = $this->getTable($this->getConnection()->getTableName('magenest_file_blob'));
        $this->getConnection()->insert($table, $data);
    }
}
