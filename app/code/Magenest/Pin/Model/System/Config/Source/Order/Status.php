<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 05/11/2015
 * Time: 09:58
 */

namespace Magenest\Pin\Model\System\Config\Source\Order;

class Status
{
    const STATUS_NOT_DELIVERED = 0;

    const STATUS_HALF_DELIVERED = 2;

    const STATUS_DELIVERED = 1;

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public function getOptionArray()
    {
        return [self::STATUS_NOT_DELIVERED => __('Not delivered'),
            self::STATUS_HALF_DELIVERED => __('Half delivered'),
            self::STATUS_DELIVERED => __('Delivered')];
    }

    /**
     * Retrieve option array with empty value
     *
     * @return string[]
     */
    public function getAllOptions()
    {
        $result = [];

        foreach ($this->getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     * @return string
     */
    public function getOptionText($optionId)
    {
        $options = $this->getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}