<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 07/10/2015
 * Time: 15:00
 */

namespace Magenest\Pin\Model\Pin\System\Config\Source;

class Itemstatus
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => \Magento\Sales\Model\Order\Item::STATUS_PENDING, 'label' => __('Pending')],
            ['value' => \Magento\Sales\Model\Order\Item::STATUS_INVOICED, 'label' => __('Invoiced')]
        ];
    }
}
