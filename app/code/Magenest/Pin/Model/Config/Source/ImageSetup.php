<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 13/10/2016
 * Time: 14:23
 */
namespace Magenest\Pin\Model\Config\Source;


class ImageSetup implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'database',
                'label' => __('Database'),
            ],
//            [
//                'value' => 'system',
//                'label' => __('System'),
//            ],
        ];
    }
}
