<?php
/**
 * Created by PhpStorm.
 * User: ninhvu
 * Date: 02/05/2018
 * Time: 12:51
 */
namespace Magenest\Pin\Model;

class Import extends \Magento\Framework\Model\AbstractModel
{
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource =
        null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection =
        null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }
    public function _construct()
    {
        $this->_init('Magenest\Pin\Model\ResourceModel\Import');
    }
}
