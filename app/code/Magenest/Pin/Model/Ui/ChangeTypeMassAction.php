<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_magento233_dev extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_magento233_dev
 */

namespace Magenest\Pin\Model\Ui;

class ChangeTypeMassAction extends \Magento\Ui\Component\Action
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     * @param null $actions
     */
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\UrlInterface $urlBuilder,
        array $components = array(),
        array $data = array(),
        $actions = null
    ) {
        parent::__construct($context, $components, $data, $actions);
        $this->urlBuilder = $urlBuilder;
        $this->request = $request;
    }

    public function prepare()
    {
        parent::prepare();
        $config = $this->getConfiguration();
        $config['url'] = $this->urlBuilder->getUrl($config['urlPath']);
        $this->setData('config', $config);
    }
}
