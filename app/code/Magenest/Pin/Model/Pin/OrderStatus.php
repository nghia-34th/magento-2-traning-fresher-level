<?php
/**
 * Created by PhpStorm.
 * User: ninhvu
 * Date: 02/11/2017
 * Time: 08:14
 */
namespace Magenest\Pin\Model\Pin;

class OrderStatus implements \Magento\Framework\Option\ArrayInterface
{
    const STATUS_DELIVERED  = 1;
    const STATUS_NOTDELIVERED  = 0;
    const STATUS_EXPIRED =2;

    public function toOptionArray()
    {
        return [
            ['value' => OrderStatus::STATUS_DELIVERED, 'label' => __('Delivered')],
            ['value' => OrderStatus::STATUS_NOTDELIVERED, 'label' => __('Not Delivered')],
            ['value' => OrderStatus::STATUS_EXPIRED, 'label' => __('Expired')],
        ];
    }
}
