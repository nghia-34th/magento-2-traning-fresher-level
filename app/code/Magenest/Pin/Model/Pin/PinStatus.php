<?php
/**
 * Created by PhpStorm.
 * User: ninhvu
 * Date: 25/04/2018
 * Time: 10:52
 */
namespace Magenest\Pin\Model\Pin;

class PinStatus implements \Magento\Framework\Option\ArrayInterface
{
    const STATUS_AVAILABLE = 1;
    const STATUS_NOTAVAILABLE = 2;
    const STATUS_ORDERED = 3;
    const STATUS_DELIVERED = 4;

    public function toOptionArray()
    {
        return [
            ['value' => PinStatus::STATUS_AVAILABLE, 'label' => __('Available')],
            ['value' => PinStatus::STATUS_NOTAVAILABLE, 'label' => __('Not Available')],
            ['value' => PinStatus::STATUS_ORDERED, 'label' => __('Ordered')],
            ['value' => PinStatus::STATUS_DELIVERED, "label" => __("Delivered")],
        ];
    }
}
