<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 26/10/2018
 * Time: 10:41
 */
namespace Magenest\Pin\Model\Pin;

use Magenest\Pin\Model\Pin;

class Type implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
            ['value' => Pin::TYPE_TEXT, 'label' => __('Text')],
            ['value' => Pin::TYPE_FILE, 'label' => __('File')],
        ];
    }
}
