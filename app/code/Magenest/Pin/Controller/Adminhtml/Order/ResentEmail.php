<?php
/**
 * Created by PhpStorm.
 * User: ninhvu
 * Date: 04/05/2018
 * Time: 09:47
 */

namespace Magenest\Pin\Controller\Adminhtml\Order;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;

class ResentEmail extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Magenest_Pin::manage_pin";

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * @var \Magenest\Pin\Model\PurchasedFactory
     */
    protected $purchasedFactory;

    /**
     * @var \Magenest\Pin\Model\BlobFactory
     */
    protected $_blobFactory;

    /**
     * @var \Magenest\Pin\Model\PinFactory
     */
    protected $_pinFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magenest\Pin\Helper\Data
     */
    protected $_helper;

    /**
     * @var ResultFactory
     */
    protected $redirect;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magenest\Pin\Model\PurchasedFactory $purchasedFactory
     * @param \Magenest\Pin\Model\BlobFactory $blobFactory
     * @param \Magenest\Pin\Model\PinFactory $pinFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magenest\Pin\Helper\Data $helper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magenest\Pin\Model\PurchasedFactory $purchasedFactory,
        \Magenest\Pin\Model\BlobFactory $blobFactory,
        \Magenest\Pin\Model\PinFactory $pinFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magenest\Pin\Helper\Data $helper
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->orderFactory = $orderFactory;
        $this->purchasedFactory = $purchasedFactory;
        $this->_blobFactory = $blobFactory;
        $this->_pinFactory = $pinFactory;
        $this->_productFactory = $productFactory;
        $this->_helper = $helper;
        $this->redirect = $context->getResultFactory();
    }

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $purchaseItemsToDelivered = array();
        $data = $this->getRequest()->getParams();
        $order = $this->orderFactory->create()->load($data['order_id']);
        $storeId = $order->getStoreId();
        $customerName = $order->getCustomerName();
        $customerEmail = $order->getCustomerEmail();
        $purchased = $this->purchasedFactory->create()->getCollection()->addFieldToFilter("order_id", $data['order_id']);
        foreach ($purchased as $link) {
            if ($link->getPinCode()) {
                $body = $link->getPinCode();
                $name = $link->getProductName() . '.txt';
            } elseif ($link->getPinFileId()) {
                $blob = $this->_blobFactory->create()->load($link->getPinFileId());
                $code = $this->_pinFactory->create()->load($link->getPinFileId());
                $name = $code->getOriFileName();
                $src = $blob->getPinFile();
                $body = $src;
            } else {
                $this->messageManager->addNotice(__("A pin code was not generated for `{$link->getProductName()}`. Email has not been resent."));
                continue;
            }
            $purchaseItemsToDelivered[] = array('body' => $body, 'name' => $name);
            $productId = $this->_productFactory->create()->getIdBySku($link->getProductSku());
            $this->_helper->sendNotificationEmail($storeId, $customerName, $customerEmail, $purchaseItemsToDelivered, $productId, $order->getId());
            $purchaseItemsToDelivered = array();
            $this->messageManager->addSuccess(__("Email has been resent for `{$link->getProductName()}``"));
        }
        $resultPage = $this->redirect->create(ResultFactory::TYPE_REDIRECT);
        return $resultPage->setPath("sales/order/view", ['order_id' => $data['order_id']]);
    }
}
