<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 30/10/2015
 * Time: 10:48
 */

namespace Magenest\Pin\Controller\Adminhtml\Order;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Magenest_Pin::manage_pin";

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {

        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Pin::pincode');
        $resultPage->addBreadcrumb(__('CMS'), __('CMS'));
        $resultPage->addBreadcrumb(__('Manage Pin Order'), __('Manage Pin Order'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Pin Order'));
//        $resultPage->addContent($resultPage->getLayout()->createBlock('Magenest\Pin\Block\Adminhtml\Order\Grid'));

        return $resultPage;
    }

}
