<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 04/01/2019
 * Time: 13:16
 */
namespace Magenest\Pin\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;

class SampleCsv extends Action
{
    const ADMIN_RESOURCE = "Magenest_Pin::manage_pin";

    public function __construct(Action\Context $context)
    {
        parent::__construct($context);
    }

    public function execute()
    {

        $dataSample = ['key1','key2','key3','key4','key5','key6','key7','key8'];
        $outputFile = "ProductSampleFile.csv";
        $handle = fopen($outputFile, 'w');
        foreach ($dataSample as $content) {
            $row = [$content];
            fputcsv($handle, $row);
        }
        $this->downloadCsv($outputFile);
    }

    public function downloadCsv($file)
    {
        if (file_exists($file)) {
            //set appropriate headers
            header('Content-Description: File Transfer');
            header('Content-Type: application/csv');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
        }
    }
}
