<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 13/05/2017
 * Time: 14:56
 */
namespace Magenest\Pin\Controller\Adminhtml\File;

use Magenest\Pin\Controller\Adminhtml\Report;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magenest\Pin\Model\BlobFactory;
use Magenest\Pin\Model\PinFactory;
use Psr\Log\LoggerInterface;

class Download extends Report
{
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var BlobFactory
     */
    protected $_blobFactory;

    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $_fileFactory;

    /**
     * @var PinFactory
     */
    protected $_pinFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * Download constructor.
     * @param BlobFactory $blobFactory
     * @param ScopeConfigInterface $scopeConfigInterface
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param PinFactory $pinFactory
     * @param LoggerInterface $loggerInterface
     * @param Context $context
     */
    public function __construct(
        BlobFactory $blobFactory,
        ScopeConfigInterface $scopeConfigInterface,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        PinFactory $pinFactory,
        LoggerInterface $loggerInterface,
        Context $context
    ) {

        $this->_logger=$loggerInterface;
        $this->_pinFactory=$pinFactory;
        $this->_fileFactory=$fileFactory;
        $this->_blobFactory=$blobFactory;
        $this->_scopeConfig=$scopeConfigInterface;
        parent::__construct($context);
    }

    public function execute()
    {
        $fileName=$this->getRequest()->getParam('name');
        if ($fileName!='database') {
            $desDir = $this->_scopeConfig->getValue('pin/directory/path');
            $fileUrl =$desDir.'/'.$fileName;

            $fp = fopen($fileUrl, 'r');
            $content = fread($fp, filesize($fileUrl));
            return $this->_fileFactory->create(
                $fileName,
                $content,
                DirectoryList::VAR_DIR
            );
        } elseif ($fileName=='database') {
            $id=$this->getRequest()->getParam('file_id');
            $blob=$this->_blobFactory->create()->load($id);
            $content = $blob->getPinFile();
            $pin=$this->_pinFactory->create()->load($id);
            $date = $this->_objectManager->get('Magento\Framework\Stdlib\DateTime\DateTime')->date('Y-m-d_H-i-s');
            $fileName = $pin->getOriFileName();
            return $this->_fileFactory->create(
                $fileName,
                $content,
                DirectoryList::VAR_DIR
            );
        } else {
            return $this->resultForwardFactory->create()->forward('noroute');
        }
    }
}
