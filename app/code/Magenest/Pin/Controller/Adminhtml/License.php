<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 11/10/2016
 * Time: 16:02
 */

namespace Magenest\Pin\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magenest\Pin\Model\PinFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magenest\Pin\Model\BlobFactory;
use Magento\Framework\Filesystem\Driver\File;
use Psr\Log\LoggerInterface;
use Magenest\Pin\Model\ResourceModel\Blob;

/**
 * Class License
 *
 * @package Magenest\Pin\Controller\Adminhtml
 */
abstract class License extends Action
{
    /**
     * @var \Magento\Framework\Registry|null
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var \Magento\UrlRewrite\Model\UrlRewrite
     */
    protected $_urlRewrite;

    /**
     * @var \Magento\Framework\View\LayoutFactory
     */
    protected $layoutFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var PinFactory
     */
    protected $_pinFactory;
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var BlobFactory
     */
    protected $_blobFactory;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    protected $_file;

    protected $_logger;

    /**
     * @var Blob
     */
    protected $_blobCollection;
    protected $_productFactory;
    /**
     * @var Filter
     */
    protected $_filter;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     * @param PinFactory $pinFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     * @param Filter $filter
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        PinFactory $pinFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        Filter $filter,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        ScopeConfigInterface $scopeConfigInterface,
        Filesystem $filesystem,
        UploaderFactory $uploaderFactory,
        BlobFactory $blobFactory,
        File $file,
        LoggerInterface $loggerInterface,
        ProductFactory $productFactory,
        Blob $blob
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->layoutFactory = $layoutFactory;
        $this->_pinFactory = $pinFactory;
        $this->_filter = $filter;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->_scopeConfig = $scopeConfigInterface;
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $uploaderFactory;
        $this->_blobFactory = $blobFactory;
        $this->_file = $file;
        $this->_logger = $loggerInterface;
        $this->_blobCollection = $blob;
        $this->_productFactory = $productFactory;
        parent::__construct($context);
    }


    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();

        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Pin::manage_license_code');
    }
}
