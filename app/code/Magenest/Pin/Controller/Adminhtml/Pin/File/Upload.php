<?php
/**
 *
 * Copyright © 2016 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Pin\Controller\Adminhtml\Pin\File;

use Magento\Framework\Controller\ResultFactory;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magenest\Pin\Helper\File as FileHelper;
use Magento\Backend\App\Action;

class Upload extends Action
{
    const ADMIN_RESOURCE = "Magenest_Pin::manage_pin";

    /**
     * Downloadable file helper.
     *
     * @var FileHelper
     */
    protected $_fileHelper;

    /**
     * @var UploaderFactory
     */
    private $uploaderFactory;

    /**
     * Upload constructor.
     *
     * @param Action\Context $context
     * @param UploaderFactory $uploaderFactory
     * @param FileHelper $fileHelper
     */
    public function __construct(
        Action\Context $context,
        UploaderFactory $uploaderFactory,
        FileHelper $fileHelper
    ) {
        parent::__construct($context);
        $this->uploaderFactory = $uploaderFactory;
        $this->_fileHelper = $fileHelper;
    }

    /**
     * Upload file controller action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $type = $this->getRequest()->getParam('type');
        $tmpPath = 'pin/tmp';
        try {
            $uploader = $this->uploaderFactory->create(['fileId' => $type]);
            $result = $this->_fileHelper->uploadFromTmp($tmpPath, $uploader);
            if (!$result) {
                throw new \Exception('File can not be moved from temporary folder to the destination folder.');
            }
            unset($result['tmp_name'], $result['path']);

            $result['cookie'] = [
                'name' => $this->_getSession()->getName(),
                'value' => $this->_getSession()->getSessionId(),
                'lifetime' => $this->_getSession()->getCookieLifetime(),
                'path' => $this->_getSession()->getCookiePath(),
                'domain' => $this->_getSession()->getCookieDomain(),
            ];
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }

        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}
