<?php
namespace Magenest\Pin\Controller\Adminhtml\Upload;

use Magenest\Pin\Model\PinFactory;
use Magento\Backend\App\Action;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Catalog\Model\Product;
use Magenest\Pin\Model\Pin;
class Save extends Action
{
    const ADMIN_RESOURCE = "Magenest_Pin::import_pin_file";

    /**
     * @var \Magento\ImportExport\Model\Import
     */
    protected $importBean;

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $uploaderFactory;

    protected $varDirectory;

    /**
     * @var \Magenest\Pin\Model\ImportFactory
     */
    protected $importFactory;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var StockRegistryInterface
     */
    protected $_stock;

    /**
     * @var PinFactory
     */
    protected $_pinFactory;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;
    /**
     * @var Magento\Catalog\Model\Product
     */
    private $product;
    /**
     * @var Pin
     */
    private $pin;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param \Magento\ImportExport\Model\Import $importModel
     * @param \Magento\ImportExport\Model\Import\Source\CsvFactory $sourceCsvFactory
     * @param \Magento\ImportExport\Model\Export\Adapter\CsvFactory $outputCsvFactory
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magenest\Pin\Model\ImportFactory $importFactory
     * @param ScopeConfigInterface $scopeConfig
     * @param StockRegistryInterface $stock
     * @param PinFactory $pinFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\ImportExport\Model\Import $importModel,
        \Magento\ImportExport\Model\Import\Source\CsvFactory $sourceCsvFactory,
        \Magento\ImportExport\Model\Export\Adapter\CsvFactory $outputCsvFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Magenest\Pin\Model\ImportFactory $importFactory,
        ScopeConfigInterface $scopeConfig,
        Product $product,
        Pin $pin,
        StockRegistryInterface $stock,
        PinFactory $pinFactory

    ) {
        parent::__construct($context);
        $this->importBean = $importModel;
        $this->sourceCsvFactory = $sourceCsvFactory;
        $this->uploaderFactory = $uploaderFactory;
        $this->outputCsvFactory = $outputCsvFactory;
        $this->filesystem = $filesystem;
        $this->importFactory = $importFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_stock= $stock;
        $this->_pinFactory = $pinFactory;
        $this->product = $product;
        $this->pin = $pin;
    }

    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('pin/code/index', ['_current' => false]);
        //upload file
        $message = '';

        try {
            $uploader = $this->uploaderFactory->create(['fileId' => 'import_file']);
            $this->varDirectory = $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
            $uploadDir = $this->varDirectory->getAbsolutePath('importexport/');
            $result = $uploader->save($uploadDir);
        } catch (\Exception $exception) {
            $this->messageManager->addError('Can\'t up load the file. Please try again.');
            return $resultRedirect->setPath('*/*/index', ['_current' => true]);
        }
        $extension = pathinfo($result['file'], PATHINFO_EXTENSION);

        $uploadedFile = $result['path'] . $result['file'];

        try {
            //get csv adapter
            $csvAdapter = \Magento\ImportExport\Model\Import\Adapter::findAdapterFor(
                $uploadedFile,
                $this->filesystem->getDirectoryWrite(DirectoryList::ROOT),
                ','
            );
        } catch (\Exception $e) {
            $this->messageManager->addError('This file extension is not supported. Please try again.');
            return $resultRedirect->setPath('*/*/index', ['_current' => true]);
        }


        try {
            $count=0;
            foreach ($csvAdapter as $rowNum => $record) {
                $pin_number = $record ['pin'];
                $product_sku = $record ['sku'];
                $status = $record ['status'];
                $comments = $record ['comments'];
                $product_id = ( int )$this->product->getIdBySku($product_sku);
                if ($product_id==0) {
                    $error = 'sku';
                }

                if ($product_id) {
                    $product = $this->product->load($product_id);
                    $product_name = $product->getName();
                    $model = $this->pin;
                    $insertData = array();
                    $insertData ['pin_type'] = 1;
                    $insertData ['product_id'] = $product_id;
                    $insertData ['product_name'] = $product_name;
                    $insertData ['code'] = $pin_number;
                    $insertData ['status'] = $status;
                    $insertData ['comments'] = $comments;
                    $insertData ['file_extension'] = 'text';
                    $model->setData($insertData);
                    $model->save();
                    $importHistory = $this->importFactory->create();
                    $importHistory->setPinId($model->getId());
                    $importHistory->save();
                    $count++;
//                    $confQty = $this->scopeConfig->getValue('pin/inventory/auto_update');
//                    $confQty = $this->scopeConfig->getValue('pin/inventory/auto_update');
//                    if ($confQty == 1) {
//                        $this->_setQty($product);
//                    }
                    $this->_eventManager->dispatch('save_qty_product', ['product' => $product]);
                } else {
                    if ($error='sku') {
                        $this->messageManager->addError("Product Sku \"".$product_sku."\"doesn't exist, please check uploaded file.");
                    } else {
                        $this->messageManager->addError('Please check permission file CSV.');
                    }
                }
            }
        } catch (\Exception $e) {
            $this->messageManager->addError('Please check sample file and update CSV file.');
            return $resultRedirect->setPath('*/*/index', ['_current' => true]);
        }
        $this->messageManager->addSuccess("Total ".$count." key(s) imported");
        return $resultRedirect->setPath('*/import/index', ['_current' => true]);
    }

    protected function _setQty($product)
    {
        $pinCollection = $this->_pinFactory->create()
            ->getCollection()
            ->addFieldToFilter('product_id', $product->getId())
            ->addFieldToFilter('status', 1);
        $qty = (int)$pinCollection->getSize();

        $stockItem = $this->_stock->getStockItemBySku($product->getSku());
        $stockItem->setQty($qty);
        $stockItem->setIsInStock((bool)$qty); // this line
        $this->_stock->updateStockItemBySku($product->getSku(), $stockItem);
    }
}
