<?php
namespace Magenest\Pin\Controller\Adminhtml\Upload;

use Magento\Backend\App\Action;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\Registry;

class Edit extends Action
{
    const ADMIN_RESOURCE = "Magenest_Pin::import_pin_file";

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        Registry $registry
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Pin::import_pin_file');

        return $resultPage;
    }
}
