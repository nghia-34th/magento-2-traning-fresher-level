<?php
/**
 * Created by PhpStorm.
 * User: canhnd
 * Date: 06/06/2017
 * Time: 17:03
 */

namespace Magenest\Pin\Controller\Adminhtml\Upload;

use Magento\Backend\App\Action\Context;

/**
 * Class Sample
 * @package Magenest\Pin\Controller\Adminhtml\Upload
 */
class Sample extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Magenest_Pin::import_pin_file";

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    protected $fileSystem;

    protected $dir;

    protected $fileFactory;

    protected $csvProcessor;

    public function __construct(
        Context                                                        $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\App\Filesystem\DirectoryList                $directoryList,
        \Magento\Framework\App\Response\Http\FileFactory               $fileFactory,
        \Magento\Framework\File\Csv                                    $csvProcessor,
        \Magento\Framework\Filesystem\Driver\File                      $fileSystem
    )
    {
        $this->csvProcessor = $csvProcessor;
        $this->fileFactory = $fileFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->directoryList = $directoryList;
        $this->fileSystem = $fileSystem;
        parent::__construct($context);
    }

    public function execute()
    {
        $dataSample = [
            [
                __('pin'),
                __('sku'),
                __('status'),
                __('comments')
            ],
            [
                'pin' => 'Live boxlice0',
                'sku' => 'License Code',
                'status' => '1',
                'comment' => 'http://google.com'
            ],
            [
                'pin' => 'Live box licen1',
                'sku' => 'License Code',
                'status' => '2',
                'comment' => 'http://google.com'
            ],
            [
                'pin' => 'Live box licen3',
                'sku' => 'License Code',
                'status' => '3',
                'comment' => 'http://google.com'
            ]
        ];

        $fileName = 'SampleFile.csv';
        $filePath = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR) . "Sample.php/" . $fileName;

        $this->csvProcessor
            ->setDelimiter(';')
            ->setEnclosure('"')
            ->saveData(
                $filePath,
                $dataSample
            );
        return $this->fileFactory->create(
            $fileName,
            [
                'type' => "filename",
                'value' => $fileName,
                'rm' => true,
            ],
            \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR,
            'application/octet-stream'
        );
    }

}
