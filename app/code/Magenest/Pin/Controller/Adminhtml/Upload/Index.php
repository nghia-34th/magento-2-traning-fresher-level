<?php

namespace Magenest\Pin\Controller\Adminhtml\Upload;

use \Magento\Backend\App\Action\Context;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Backend\App\Action;

class Index extends Action
{
    const ADMIN_RESOURCE = "Magenest_Pin::import_pin_file";

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {

        parent::__construct($context);
        $this->_resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Pin::import_pin_file');
        $resultPage->addBreadcrumb(__('Manage Pin'), __('Manage Pin'));
        $resultPage->getConfig()->getTitle()->prepend(__('Upload'));
        $resultPage->addContent($resultPage->getLayout()->createBlock('Magenest\Pin\Block\Adminhtml\Upload\Edit'));
        return $resultPage;
    }
}
