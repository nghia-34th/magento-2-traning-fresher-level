<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 27/07/2016
 * Time: 23:26
 */
namespace Magenest\Pin\Controller\Adminhtml\Instruction;

use Magento\Backend\App\Action;

/**
 * Class MassDelete
 */
class MassDelete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Magenest_Pin::manage_pin_instruction";

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $Ids = $this->getRequest()->getParam('instruction');
        if (!is_array($Ids) || empty($Ids)) {
            $this->messageManager->addError(__('Please select instruction(s).'));
        } else {
            try {
                foreach ($Ids as $Id) {
                    $post = $this->_objectManager->get('Magenest\Pin\Model\Instruction')->load($Id);
                    $post->delete();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.', count($Ids))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('*/*/index');
    }
}
