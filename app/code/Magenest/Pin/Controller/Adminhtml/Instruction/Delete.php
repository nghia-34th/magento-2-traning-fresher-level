<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 27/07/2016
 * Time: 23:28
 */
namespace Magenest\Pin\Controller\Adminhtml\Instruction;

use \Magento\Backend\App\Action\Context;

class Delete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Magenest_Pin::manage_pin_instruction";

    /**
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        try {
            $post = $this->_objectManager->get('Magenest\Pin\Model\Instruction')->load($id);
            $post->delete();
            $this->messageManager->addSuccess(
                __('A total of 1 record have been deleted.')
            );
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        /**
         * @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect
         */
        return $this->resultRedirectFactory->create()->setPath('*/*/index');
    }
}
