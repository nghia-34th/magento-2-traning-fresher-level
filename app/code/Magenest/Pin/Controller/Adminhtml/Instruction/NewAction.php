<?php
/**
 * Created by PhpStorm.
 * User: canh
 * Date: 25/01/2016
 * Time: 10:58
 */
namespace Magenest\Pin\Controller\Adminhtml\Instruction;

use Magento\Backend\App\Action;

class NewAction extends Action
{
    const ADMIN_RESOURCE = "Magenest_Pin::manage_pin_instruction";

    /**
     * @var \Magento\Backend\Model\View\Result\Forward
     */
    protected $resultForwardFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
    ) {
        $this->resultForwardFactory = $resultForwardFactory;
        parent::__construct($context);
    }

    /**
     * Forward to edit
     *
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        $this->_forward('edit');
    }
}
