<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_Pin extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Pin
 * @author   <CanhND>-duccanhdhbkhn@gmail.com
 */

namespace Magenest\Pin\Controller\Adminhtml\License;

use Magenest\Pin\Controller\Adminhtml\License;
use Magenest\Pin\Model\BlobFactory;
use Magenest\Pin\Model\PinFactory;
use Magenest\Pin\Model\ResourceModel\Blob;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Ui\Component\MassAction\Filter;
use Psr\Log\LoggerInterface;

/**
 * Class MassDelete
 *
 * @package Magenest\Pin\Controller\Adminhtml\License
 */
class MassDelete extends License
{
    /**
     * @var \Magenest\Pin\Model\ImportFactory
     */
    protected $importFactory;

    /**
     * @var StockRegistryInterface
     */
    protected $_stock;

    protected $product;

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        PinFactory $pinFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        Filter $filter,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        ScopeConfigInterface $scopeConfigInterface,
        Filesystem $filesystem,
        UploaderFactory $uploaderFactory,
        BlobFactory $blobFactory,
        File $file,
        LoggerInterface $loggerInterface,
        ProductFactory $productFactory,
        Blob $blob,
        \Magenest\Pin\Model\ImportFactory $importFactory,
        StockRegistryInterface $stock
    ) {
        parent::__construct($context, $coreRegistry, $resultPageFactory, $pinFactory, $resultRawFactory, $layoutFactory, $filter, $resultForwardFactory, $scopeConfigInterface, $filesystem, $uploaderFactory, $blobFactory, $file, $loggerInterface, $productFactory, $blob);
        $this->importFactory = $importFactory;
        $this->_stock = $stock;
    }

    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws LocalizedException
     */
    public function execute()
    {
        $brandCollection = $this->_pinFactory->create()->getCollection();
        $collections = $this->_filter->getCollection($brandCollection);
        $totals = 0;
        try {
            $productIds = [];
            foreach ($collections as $item) {
                $idPin = $item->getId();
                if ($idPin) {
                    $id = array($item->getProductId());
                    if (!in_array($id, $productIds)) {
                        $productIds[] = $id;
                    }
                    $item->delete();
                    $imports = $this->importFactory->create()->getCollection()->addFieldToFilter("pin_id", $idPin);
                    foreach ($imports as $import) {
                        $import->delete();
                    }
                    $totals++;
                }
            }
            foreach ($productIds as $productId) {
                $this->_setQty($productId);
            }

            $this->messageManager->addSuccess(__('A total of %1 record(s) have been deteled.', $totals));
        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->_getSession()->addException($e, __('Something went wrong while delete the post(s).'));
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    protected function _setQty($productId)
    {
        $pinCollection = $this->_pinFactory->create()
            ->getCollection()
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('status', 1);
        $qty = (int)$pinCollection->getSize();
        $product = $this->_productFactory->create()->load($productId);
        $stockItem = $this->_stock->getStockItemBySku($product->getSku());
        $stockItem->setQty($qty);
        $stockItem->setIsInStock((bool)$qty); // this line
        $this->_stock->updateStockItemBySku($product->getSku(), $stockItem);
    }
}
