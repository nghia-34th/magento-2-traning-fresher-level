<?php

namespace Magenest\Pin\Controller\Adminhtml\License;

use \Magento\Backend\App\Action\Context;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Backend\App\Action;

class Edit extends Action
{
    const ADMIN_RESOURCE = "Magenest_Pin::manage_license_code";

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Pin::manage_license_code');
        $resultPage->addBreadcrumb(__('CMS'), __('CMS'));
        $resultPage->addBreadcrumb(__('Manage Pin'), __('Manage Pin'));
        $resultPage->getConfig()->getTitle()->prepend(__('Edit Pin'));

//        $resultPage->addContent($resultPage->getLayout()->createBlock('Magenest\Pin\Block\Adminhtml\License\Edit'));
        return $resultPage;
    }
}
