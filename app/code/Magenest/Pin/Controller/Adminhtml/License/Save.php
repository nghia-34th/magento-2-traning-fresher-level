<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 22/04/2017
 * Time: 15:43
 */

namespace Magenest\Pin\Controller\Adminhtml\License;

use Magenest\Pin\Controller\Adminhtml\License;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends License
{
    public function execute()
    {
        try {
            // text
            $value = $this->getRequest()->getPostValue();
            $pin = $this->_pinFactory->create()->load($value['id']);
            if ($value['save_option'] == \Magenest\Pin\Model\Pin::TYPE_TEXT) {
                if ($pin->getFileExtension() == 'database') {
                    $blob = $this->_blobFactory->create()->load($value['id']);
                    $blob->delete();
                }
                $pin->setCode($value['pin_code']);
                $pin->setFileExtension('text');
                $pin->setPinType(1);
                $pin->setOriFileName(null);
                $pin->setComments($value['comment']);
                $pin->save();
                $this->messageManager->addSuccess(__('Save successfully'));
            }
            if ($value['save_option'] == \Magenest\Pin\Model\Pin::TYPE_FILE) {
                // change image
                $image = $this->getRequest()->getFiles('pin_file_after');
                if ($image['type']) {
                    $check = strpos($image['type'], "image");
                    if ($check !== false) {
                        if ($pin->getFileExtension() == 'database') {
                            $id = $pin->getId();
                            $blob = $this->_blobFactory->create()->load($id);
                            $blob->delete();
                        }
                        $preFileName = $pin->getOriFileName();
                        $pin->setPinType(2);
                        $pin->setComments($value['comment']);
                        $pin->setOriFileName($image['name']);

                        $pin->setCode(null);
                        $varDirectory = $this->_filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
                        $uploadDir = $varDirectory->getAbsolutePath('importexport/');
                        $fileUpload = $this->_fileUploaderFactory->create(['fileId' => $image]);
                        $result = $fileUpload->save($uploadDir);
                        $filePath = $result['path'] . $result['file'];
                        $fileRead = new  \Magento\Framework\Filesystem\File\Read($filePath, $this->_file);
                        $content = $fileRead->readAll();
                        if ($pin->getFileExtension() == 'system') {
                            $desDir = $this->_scopeConfig->getValue('pin/directory/path');
                            $this->_file->deleteFile($desDir . '/' . $preFileName);
                        }
                        $pin->setFileExtension('database');
                        $pin->save();
                        $blob = $this->_blobFactory->create()->load($pin->getId());
                        if ($blob->getId()) {
                            $blob->setPinFile($content);
                            $blob->save();
                        } else {
                            $this->_blobCollection->insertData($pin->getId(), $content);
                        }
                        $this->messageManager->addSuccess(__('Save successfully'));
                    } else {
                        $this->messageManager->addError("Please check the image file format");
                    }
                } else {
                    // change comment in image
                    $preFileName = $pin->getOriFileName();
                    $pin->setFileExtension('database');
                    $pin->setPinType(2);
                    $pin->setComments($value['comment']);
                    $pin->setCode(null);
                    $pin->save();
                    $this->messageManager->addSuccess(__('Save successfully'));
                }
            }

        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath($this->getRequest()->getPostValue('url_path'));
        return $resultRedirect;
    }

    public function getSaveUrl()
    {
        // lay url cua previous page
        $url = $this->getRequest()->getServer('HTTP_REFERER');
        if ($url == null) {
            $url = $this->getUrl('pin/license/index');
        }
        return $url;
    }
}