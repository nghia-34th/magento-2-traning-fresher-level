<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 11/10/2016
 * Time: 16:00
 */
namespace Magenest\Pin\Controller\Adminhtml\License;

use Magenest\Pin\Controller\Adminhtml\License;

class Index extends License
{
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Pin::manage_license_code');
        $resultPage->addBreadcrumb(__('Game License'), __('Game License'));
        $resultPage->addBreadcrumb(__('Manage Game Code'), __('Manage Game Code'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Game Code'));
        return $resultPage;
    }
}
