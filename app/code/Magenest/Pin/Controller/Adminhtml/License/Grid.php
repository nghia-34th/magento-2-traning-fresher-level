<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_Pin extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Pin
 * @author   CanhND <duccanhdhbkhn@gmail.com>
 */
namespace Magenest\Pin\Controller\Adminhtml\License;

use Magenest\Pin\Controller\Adminhtml\License;

class Grid extends License
{
    /**
     * Managing newsletter grid
     *
     * @return void
     */
    public function execute()
    {
        $this->_view->loadLayout(false);
        $this->_view->renderLayout();
    }
}
