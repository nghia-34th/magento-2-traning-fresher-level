<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_Pin extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Pin
 * @author   <CanhND>-duccanhdhbkhn@gmail.com
 */
namespace Magenest\Pin\Controller\Adminhtml\License;

use Magenest\Pin\Controller\Adminhtml\License;
use Magenest\Pin\Model\BlobFactory;
use Magenest\Pin\Model\PinFactory;
use Magenest\Pin\Model\ResourceModel\Blob;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Ui\Component\MassAction\Filter;
use Psr\Log\LoggerInterface;

class MassStatus extends License
{
    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws LocalizedException
     */
    public function execute()
    {
        $licenseCollection = $this->_pinFactory->create()->getCollection();
        $collections     = $this->_filter->getCollection($licenseCollection);
        $status          = (int) $this->getRequest()->getParam('status');
        $totals          = 0;
        $productIds=[];
        try {
            foreach ($collections as $item) {
                if (!in_array($item->getProductId(), $productIds)) {
                    $productIds[]= $item->getProductId();
                }
                $item->setStatus($status);
                $item->save();
                $totals++;
            }
            $this->_logger->critical(print_r($productIds, true));
            foreach ($productIds as $productId) {
                $this->_productFactory->create()->load($productId)->save();
            }
            $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been updated.', $totals));
        } catch (LocalizedException | \Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        $resultRedirect = $this->resultRedirectFactory->create();

        return $resultRedirect->setPath('*/*');
    }
}
