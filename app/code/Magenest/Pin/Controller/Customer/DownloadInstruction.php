<?php
/**
 * Created by PhpStorm.
 * User: ninhvu
 * Date: 09/05/2018
 * Time: 13:23
 */

namespace Magenest\Pin\Controller\Customer;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magenest\Pin\Model\PinFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;

class DownloadInstruction extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $_fileFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var PinFactory
     */
    protected $_pinFactory;

    protected $instructionFactory;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param PinFactory $pinFactory
     * @param ScopeConfigInterface $scopeConfigInterface
     * @param \Magenest\Pin\Model\InstructionFactory $instructionFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        PinFactory $pinFactory,
        ScopeConfigInterface $scopeConfigInterface,
        \Magenest\Pin\Model\InstructionFactory $instructionFactory
    ) {
        $this->_pinFactory=$pinFactory;
        $this->_fileFactory = $fileFactory;
        $this->_scopeConfig=$scopeConfigInterface;
        $this->instructionFactory = $instructionFactory;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        if ($id) {
                $instruction = $this->instructionFactory->create()->load($id);
                $content = $instruction->getInstructions();
                $filename = $instruction->getName();
                return $this->_fileFactory->create(
                    $filename,
                    $content,
                    DirectoryList::TEMPLATE_MINIFICATION_DIR
                );
        } else {
            return $this->resultForwardFactory->create()->forward('noroute');
        }
    }
}
