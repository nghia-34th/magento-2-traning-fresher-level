<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 05/01/2016
 * Time: 14:01
 */

namespace Magenest\Pin\Controller\Customer;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magenest\Pin\Model\BlobFactory;
use Magenest\Pin\Model\PinFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magenest\Pin\Model\Purchased;
use Magento\Framework\Stdlib\DateTime\DateTime;
use \Magento\Framework\Filesystem\Io\File;
class Download extends \Magento\Framework\App\Action\Action
{
    const INSTRUCTION = 'instruction';
    const DATABASE = 'database';

    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $_fileFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var BlobFactory
     */
    protected $_blobFactory;

    /**
     * @var PinFactory
     */
    protected $_pinFactory;

    /**
     * @var \Magenest\Pin\Model\InstructionFactory
     */
    protected $instructionFactory;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Cms\Model\Template\Filter
     */
    protected $filter;
    /**
     * @var Purchased
     */
    private $purchased;
    /**
     * @var DateTime
     */
    private $dateTime;
    /**
     * @var Read
     */
    private $read;
    /**
     * @var File
     */
    private $file;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param BlobFactory $blobFactory
     * @param PinFactory $pinFactory
     * @param \Magenest\Pin\Model\InstructionFactory $instructionFactory
     * @param ScopeConfigInterface $scopeConfigInterface
     * @param \Magento\Cms\Model\Template\Filter $filter
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        BlobFactory $blobFactory,
        PinFactory $pinFactory,
        \Magenest\Pin\Model\InstructionFactory $instructionFactory,
        ScopeConfigInterface $scopeConfigInterface,
        DateTime $dateTime,
        Purchased $purchased,
        File $file,
        \Magento\Cms\Model\Template\Filter $filter
    ) {
        $this->_pinFactory = $pinFactory;
        $this->_fileFactory = $fileFactory;
        $this->_blobFactory = $blobFactory;
        $this->_scopeConfig = $scopeConfigInterface;
        $this->instructionFactory = $instructionFactory;
        $this->filter = $filter;
        parent::__construct($context);
        $this->purchased = $purchased;
        $this->dateTime = $dateTime;
        $this->file = $file;
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException|\Exception
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $type = $this->getRequest()->getParam('type');

        if ($id) {
            if ($type && $type == self::INSTRUCTION) {
                $purchasedModel = $this->purchased->load($id);
                $idInstruction = $purchasedModel->getInstructionId();
                $instruction = $this->instructionFactory->create()->load($idInstruction);
                $content = $instruction->getInstructions();
                $index = true;
                $baseUrl = $this->_url->getBaseUrl();
                $contentCurrent = "<!DOCTYPE html><head><meta charset=\"UTF-8\"><title>Instruction</title></head><body>" . $content . "</body>";
                while ($index) {
                    $linkPos1 = strpos($contentCurrent, 'src="{{');
                    $construction = [];
                    if ($linkPos1 !== false) {
                        $linkPos2 = strpos($contentCurrent, '}}"', $linkPos1);
                        $link = substr($contentCurrent, $linkPos1 + 5, $linkPos2 + 2 - $linkPos1 - 5);
                        $construction[] = $link;
                        $linkElems = explode(" ", $link);
                        $type = substr($linkElems[0], 2);
                        $construction[] = $type;
                        $url = " " . substr($linkElems[1], 0, strlen($linkElems[1]) - 2);
                        $construction[] = $url;
                        $path = $this->filter->mediaDirective($construction);
                        $linkReplace = $baseUrl . $path;
                        $contentCurrent = str_replace($link, $linkReplace, $contentCurrent);
                    } else {
                        $index = false;
                    }
                }
                $filename = $instruction->getName();
                $date = $this->dateTime->date('Y-m-d_H-i-s');
                return $this->_fileFactory->create(
                    $filename . ".html",
                    $contentCurrent,
                    DirectoryList::MEDIA
                );
            } else {
                $purchasedModel = $this->purchased->load($id);
                if ($purchasedModel->getFileExtension() == self::DATABASE) {
                    $id = $purchasedModel->getPinFileId();
                    $blob = $this->_blobFactory->create()->load($id);
                    $content = $blob->getPinFile();
                    $pin = $this->_pinFactory->create()->load($id);
                    $date = $this->dateTime->date('Y-m-d_H-i-s');
                    $filename = $pin->getOriFileName();
                    return $this->_fileFactory->create(
                        $filename,
                        $content,
                        DirectoryList::VAR_DIR
                    );
                } elseif ($purchasedModel->getFileExtension() != self::DATABASE) {
                    $filename = $purchasedModel->getFileExtension();
                    $desDir = $this->_scopeConfig->getValue('pin/directory/path');
                    $fileUrl = $desDir . '/' . $filename;

//                    $fp = fopen($fileUrl, 'r');
//                    $content = fread($fp, filesize($fileUrl));

//                    $fp = $this->file->open($fileUrl);
                    $content = $this->file->read($fileUrl);
                    return $this->_fileFactory->create(
                        $filename,
                        $content,
                        DirectoryList::VAR_DIR
                    );
                }
            }
        } else {
            return $this->resultForwardFactory->create()->forward('noroute');
        }
    }
}
