<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 02/10/2015
 * Time: 11:07
 */
namespace Magenest\Pin\Helper;

use Magento\Cms\Model\Template\FilterProvider;
use Magento\Sales\Model\Order;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const EMAIL_TEMPLATE = "pin/pin/email_template";
    const EMAIL_IDENTITY = "pin/pin/email_identity";

    /**
     * @var \Magenest\Pin\Model\PinFactory
     */
    protected $_pinFactory;

    /**
     * @var \Magenest\Pin\Model\Mail\TransportBuilderCheckVersion
     */
    protected $_transportBuilderCheckVersion;

    /**
     * @var array
     */
    protected $var=[];

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var FilterProvider
     */
    protected $_filterProvider;

    /**
     * @var \Magenest\Pin\Model\InstructionFactory
     */
    protected $instructionFactory;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderCollection;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magenest\Pin\Model\PurchasedFactory
     */
    protected $purchaseFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magenest\Pin\Model\BlobFactory
     */
    protected $blobFactory;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $_productMetadata;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magenest\Pin\Model\Mail\TransportBuilderCheckVersion $transportBuilderCheckVersion
     * @param \Magenest\Pin\Model\PinFactory $pinFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magenest\Pin\Model\InstructionFactory $instructionFactory
     * @param FilterProvider $filterProvider
     * @param \Magento\Sales\Model\OrderFactory $orderCollection
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magenest\Pin\Model\PurchasedFactory $purchasedFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magenest\Pin\Model\BlobFactory $blobFactory
     * @param \Magento\Framework\App\ProductMetadataInterface $productMetadata
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magenest\Pin\Model\Mail\TransportBuilderCheckVersion $transportBuilderCheckVersion,
        \Magenest\Pin\Model\PinFactory $pinFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magenest\Pin\Model\InstructionFactory $instructionFactory,
        FilterProvider $filterProvider,
        \Magento\Sales\Model\OrderFactory $orderCollection,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magenest\Pin\Model\PurchasedFactory $purchasedFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magenest\Pin\Model\BlobFactory $blobFactory,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata
    ) {
        parent::__construct($context);
        $this->_pinFactory = $pinFactory;
        $this->_productFactory = $productFactory;
        $this->_transportBuilderCheckVersion = $transportBuilderCheckVersion->create();
        $this->_filterProvider = $filterProvider;
        $this->instructionFactory = $instructionFactory;
        $this->orderCollection = $orderCollection;
        $this->messageManager = $messageManager;
        $this->purchaseFactory = $purchasedFactory;
        $this->productRepository = $productRepository;
        $this->blobFactory = $blobFactory;
        $this->_productMetadata = $productMetadata;
    }

    /**
     * @return \Magento\Framework\App\RequestInterface
     */
    public function getRequest()
    {
        return $this->_request;
    }

    /**
     * @return array
     */
    public function getVars($attachments)
    {
        $html['pin_info_html']='';
        if ($attachments) {
            foreach ($attachments as $attachment) {
                if (strpos($attachment['name'], ".txt")!==false) {
                    // exsit .txt in $attachement['name']
                    $html['pin_info_html']= $html['pin_info_html']."<span>Pin code: ".$attachment['body']."</span><br>";
                }
            }
        }
        return $html;
    }

    /**
     * @param int $storeId
     * @param $recipientName
     * @param $recipientEmail
     * @param $attachments
     * @param $productId
     * @param $orderId
     * @throws \Exception
     */
    public function sendNotificationEmail($storeId, $recipientName, $recipientEmail, $attachments, $productId, $orderId)
    {
//        $storeId = 1;
        $templateId = $this->scopeConfig->getValue(self::EMAIL_TEMPLATE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        if (!$templateId) {
            $templateId = "pin_email_order_template";
        }

        $product = $this->_productFactory->create()->load($productId);

        $from = $this->scopeConfig->getValue(
            self::EMAIL_IDENTITY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        if (!$from) {
            $from = 'general';
        }
        $instruction = $this->getIntroductions($product->getData('instructions'));
        if ($instruction == 0) {
            $instruction = "";
        } else {
            $instruction = "Instruction <b>" . $product->getName() . "</b> in the attachement.";
        }

        $this->_transportBuilderCheckVersion->reset();
        $this->_transportBuilderCheckVersion->setTemplateIdentifier($templateId)
            ->setTemplateOptions(
                ['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId]
            )->setTemplateVars(
                [
                    $this->getVars($attachments),
                    'pin_code_html' => $this->getHtmlCode($attachments),
                    'name' => $product->getName(),
                    'instructions' => $instruction,
                ]
            )
            ->setFrom(
                $from
            )
            ->addTo(
                $recipientEmail,
                $recipientName // Name not support Vietnamese
            );

        if ($attachments) {
            $idInstruction = $product->getData('instructions');
            $content = $this->instructionFactory->create()->load($idInstruction)->getInstructions() ?? '';
            $body = $this->getIntroductions($content);
            $name = 'instructions.html';
            if ($body) {
                $attachments[] = array('body' => $body, 'name' => $name);
            }
        }

        $version = $this->_productMetadata->getVersion();

        //check version magento
        /** @var  $transport \Magento\Framework\Mail\TransportInterface */
        if ($version >="2.3.3"){
            if ($attachments) {
                $this->_transportBuilderCheckVersion->setAttachment($attachments);
            }
            $transport = $this->_transportBuilderCheckVersion->getTransport();
        }elseif ($version <="2.3.2"){
            if(method_exists($this->_transportBuilderCheckVersion->getMessage(),'createAttachment')) {
                if ($attachments) {
                    foreach ($attachments as $attachment) {
                        if (strpos($attachment['name'], ".txt") === false) {
                            $this->_transportBuilderCheckVersion->createAttachment($attachment);
                        }
                    }
                }
                $transport = $this->_transportBuilderCheckVersion->getTransport();
            }else{
                $transport = $this->_transportBuilderCheckVersion->getTransport();
                if ($attachments) {
                    foreach ($attachments as $attachment) {
                        if (strpos($attachment['name'], ".txt") === false) {
                            $this->_transportBuilderCheckVersion->createAttachment($attachment,$transport);
                        }
                    }
                }
            }
        }

        try {
            $transport->sendMessage();
        } catch (\Magento\Framework\Exception\MailException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_logger->critical('Email error '.$e);
        }
    }

    /**
     * @param $product_id
     * @return mixed
     */
    public function getIsPINProduct($product_id)
    {

        return $this->_pinFactory->create()->getIsPinProduct($product_id);
    }

    /**
     * @param $content
     * @return string
     * @throws \Exception
     */
    public function getIntroductions($content)
    {
        return $this->_filterProvider->getPageFilter()->filter($content);
    }

    public function getHtmlCode($attachments)
    {
        $html['pin_info_html']='';
        if ($attachments) {
            foreach ($attachments as $attachment) {
                if (strpos($attachment['name'], ".txt")!==false) {
                    // exsit .txt in $attachement['name']
                    $html['pin_info_html']= $html['pin_info_html']."<span>Pin code: ".$attachment['body']."</span><br>";
                }
            }
        }
        return $html['pin_info_html'];
    }
}
