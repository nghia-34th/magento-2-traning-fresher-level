<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 12/10/2016
 * Time: 15:45
 */
namespace Magenest\Pin\Helper;

/**
 * Class File
 * @package Magenest\Pin\Helper
 */
class File extends \Magento\Downloadable\Helper\File
{

    /**
     * Upload file from temporary folder.
     * @param string $tmpPath
     * @param \Magento\MediaStorage\Model\File\Uploader $uploader
     * @return array
     */
    public function uploadFromTmp($tmpPath, \Magento\MediaStorage\Model\File\Uploader $uploader)
    {
        $uploader->setAllowRenameFiles(true);
        $uploader->setFilesDispersion(false);
        $absoluteTmpPath = $this->_mediaDirectory->getAbsolutePath($tmpPath);
        $result = $uploader->save($absoluteTmpPath);

        return $result;
    }

    /**
     * @return \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    public function getMediaDir()
    {
        return $this->_mediaDirectory;
    }
}
