
define([
    "jquery",
    "Magento_Ui/js/lib/core/class",
    'Magento_Ui/js/lib/spinner',
    "Magenest_Pin/js/pin",
    "underscore"
], function ($, Class, loader, Pin, _) {
    "use strict";
    return Class.extend({
        defaults: {
            /**
             * the table element
             */
            mainElement: ''
        },
        /**
         * Constructor
         */
        initialize: function (config) {

            this.initConfig(config);


            this._addButton(config);


            return this;
        },


        escapeRegExp: function (string) {
            return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        },

        replaceAll: function (string, find, replace) {
            var pin = this;
            return string.replace(new RegExp(pin.escapeRegExp(find), 'g'), replace);
        },

        _addButton: function (config) {
            var pin = this;


            var addBtn =  pin.mainElement.find('[data-action="add-new-row"]');
            addBtn.click(function () {

                var rowIds = new Array();
                var template =  pin.mainElement.find('[data-role="sample-template"]').html();

                var trElements = pin.mainElement.find('tbody').find('tr');


                jQuery(trElements).each(function (index, element) {
                    if (jQuery(element).data('order') != null) {

                        rowIds.push(jQuery(element).data('order'));


                    }
                });

                // var row_id = rowIds.max();

                var row_id = Math.max.apply(rowIds, rowIds);
                var next_id = parseInt(row_id) + 1;

                var is_new_template = '<input type="hidden" name="pin[' + next_id
                    + '][is_new]" value="1" />';
                var templateRow = '<tr ' + ' data-order =' + next_id + '>' + template;
                var valueFind = '/value=\".+\"/';
                templateRow = pin.replaceAll(templateRow, valueFind, ' ');

                var find = "[0]";
                var re = new RegExp(find, 'g');

                var replace = "[" + next_id + "]";
                var res = pin.replaceAll(templateRow, find, replace);

                find = '_0_';
                replace = '_' + next_id + '_';
                re = new RegExp(find, 'g');
                res = pin.replaceAll(res, find, replace);
                var newRow = res + is_new_template + '</tr>';

                pin.mainElement.find('tbody').append(newRow);
            });
        }


    });
});
