/**
 * Copyright © 2016 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {

    paths: {
        "jquery.validate": "jquery/jquery.validate",

        "jquery.validate.ext": "Magenest_Pin/js/lib/additional-methods"
    },
    shim: {
        'jquery.validate.ext': {
            'deps': ['jquery.validate']
        }
    }
};
