##CHANGELOG - Game License
=============
Compatible with 
```
Magento Commerce 2.1.x, 2.2.x, 2.3.x, 2.4.x
Magento OpenSource 2.1.x, 2.2.x, 2.3.x, 2.4.x
```
##101.3.4 - 30/6/2022
- License Delivery now compatible with Magento 2.4.4
- Fix error save image
- Fix back button in form edit 

##101.3.4 - 06/01/2021
- License Delivery now compatible with Magento 2.4.3
- Fix: Compatible with split database
- Fix: Custom product option
- Fix: Import product

##101.3.3 - 20/11/2020
-   License Delivery now compatible with Magento 2.4.1

##101.3.1 - 08/11/2019
-   License Delivery now compatible with Magento 2.3.3
-   Fix: Email attachments

##101.3.0 - 10/01/2019
-   License Delivery now compatible with Magento 2.3
-   New Feauture: Product CSV Upload
-   Change format of Zip upload file
-   Back License To Stock When Order is Canceled
-   Fix bug url product license grid
-   Fix: email attachments
-   Fix bug: duplicate code, resend email
-   Fix bug: CSV upload and File Param
-   Fix bug: salable qty
-   Fix bug: Download instruction
-   Fix bug: Guest checkout license delivery
 
##101.2.6 - 24/10/2018
 - Use license product for group, bundle product
 - Fix duplicate email and other bug
##101.2.5 - 12/5/2018
 - Update interface product form
 - flexible virtual/real product
 - remove order license grid
 - upgrade grid license
 - simple config
 - add history inport
 - new feature: resend license
 - dowload intruction of license in customer acccount
 
=============
##101.2.4 - 24/2/2018
 - Update license status Enable -> Ordered -> Sold 

##101.2.3 - 31/1/2018
=============
 - Fix guest checkout
 - Fix csv import message

##101.2.2 - 21/10/2017
=============
*Compatible with Magento Open Source 2.2.0
 - Auto update inventore when update status of License
 - Update license status Enable -> Delivered -> Sold 
 
##101.2.0 - 21/08/2017
=============
* Compatible with Magento Open Source 2.1.x
* Compatible with Magento Commerce 2.1.x

##101.1.1 - 15/05/2017
=============
 - Refactor code
 - Change Grid manage license, order
 - Allow add Product License from Sample Product
 - Allow setting two method save license image
 - Show license code order
 - Allow add image code in folder system
 - Fix send email key attach file

##101.1.0 - 30/07/2016
=============
Second release
- Refactor code, use grid UI manage all license key
- Fix add image key, provide setting 2 type save image key
- Add attribute game code, allow add new product game code from old product
- Allow add instructions for product license
- Manage all game key for product
- Fix import text code CSV
- Send key code to customer with : info code, instruction product
- Show information code in order.

##100.0.0 - 30/04/2016
=============
First release
