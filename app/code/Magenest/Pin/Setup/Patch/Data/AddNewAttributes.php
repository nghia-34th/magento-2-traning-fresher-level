<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magenest\Pin\Setup\Patch\Data;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
* Patch is mechanism, that allows to do atomic upgrade data changes
*/
class AddNewAttributes implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var \Magento\Sales\Setup\SalesSetupFactory
     */
    private $salesSetupFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    private $productFactory;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Framework\App\State $state,
        EavSetupFactory $eavSetupFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Sales\Setup\SalesSetupFactory $salesSetupFactory
    ) {
        try {
            $state->setAreaCode('frontend');
        } catch (LocalizedException $e) {
        }
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->productFactory = $productFactory;
        $this->salesSetupFactory = $salesSetupFactory;
    }

    /**
     * Do Upgrade
     *
     * @return void
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'instructions',
            [
                'label'                   => 'Instructions',
                'group'                   => 'Product Details',
                'type'                    => 'int',
                'input'                   => 'select',
                'backend'                 => '',
                'frontend'                => '',
                'sort_order'              => '50',
                'class'                   => '',
                'source'                  => 'Magenest\Pin\Model\Instructions',
                'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible'                 => true,
                'required'                => false,
                'user_defined'            => false,
                'default'                 => '',
                'searchable'              => false,
                'filterable'              => false,
                'comparable'              => false,
                'visible_on_front'        => false,
                'used_in_product_listing' => true,
                'wysiwyg_enabled'         => true,
                'unique'                  => false,
                'apply_to'                => 'license',
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'is_virual_product',
            [
                'label'                   => 'Is Virtual Product',
                'group'                   => 'Product Details',
                'input'                   => 'boolean',
                "source"                  => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'backend'                 => '',
                'frontend'                => '',
                'sort_order'              => '80',
                'class'                   => '',
                'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible'                 => true,
                'required'                => false,
                'user_defined'            => true,
                'default'                 => '',
                'searchable'              => false,
                'filterable'              => false,
                'comparable'              => false,
                'visible_on_front'        => false,
                'used_in_product_listing' => true,
                'wysiwyg_enabled'         => true,
                'unique'                  => false,
                'apply_to'                => 'license',
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'terms_and_conditions',
            [
                'group' => 'License',
                'type' => 'text',
                'backend' => '',
                'frontend' => '',
                'label' => 'Terms and Condition',
                'input' => 'textarea',
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'wysiwyg_enabled' => true,
                'is_html_allowed_on_front' => true,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'wysiwyg' => [
                    "add_variables" => true,
                    "add_widgets" => true,
                    "add_directives" => true,
                    "use_container" => true,
                    "container_class" => "hor-scroll",
                ],
                'unique' => false,
                'apply_to' => 'license'
            ]
        );

        $entityType = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $eavSetup->updateAttribute($entityType, 'is_virual_product', 'frontend_input', 'select');
        $eavSetup->updateAttribute($entityType, 'is_virual_product', 'backend_type', 'int');
        $eavSetup->updateAttribute($entityType, 'is_virual_product', 'default_value', 1);

        $fields = [
            'price',
            'special_from_date',
            'special_price',
            'special_to_date',
            'minimal_price',
            'tier_price',
            'cost',
            'weight',
            'tax_class_id',
        ];
        foreach ($fields as $field) {
            $applyForProductType =  'simple,configurable,virtual,bundle,downloadable';
            $alreadyApplyForTheseProductType = explode(',', $applyForProductType);
            if (!in_array('license', $alreadyApplyForTheseProductType)) {
                $alreadyApplyForTheseProductType[] = 'license';
                $needApplyFor = implode(',', $alreadyApplyForTheseProductType);
                $eavSetup->updateAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    $field,
                    'apply_to',
                    $needApplyFor
                );
            }
        }

        // Create sample product
        $_product = $this->productFactory->create();
        $_productCheck = $_product->getIdBySku("License Code");
        if(!$_productCheck){
            $_product->setName('Test License Product');
            $_product->setTypeId('license');
            $_product->setAttributeSetId(4);
            $_product->setSku('License Code');
            $_product->setWebsiteIds(array(1));
            $_product->setVisibility(4);
            $_product->setPrice(12);
            $_product->setStockData(array(
                'use_config_manage_stock' => 0, //'Use config settings' checkbox
                'manage_stock' => 1, //manage stock
                'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
                'max_sale_qty' => 10000, //Maximum Qty Allowed in Shopping Cart
                'is_in_stock' => 1, //Stock Availability
                'qty' => 100 //qty
            ));
//            $_product->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }
}
