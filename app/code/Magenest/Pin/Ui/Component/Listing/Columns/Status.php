<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 11/10/2016
 * Time: 16:33
 */
namespace Magenest\Pin\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;


class Status extends Column
{
    /**
     *  Status
     */
    const STATUS_ENABLE  = 1;
    const STATUS_DISABLE = 2;
    const STATUS_DELIVERY = 3;
    const STATUS_SOLD = 4;

    /**
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param  array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                if (!empty($item['status'])) {
                    $item['status'] = strip_tags($this->getOptionGrid($item['status']), ENT_IGNORE);
                }
            }
        }

        return $dataSource;
    }

    /**
     * @return array
     */
    public function getOptionArray()
    {
        return [
            self::STATUS_ENABLE  => __('Available'),
            self::STATUS_DISABLE => __('Not Available'),
            self::STATUS_DELIVERY => __('Ordered'),
            self::STATUS_SOLD => __('Delivered'),
        ];
    }

    /**
     * @param $optionId
     * @return string
     */
    public function getOptionGrid($optionId)
    {
        $options = $this->getOptionArray();
        if ($optionId == self::STATUS_ENABLE) {
            $html = '<span class="grid-severity-notice"><span>'.$options[$optionId].'</span>'.'</span>';
        }
        if ($optionId == self::STATUS_DISABLE) {
            $html = '<span class="grid-severity-critical"><span>'.$options[$optionId].'</span></span>';
        }
        if ($optionId == self::STATUS_DELIVERY) {
            $html = '<span class="grid-severity-reject"><span>'.$options[$optionId].'</span></span>';
        }
        if ($optionId == self::STATUS_SOLD) {
            $html = '<span class="grid-severity-reject"><span>'.$options[$optionId].'</span></span>';
        }
        return $html;
    }
}