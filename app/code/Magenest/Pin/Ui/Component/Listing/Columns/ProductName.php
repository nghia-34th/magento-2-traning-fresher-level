<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 12/10/2016
 * Time: 08:38
 */
namespace Magenest\Pin\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;
use Magento\Catalog\Model\ProductFactory;


class ProductName extends Column
{
    /**
     * @var string
     */
    private $productUrl;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param ProductFactory $productFactory
     * @param array $components
     * @param array $data
     * @param string $product
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        ProductFactory $productFactory,
        array $components = [],
        array $data = []
    ) {
        $this->productFactory = $productFactory;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param  array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['product_id'])) {
                    $item[$fieldName] = "<a href='".$this->urlBuilder->getUrl('catalog/product/edit', ['id' => $item['product_id']])."' target='blank' title='".$this->getProductName($item['product_id'])."'>".$item[$fieldName].'</a>';
                }
            }
        }

        return $dataSource;
    }

    /**
     * Get Product Name
     *
     * @param $productId
     * @return mixed
     */
    public function getProductName($productId)
    {
        $productName = 'License';

        $collection = $this->productFactory->create()->getCollection()
            ->addFieldToFilter('entity_id', $productId)
            ->addAttributeToSelect('*');

        foreach ($collection as $param) {
            $productName = $param->getName();
        }

        return $productName;
    }
}
