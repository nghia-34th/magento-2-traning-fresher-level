<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 11/10/2016
 * Time: 17:04
 */

namespace Magenest\Pin\Ui\Component\Listing\Columns;

use Magenest\Pin\Model\BlobFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\View\Asset\Repository;

/**
 * Class Image
 *
 * @package Magenest\Pin\Ui\Component\Listing\Columns
 */
class Image extends Column
{
    /**
     * Url path
     */
    const BLOG_URL_PATH_EDIT = 'catalog/product/edit';

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var string
     */
    private $editUrl;

    /**
     * @var \Magento\Framework\View\Asset\Repository ;
     */
    protected $_assetRepo;

    /**
     * @var BlobFactory
     */
    protected $_blobFactory;

    /**
     * Image constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param StoreManagerInterface $storemanager
     * @param Repository $repository
     * @param BlobFactory $blobFactory
     * @param array $components
     * @param array $data
     * @param string $editUrl
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        StoreManagerInterface $storemanager,
        Repository $repository,
        BlobFactory $blobFactory,
        array $components = [],
        array $data = [],
        $editUrl = self::BLOG_URL_PATH_EDIT
    ) {
        $this->_blobFactory = $blobFactory;
        $this->_assetRepo = $repository;
        $this->_storeManager = $storemanager;
        $this->urlBuilder = $urlBuilder;
        $this->editUrl = $editUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function prepareDataSource(array $dataSource)
    {

        $mediaDirectory = $this->_storeManager->getStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
        );
        $hostUrl = $this->_storeManager->getStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_LINK
        );

        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $template = new \Magento\Framework\DataObject($item);

                $srcImage = $template->getOriFileName();
                $configFile = $template->getFileExtension();
                $productId = $template->getProductId();
                if ($srcImage != null) {
                    if ($configFile == 'system') {
                        $imageUrl = $mediaDirectory . 'pin/file/' . $template->getOriFileName();
                        $item[$fieldName . '_src'] = $imageUrl;
                        $item[$fieldName . '_alt'] = $template->getName();
                        $item[$fieldName . '_link'] = $this->urlBuilder->getUrl(
                            $this->editUrl,
                            [
                                'id' => $productId,
                                'store' => $this->context->getRequestParam('store'),
                            ]
                        );
                        $item[$fieldName . '_orig_src'] = $imageUrl;
                    }
                    if ($configFile == 'database') {
                        $id = $template->getId();
                        $blob = $this->_blobFactory->create()->load($id);
                        $imageUrl = 'data:image/jpeg;base64,' . base64_encode($blob->getPinFile());
                        $item[$fieldName . '_src'] = $imageUrl;
                        $item[$fieldName . '_alt'] = $template->getName();
                        $item[$fieldName . '_link'] = $this->urlBuilder->getUrl(
                            $this->editUrl,
                            [
                                'id' => $productId,
                                'store' => $this->context->getRequestParam('store'),
                            ]
                        );
                        $item[$fieldName . '_orig_src'] = $imageUrl;
                    }
                } else {
                    $srcImage = 'thumbnail.jpg';
                    $imageUrl = $this->_assetRepo->getUrl('Magenest_Pin::images/' . $srcImage);
                    $item[$fieldName . '_src'] = $imageUrl;
                    $item[$fieldName . '_alt'] = $template->getName();
                    $item[$fieldName . '_link'] = $this->urlBuilder->getUrl(
                        $this->editUrl,
                        [
                            'id' => $productId,
                            'store' => $this->context->getRequestParam('store'),
                        ]
                    );
                    $item[$fieldName . '_orig_src'] = $imageUrl;
                }
            }
        }

        return $dataSource;
    }
}
