<?php
/**
 * Created by PhpStorm.
 * User: ninhvu
 * Date: 21/04/2018
 * Time: 14:54
 */

namespace Magenest\Pin\Ui\Component\Listing\Columns;

use Magenest\Pin\Model\Pin\PinStatus;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class OrderPin extends Column
{
    /** Url path */
    const BLOG_URL_LICENSE_EDIT = 'sales/order/view/order_id';

    const DELIVERED = 'delivered';
    /**
     * @var string
     */
    private $licenseUrl;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magenest\Pin\Model\PurchasedFactory
     */
    protected $purchased;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param \Magenest\Pin\Model\PurchasedFactory $purchasedFactory
     * @param array $components
     * @param array $data
     * @param string $license
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        \Magenest\Pin\Model\PurchasedFactory $purchasedFactory,
        array $components = [],
        array $data = [],
        $license = self::BLOG_URL_LICENSE_EDIT
    ) {
        $this->licenseUrl = $license;
        $this->urlBuilder = $urlBuilder;
        $this->purchased = $purchasedFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $name = $this->getData('name');
                if (($item['status'] == self::DELIVERED) || ($item['status'] == "Ordered")) {
                    if (isset($item['order_item_id']) && $item['order_item_id'] != null) {
                        $purchased = $this->purchased->create()->getCollection()
                            ->addFieldToFilter("order_item_id", $item['order_item_id'])->getFirstItem();
                        $orderId = $purchased['order_id'];
                        $orderIncrementId = $purchased['order_increment_id'];
                        $item[$name]['edit'] = [
                            'href' => $this->urlBuilder->getUrl($this->licenseUrl, ['order_id' => $orderId]),
                            'label' => __("#" . $orderIncrementId)];
                    }
                } elseif ((($item['status'] == PinStatus::STATUS_ORDERED) || ($item['status'] == PinStatus::STATUS_DELIVERED)) && isset($item['order_id'])) {
                    $item[$name] = "<a href='" . $this->urlBuilder->getUrl('sales/order/view/order_id', ['order_id' => $item['order_id']]) . "' target='blank'>" . "#" . $item['increment_id'] . '</a>';
                }
            }
        }
        return $dataSource;
    }
}
