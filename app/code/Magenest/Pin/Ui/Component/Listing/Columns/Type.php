<?php
/**
 * Created by PhpStorm.
 * User: duccanh
 * Date: 11/10/2016
 * Time: 16:48
 */

namespace Magenest\Pin\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;


class Type extends Column
{
    /**
     *  Code Type
     */
    const CODE_TEXT = 1;
    const CODE_FILE = 2;

    /**
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                if (!empty($item['pin_type'])) {
                    $item['pin_type'] = strip_tags($this->getOptionGrid($item['pin_type']), ENT_IGNORE);
                }
            }
        }

        return $dataSource;
    }


    /**
     * @return array
     */
    public function getOptionArray()
    {
        return [
            self::CODE_TEXT => __('Text'),
            self::CODE_FILE => __('File'),
        ];
    }


    /**
     * @param $optionId
     * @return string
     */
    public function getOptionGrid($optionId)
    {
        $options = $this->getOptionArray();
        if ($optionId == self::CODE_TEXT) {
            $html = '<span class="grid-severity-notice"><span>' . $options[$optionId] . '</span>' . '</span>';
        } else {
            $html = '<span class="grid-severity-critical"><span>' . $options[$optionId] . '</span></span>';
        }

        return $html;
    }
}