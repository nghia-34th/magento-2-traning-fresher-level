<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_Pin extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Pin
 */

namespace Magenest\Pin\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Stdlib\ArrayManager;

class Attributes extends AbstractModifier
{
    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var LocatorInterface
     */
    private $locator;

    /**
     * Attributes constructor.
     *
     * @param ArrayManager $arrayManager
     * @param LocatorInterface $locator
     */
    public function __construct(ArrayManager $arrayManager, LocatorInterface $locator)
    {
        $this->arrayManager = $arrayManager;
        $this->locator = $locator;
    }

    /**
     * @param array $data
     * @return array
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        $product = $this->locator->getProduct()->getData();
        if ($product['type_id'] == "license") {
            $attribute = 'is_virual_product';
            $path = $this->arrayManager->findPath($attribute, $meta, null, 'children');
            $meta = $this->arrayManager->set(
                "{$path}/arguments/data/config/disabled",
                $meta,
                true
            );
        }

        return $meta;
    }
}
