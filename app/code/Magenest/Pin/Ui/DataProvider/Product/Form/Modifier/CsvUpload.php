<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 21/12/2018
 * Time: 10:15
 */

namespace Magenest\Pin\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\ProductOptions\ConfigInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Ui\Component\Modal;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\DynamicRows;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Framework\Locale\CurrencyInterface;
use Magento\Ui\Component\Form;

class CsvUpload extends AbstractModifier
{
    const ALLOWED_CSV_EXTENSIONS = 'csv';
    const BUTTON_IMPORT_CSV = 'button_import_csv';
    const GRID_CSV_IMPORT_NAME = 'csv_import';

    /**
     * @var \Magento\Catalog\Model\Locator\LocatorInterface
     */
    protected $locator;

    /**
     * @var \Magento\Catalog\Model\ProductOptions\ConfigInterface
     */
    protected $productOptionsConfig;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var ArrayManager
     */
    protected $arrayManager;

    /**
     * @var array
     */
    protected $meta = [];

    /**
     * @var CurrencyInterface
     */
    private $localeCurrency;

    /**
     * @param LocatorInterface $locator
     * @param StoreManagerInterface $storeManager
     * @param ConfigInterface $productOptionsConfig
     * @param UrlInterface $urlBuilder
     * @param ArrayManager $arrayManager
     */
    public function __construct(
        LocatorInterface $locator,
        StoreManagerInterface $storeManager,
        ConfigInterface $productOptionsConfig,
        UrlInterface $urlBuilder,
        ArrayManager $arrayManager
    ) {
        $this->locator = $locator;
        $this->storeManager = $storeManager;
        $this->productOptionsConfig = $productOptionsConfig;
        $this->urlBuilder = $urlBuilder;
        $this->arrayManager = $arrayManager;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;
        $product = $this->locator->getProduct()->getData();
        if ($product['type_id'] == "license") {
            $this->createCsvPanel();
        }
        return $this->meta;
    }

    protected function createCsvPanel()
    {
        $this->meta = array_replace_recursive(
            $this->meta,
            [
                License::GROUP_CUSTOM_OPTIONS_NAME => [
                    'children' => [
                        License::CONTAINER_HEADER_NAME => $this->getHeaderContainerConfig(20),
                        static::GRID_CSV_IMPORT_NAME => $this->getCsvImportGridConfig(60)
                    ]
                ]
            ]
        );
        return $this;
    }

    protected function getHeaderContainerConfig($sortOrder)
    {
        return [
            'children' => [
                static::BUTTON_IMPORT_CSV => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'title' => __('Import File CSV'),
                                'formElement' => Container::NAME,
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/form/components/button',
                                'sortOrder' => 30,
                                'actions' => [
                                    [
                                        'targetName' => ' index = ' . static::GRID_CSV_IMPORT_NAME,
                                        'actionName' => 'processingAddChild',
                                    ]
                                ]
                            ]
                        ],
                    ],
                ],
            ],
        ];
    }

    protected function getCsvImportGridConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'addButtonLabel' => __('Add Option'),
                        'componentType' => DynamicRows::NAME,
                        'component' => 'Magento_Catalog/js/components/dynamic-rows-import-custom-options',
                        'template' => 'Magenest_Pin/collapsible',
                        'additionalClasses' => 'admin__field-wide',
                        'deleteProperty' => License::FIELD_IS_DELETE,
                        'deleteValue' => '1',
                        'addButton' => false,
                        'renderDefaultRecord' => false,
                        'columnsHeader' => false,
                        'collapsibleHeader' => true,
                        'sortOrder' => $sortOrder,
                        'dataProvider' => License::CUSTOM_OPTIONS_LISTING,
                        'links' => ['insertData' => '${ $.provider }:${ $.dataProvider }'],
                    ],
                ],
            ],
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'headerLabel' => __('Import CSV File'),
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/dynamic-rows/record',
                                'positionProvider' => License::CONTAINER_OPTION . '.' . License::FIELD_SORT_ORDER_NAME,
                                'isTemplate' => true,
                                'is_collection' => true,
                            ],
                        ],
                    ],
                    'children' => [
                        License::CONTAINER_OPTION => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'componentType' => Fieldset::NAME,
                                        'label' => null,
                                        'sortOrder' => 10,
                                        'opened' => true,
                                    ],
                                ],
                            ],
                            'children' => [
                                License::CONTAINER_PIN_SUPPLIER => $this->getCsvSupplierConfig(10),
                            ]
                        ],
                    ]
                ]
            ]
        ];
    }

    protected function getCsvSupplierConfig($sortOrder)
    {
        $commonContainer = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Container::NAME,
                        'formElement' => Container::NAME,
                        'component' => 'Magento_Ui/js/form/components/group',
                        'breakLine' => false,
                        'showLabel' => false,
                        'additionalClasses' => 'admin__field-group-columns admin__control-group-equal',
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
            'children' => [
                License::FIELD_PIN_SUPPLIER => $this->getConfig(
                    20,
                    [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'label' => __('Additional Comments'),
                                    'component' => 'Magento_Catalog/component/static-type-input',
                                    'valueUpdate' => 'input',
                                    'notice' => 'The System Only Accept the CSV format file',
                                    'imports' => [
                                        'optionId' => '${ $.provider }:${ $.parentScope }.option_id'
                                    ]
                                ],
                            ],
                        ],
                    ]
                ),
                'links_file' => $this->getConfig(
                    30,
                    [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'label' => __('CSV File'),
                                    'formElement' => 'fileUploader',
                                    'componentType' => 'fileUploader',
                                    'component' => 'Magento_Downloadable/js/components/file-uploader',
                                    'elementTmpl' => 'Magenest_Pin/file-uploader',
                                    'fileInputName' => 'pin_code',
                                    "sampleUrl" => $this->urlBuilder->getUrl('pin/product/sampleCsv'),
                                    'allowedExtensions' => static::ALLOWED_CSV_EXTENSIONS,
                                    'uploaderConfig' => [
                                        'url' => $this->urlBuilder->getUrl(
                                            'pin/pin_file/upload',
                                            ['type' => 'pin_code', '_secure' => true]
                                        ),
                                    ],
                                    'dataScope' => 'pin_file',
                                    'validation' => [
                                        'required-entry' => false,
                                    ],
                                ],
                            ],
                        ],
                    ]
                ),
            ]
        ];

        return $commonContainer;
    }

    /**
     * Get config for "Title" fields
     *
     * @param int $sortOrder
     * @param array $options
     * @return array
     */
    protected function getConfig($sortOrder, array $options = [])
    {
        return array_replace_recursive(
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => __('Title'),
                            'componentType' => Field::NAME,
                            'formElement' => Input::NAME,
                            'dataScope' => License::FIELD_PIN_SUPPLIER,
                            'dataType' => Text::NAME,
                            'sortOrder' => $sortOrder,
                            'validation' => [
                                'required-entry' => false
                            ],
                        ],
                    ],
                ],
            ],
            $options
        );
    }
}
