<?php
/**
 * Created by PhpStorm.
 * User: canh
 * Date: 07/07/2016
 * Time: 14:53
 */

namespace Magenest\Pin\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Model\ProductOptions\ConfigInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Locale\CurrencyInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\DynamicRows;
use Magento\Ui\Component\Form;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Fieldset;


class License extends AbstractModifier
{
    /**#@+
     * Group values
     */
    const GROUP_CUSTOM_OPTIONS_NAME = 'license';
    const GROUP_CUSTOM_OPTIONS_SCOPE = 'data.product';
    const GROUP_CUSTOM_OPTIONS_PREVIOUS_NAME = '';
    const GROUP_CUSTOM_OPTIONS_DEFAULT_SORT_ORDER = 5;
    /**#@-*/

    /**#@+
     * Button values
     */
    const BUTTON_ADD = 'button_add';
    const BUTTON_IMPORT = 'button_import';
    const BUTTON_IMPORT_ZIP = 'button_import_zip';
    const BUTTON_IMPORT_IMAGE = 'button_import_image';
    /**#@-*/

    /**#@+
     * Container values
     */
    const CONTAINER_TERM_CONDITION = "container_term_condition";
    const CONTAINER_LICENSE_GRID = 'container_license_grid';
    const CONTAINER_HEADER_NAME = 'container_header';
    const CONTAINER_OPTION = 'container_option';
    const CONTAINER_PIN_SUPPLIER = 'container_supplier';
    const CONTAINER_PIN_CODE = 'container_common';
    const CONTAINER_PIN_FILE = 'container_file';
    /**#@-*/

    /**#@+
     * Grid values
     */
    const GRID_LICENSE_NAME = 'license_grid';
    const GRID_IMAGE_IMPORT_NAME = 'image_import';
    const GRID_ZIP_IMPORT_NAME = 'zip_import';
    const GRID_OPTIONS_NAME = 'game_license';
    const GRID_TYPE_SELECT_NAME = 'values';
    /**#@-*/

    /**#@+
     * Field values
     */
    const FIELD_ENABLE = 'affect_product_custom_options';
    const FIELD_OPTION_ID = 'option_id';
    const FIELD_PIN_SUPPLIER = 'pin_supplier';
    const FIELD_PIN_CODE = 'pin_code';
    const FIELD_PIN_FILE = 'pin_file';
    const FIELD_SORT_ORDER_NAME = 'sort_order';
    const FIELD_PRICE_NAME = 'price';
    const FIELD_IS_DELETE = 'is_delete';
    /**#@-*/

    /**#@+
     * Maximum allowed file size in bytes
     */
    const MAX_SIZE = '64000';
    /**#@-*/

    /**#@+
     * List of allowed file extensions
     */
    const ALLOWED_IMAGE_EXTENSIONS = 'jpg jpeg gif png svg';
    const ALLOWED_ZIP_EXTENSIONS = 'zip';
    /**#@-*/

    /**#@+
     * Import options values
     */
    const CUSTOM_OPTIONS_LISTING = 'product_custom_options_listing';
    /**#@-*/

    /**
     * @var \Magento\Catalog\Model\Locator\LocatorInterface
     */
    protected $locator;

    /**
     * @var \Magento\Catalog\Model\ProductOptions\ConfigInterface
     */
    protected $productOptionsConfig;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var ArrayManager
     */
    protected $arrayManager;

    /**
     * @var array
     */
    protected $meta = [];

    /**
     * @var CurrencyInterface
     */
    private $localeCurrency;

    /**
     * @param LocatorInterface $locator
     * @param StoreManagerInterface $storeManager
     * @param ConfigInterface $productOptionsConfig
     * @param UrlInterface $urlBuilder
     * @param ArrayManager $arrayManager
     */
    public function __construct(
        LocatorInterface $locator,
        StoreManagerInterface $storeManager,
        ConfigInterface $productOptionsConfig,
        UrlInterface $urlBuilder,
        ArrayManager $arrayManager
    ) {
        $this->locator = $locator;
        $this->storeManager = $storeManager;
        $this->productOptionsConfig = $productOptionsConfig;
        $this->urlBuilder = $urlBuilder;
        $this->arrayManager = $arrayManager;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        $options = [];
        $productOptions = $this->locator->getProduct()->getOptions() ?: [];

        /** @var \Magento\Catalog\Model\Product\Option $option */
        foreach ($productOptions as $index => $option) {
            $options[$index] = $this->formatPriceByPath(static::FIELD_PRICE_NAME, $option->getData());
            $values = $option->getValues() ?: [];

            /** @var \Magento\Catalog\Model\Product\Option $value */
            foreach ($values as $value) {
                $options[$index][static::GRID_TYPE_SELECT_NAME][] = $this->formatPriceByPath(
                    static::FIELD_PRICE_NAME,
                    $value->getData()
                );
            }
        }

        return array_replace_recursive(
            $data,
            [
                $this->locator->getProduct()->getId() => [
                    static::DATA_SOURCE_DEFAULT => [
                        static::FIELD_ENABLE => 1,
                        static::GRID_OPTIONS_NAME => $options
                    ]
                ]
            ]
        );
    }

    /**
     * Format float number to have two digits after delimiter
     *
     * @param string $path
     * @param array $data
     * @return array
     */
    protected function formatPriceByPath($path, array $data)
    {
        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;
        $product = $this->locator->getProduct()->getData();
        if ($product['type_id'] == self::GROUP_CUSTOM_OPTIONS_NAME) {
            $this->createCustomOptionsPanel();
        }
        return $this->meta;
    }

    /**
     * Create "Customizable Options" panel
     *
     * @return $this
     */
    protected function createCustomOptionsPanel()
    {
        $this->meta = array_replace_recursive(
            $this->meta,
            [
                static::GROUP_CUSTOM_OPTIONS_NAME => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('License Delivery'),
                                'componentType' => Fieldset::NAME,
                                'dataScope' => static::GROUP_CUSTOM_OPTIONS_SCOPE,
                                'collapsible' => true,
                                'sortOrder' => $this->getNextGroupSortOrder(
                                    $this->meta,
                                    static::GROUP_CUSTOM_OPTIONS_PREVIOUS_NAME,
                                    static::GROUP_CUSTOM_OPTIONS_DEFAULT_SORT_ORDER
                                ),
                            ],
                        ],
                    ],
                    'children' => [
//                        static::CONTAINER_TERM_CONDITION => $this->getTermsAnsConditions(5),
                        static::CONTAINER_LICENSE_GRID => $this->getLicenseGrid(10),
                        static::CONTAINER_HEADER_NAME => $this->getHeaderContainerConfig(20),
                        static::GRID_OPTIONS_NAME => $this->getOptionsGridConfig(30),
                        static::GRID_ZIP_IMPORT_NAME => $this->getZipImportGridConfig(40),
                        static::GRID_IMAGE_IMPORT_NAME => $this->getImageImportGridConfig(50)
                    ]
                ]
            ]
        );

        return $this;
    }

    public function getTermsAnsConditions($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Terms And Conditions'),
                        'componentType' => Fieldset::NAME,
                        'dataProvider' => static::CUSTOM_OPTIONS_LISTING,
                        'collapsible' => true,
                        'sortOrder' => $sortOrder,
                        'required' => "0",
                    ],
                ],
            ],
            'children' => [
                static::CONTAINER_TERM_CONDITION => [
                    "arguments" => [
                        "data" => [
                            "config" => [
                                "formElement" => "container",
                                "componentType" => "container",
                                "breakLine" => false,
                                "label" => "Terms and Conditions",
                                "component" => "Magento_Ui\/js\/form\/components\/group",
                                "sortOrder" => 0,
                                "required" => "0",
                            ]
                        ]
                    ],
                    "children" => [
                        "term_condition" => [
                            "arguments" => [
                                "data" => [
                                    "config" => [
                                        "dataType" => "textarea",
                                        "formElement" => "wysiwyg",
                                        "visible" => "1",
                                        "required" => "0",
                                        "notice" => null,
                                        "default" => null,
                                        "label" => "Terms and Conditions",
                                        "code" => "term_condition",
                                        "source" => "content",
                                        "scopeLabel" => "[STORE VIEW]",
                                        "globalScope" => false,
                                        "sortOrder" => 0,
                                        "componentType" => "field",
                                        "wysiwyg" => true,
                                        "wysiwygConfigData" => [
                                            "add_variables" => true,
                                            "add_widgets" => true,
                                            "add_directives" => true,
                                            "use_container" => true,
                                            "container_class" => "hor-scroll",
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]

                ]
            ],
        ];
    }

    public function getLicenseGrid($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Game Code'),
                        'componentType' => Fieldset::NAME,
                        'dataScope' => static::GROUP_CUSTOM_OPTIONS_SCOPE,
                        'collapsible' => true,
                        'sortOrder' => $sortOrder
                    ],
                ],
            ],
            'children' => [
                static::GRID_LICENSE_NAME => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'autoRender' => true,
                                'componentType' => 'insertListing',
                                'dataScope' => 'product_license_listing',
                                'externalProvider' => 'product_license_listing.product_license_listing_data_source',
                                'selectionsProvider' => 'product_license_listing.product_license_listing.product_license_columns.product_id',
                                'ns' => 'product_license_listing',
                                'render_url' => $this->urlBuilder->getUrl('mui/index/render'),
                                'realTimeLink' => false,
                                'behaviourType' => 'simple',
                                'externalFilterMode' => true,
                                'imports' => [
                                    'productId' => '${ $.provider }:data.product.current_product_id'
                                ],
                                'exports' => [
                                    'productId' => '${ $.externalProvider }:params.current_product_id'
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ];
    }

    /**
     * Get config for header container
     *
     * @param int $sortOrder
     * @return array
     */
    protected function getHeaderContainerConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => null,
                        'formElement' => Container::NAME,
                        'componentType' => Container::NAME,
//                        'template' => 'ui/form/components/complex',
                        'template' => 'Magenest_Pin/complex',
                        'sortOrder' => $sortOrder,
                        'content' => __(' '),
                    ],
                ],
            ],
            'children' => [
                static::BUTTON_ADD => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'title' => __('Add Text Code'),
                                'formElement' => Container::NAME,
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/form/components/button',
                                'sortOrder' => 20,
                                'actions' => [
                                    [
                                        'targetName' => ' index = ' . static::GRID_OPTIONS_NAME,
                                        'actionName' => 'processingAddChild',
                                    ]
                                ]
                            ]
                        ],
                    ],
                ],

                static::BUTTON_IMPORT_ZIP => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'title' => __('Import File Zip'),
                                'formElement' => Container::NAME,
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/form/components/button',
                                'sortOrder' => 30,
                                'actions' => [
                                    [
                                        'targetName' => ' index = ' . static::GRID_ZIP_IMPORT_NAME,
                                        'actionName' => 'processingAddChild',
                                    ]
                                ]
                            ]
                        ],
                    ],
                ],

                static::BUTTON_IMPORT_IMAGE => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'title' => __('Import Image'),
                                'formElement' => Container::NAME,
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/form/components/button',
                                'sortOrder' => 30,
                                'actions' => [
                                    [
                                        'targetName' => ' index = ' . static::GRID_IMAGE_IMPORT_NAME,
                                        'actionName' => 'processingAddChild',
                                    ]
                                ]
                            ]
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Get config for the whole grid
     *
     * @param int $sortOrder
     * @return array
     */
    protected function getOptionsGridConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'addButtonLabel' => __('Add Option'),
                        'componentType' => DynamicRows::NAME,
                        'component' => 'Magento_Catalog/js/components/dynamic-rows-import-custom-options',
                        'template' => 'Magenest_Pin/collapsible',
                        'additionalClasses' => 'admin__field-wide',
                        'deleteProperty' => static::FIELD_IS_DELETE,
                        'deleteValue' => '1',
                        'addButton' => false,
                        'renderDefaultRecord' => false,
                        'columnsHeader' => false,
                        'collapsibleHeader' => true,
                        'sortOrder' => $sortOrder,
                        'dataProvider' => static::CUSTOM_OPTIONS_LISTING,
                        'links' => ['insertData' => '${ $.provider }:${ $.dataProvider }'],
                    ],
                ],
            ],
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'headerLabel' => __('New Game Code'),
                                'componentType' => Container::NAME,
                                'component' => 'Magenest_Pin/js/dynamic-rows/record',
                                'positionProvider' => static::CONTAINER_OPTION . '.' . static::FIELD_SORT_ORDER_NAME,
                                'isTemplate' => true,
                                'is_collection' => true,
                            ],
                        ],
                    ],
                    'children' => [
                        static::CONTAINER_OPTION => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'componentType' => Fieldset::NAME,
                                        'label' => null,
                                        'sortOrder' => 10,
                                        'opened' => true,
                                    ],
                                ],
                            ],
                            'children' => [
                                static::CONTAINER_PIN_SUPPLIER => $this->getSupplierConfig(10),
//                                static::CONTAINER_PIN_FILE => $this->getFileColumn(),
                            ]
                        ],
                    ]
                ]
            ]
        ];
    }

    /**
     * Get config for the whole grid
     *
     * @param int $sortOrder
     * @return array
     */
    protected function getZipImportGridConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'addButtonLabel' => __('Add Option'),
                        'componentType' => DynamicRows::NAME,
                        'component' => 'Magento_Catalog/js/components/dynamic-rows-import-custom-options',
                        'template' => 'Magenest_Pin/collapsible',
                        'additionalClasses' => 'admin__field-wide',
                        'deleteProperty' => static::FIELD_IS_DELETE,
                        'deleteValue' => '1',
                        'addButton' => false,
                        'renderDefaultRecord' => false,
                        'columnsHeader' => false,
                        'collapsibleHeader' => true,
                        'sortOrder' => $sortOrder,
                        'dataProvider' => static::CUSTOM_OPTIONS_LISTING,
                        'links' => ['insertData' => '${ $.provider }:${ $.dataProvider }'],
                    ],
                ],
            ],
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'headerLabel' => __('Import Zip File'),
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/dynamic-rows/record',
                                'positionProvider' => static::CONTAINER_OPTION . '.' . static::FIELD_SORT_ORDER_NAME,
                                'isTemplate' => true,
                                'is_collection' => true,
                            ],
                        ],
                    ],
                    'children' => [
                        static::CONTAINER_OPTION => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'componentType' => Fieldset::NAME,
                                        'label' => null,
                                        'sortOrder' => 10,
                                        'opened' => true,
                                    ],
                                ],
                            ],
                            'children' => [
                                static::CONTAINER_PIN_SUPPLIER => $this->getZipSupplierConfig(10),
                            ]
                        ],
                    ]
                ]
            ]
        ];
    }

    protected function getImageImportGridConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'addButtonLabel' => __('Add Option'),
                        'componentType' => DynamicRows::NAME,
                        'component' => 'Magento_Catalog/js/components/dynamic-rows-import-custom-options',
                        'template' => 'Magenest_Pin/collapsible',
                        'additionalClasses' => 'admin__field-wide',
                        'deleteProperty' => static::FIELD_IS_DELETE,
                        'deleteValue' => '1',
                        'addButton' => false,
                        'renderDefaultRecord' => false,
                        'columnsHeader' => false,
                        'collapsibleHeader' => true,
                        'sortOrder' => $sortOrder,
                        'dataProvider' => static::CUSTOM_OPTIONS_LISTING,
                        'links' => ['insertData' => '${ $.provider }:${ $.dataProvider }'],
                    ],
                ],
            ],
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'headerLabel' => __('Import Image File '),
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/dynamic-rows/record',
                                'positionProvider' => static::CONTAINER_OPTION . '.' . static::FIELD_SORT_ORDER_NAME,
                                'isTemplate' => true,
                                'is_collection' => true,
                            ],
                        ],
                    ],
                    'children' => [
                        static::CONTAINER_OPTION => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'componentType' => Fieldset::NAME,
                                        'label' => null,
                                        'sortOrder' => 10,
                                        'opened' => true,
                                    ],
                                ],
                            ],
                            'children' => [
                                static::CONTAINER_PIN_SUPPLIER => $this->getImageSupplierConfig(10),

                            ]
                        ],
                    ]
                ]
            ]
        ];
    }

    /**
     * Get config for "Title" fields
     *
     * @param int $sortOrder
     * @param array $options
     * @return array
     */
    protected function getTitleFieldConfig($sortOrder, array $options = [])
    {
        return array_replace_recursive(
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => __('Game Code'),
                            'componentType' => Field::NAME,
                            'formElement' => Input::NAME,
                            'dataScope' => static::FIELD_PIN_CODE,
                            'dataType' => Text::NAME,
                            'sortOrder' => $sortOrder,
                            'validation' => [
                                'required-entry' => false
                            ],
                        ],
                    ],
                ],
            ],
            $options
        );
    }

    /**
     * Get config for container with common fields for any type
     *
     * @param int $sortOrder
     * @return array
     */
    protected function getSupplierConfig($sortOrder)
    {
        $commonContainer = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Container::NAME,
                        'formElement' => Container::NAME,
                        'component' => 'Magento_Ui/js/form/components/group',
                        'breakLine' => false,
                        'showLabel' => false,
                        'additionalClasses' => 'admin__field-group-columns admin__control-group-equal',
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
            'children' => [
                static::FIELD_PIN_SUPPLIER => $this->getConfig(
                    20,
                    [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'label' => __('Additional Comments'),
                                    'component' => 'Magento_Catalog/component/static-type-input',
                                    'valueUpdate' => 'input',
                                    'imports' => [
                                        'optionId' => '${ $.provider }:${ $.parentScope }.option_id'
                                    ]
                                ],
                            ],
                        ],
                    ]
                ),
                static::FIELD_PIN_CODE => $this->getTitleFieldConfig(10),
            ]
        ];

        return $commonContainer;
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getZipSupplierConfig($sortOrder)
    {
        $commonContainer = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Container::NAME,
                        'formElement' => Container::NAME,
                        'component' => 'Magento_Ui/js/form/components/group',
                        'breakLine' => false,
                        'showLabel' => false,
                        'additionalClasses' => 'admin__field-group-columns admin__control-group-equal',
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
            'children' => [
                static::FIELD_PIN_SUPPLIER => $this->getConfig(
                    20,
                    [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'label' => __('Additional Comments'),
                                    'component' => 'Magento_Catalog/component/static-type-input',
                                    'valueUpdate' => 'input',
                                    'notice' => 'Please note that the name of the zip file and file folder are the same and it does not contain both space and special characters',
                                    'imports' => [
                                        'optionId' => '${ $.provider }:${ $.parentScope }.option_id'
                                    ]
                                ],
                            ],
                        ],
                    ]
                ),
                'links_file' => $this->getConfig(
                    30,
                    [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'label' => __('Zip File'),
                                    'formElement' => 'fileUploader',
                                    'componentType' => 'fileUploader',
                                    'component' => 'Magento_Downloadable/js/components/file-uploader',
                                    'elementTmpl' => 'Magento_Downloadable/components/file-uploader',
                                    'fileInputName' => 'pin_code',
                                    'notice' => 'Please only use the Zip file which only contains the supported format (.JPG, .PNG or JPEG) and the maximum image file size (<64 KB)',
                                    'maxFileSize' => static::MAX_SIZE,
                                    'allowedExtensions' => static::ALLOWED_ZIP_EXTENSIONS,
                                    'uploaderConfig' => [
                                        'url' => $this->urlBuilder->getUrl(
                                            'pin/pin_file/upload',
                                            ['type' => 'pin_code', '_secure' => true]
                                        ),
                                    ],
                                    'dataScope' => 'pin_file',
                                    'validation' => [
                                        'required-entry' => false,
                                    ],
                                ],
                            ],
                        ],
                    ]
                ),
            ]
        ];

        return $commonContainer;
    }

    /**
     * @param $sortOrder
     * @return array
     * Get image import element
     */
    protected function getImageSupplierConfig($sortOrder)
    {
        $commonContainer = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Container::NAME,
                        'formElement' => Container::NAME,
                        'component' => 'Magento_Ui/js/form/components/group',
                        'breakLine' => false,
                        'showLabel' => false,
                        'additionalClasses' => 'admin__field-group-columns admin__control-group-equal',
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
            'children' => [
                static::FIELD_PIN_SUPPLIER => $this->getConfig(
                    20,
                    [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'label' => __('Additional Comments'),
                                    'component' => 'Magento_Catalog/component/static-type-input',
                                    'notice' => 'Please note that the name of the image file does not contain both space and special characters',
                                    'valueUpdate' => 'input',
                                    'imports' => [
                                        'optionId' => '${ $.provider }:${ $.parentScope }.option_id'
                                    ]
                                ],
                            ],
                        ],
                    ]
                ),
                'links_file' => $this->getConfig(
                    30,
                    [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'label' => __('Image File'),
                                    'formElement' => 'fileUploader',
                                    'componentType' => 'fileUploader',
                                    'component' => 'Magento_Downloadable/js/components/file-uploader',
                                    'elementTmpl' => 'Magento_Downloadable/components/file-uploader',
                                    'fileInputName' => 'pin_code',
                                    'notice' => 'Please only use the file which only contains the supported format (.JPG, .PNG or JPEG) and the image maximum file size (<64 KB)',
                                    'allowedExtensions' => static::ALLOWED_IMAGE_EXTENSIONS,
                                    'maxFileSize' => static::MAX_SIZE,
                                    'uploaderConfig' => [
                                        'url' => $this->urlBuilder->getUrl(
                                            'pin/pin_file/upload',
                                            ['type' => 'pin_code', '_secure' => true]
                                        ),
                                    ],
                                    'dataScope' => 'pin_file',
                                    'validation' => [
                                        'required-entry' => false,
                                    ],
                                ],
                            ],
                        ],
                    ]
                ),
            ]
        ];

        return $commonContainer;
    }

    /**
     * Get config for "Title" fields
     *
     * @param int $sortOrder
     * @param array $options
     * @return array
     */
    protected function getConfig($sortOrder, array $options = [])
    {
        return array_replace_recursive(
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => __('Title'),
                            'componentType' => Field::NAME,
                            'formElement' => Input::NAME,
                            'dataScope' => static::FIELD_PIN_SUPPLIER,
                            'dataType' => Text::NAME,
                            'sortOrder' => $sortOrder,
                            'validation' => [
                                'required-entry' => false
                            ],
                        ],
                    ],
                ],
            ],
            $options
        );
    }

    /**
     * Get config for "Option Type" field
     *
     * @param int $sortOrder
     * @return array
     */
    protected function getTypeFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Game Code File'),
                        'componentType' => Field::NAME,
                        'formElement' => Select::NAME,
                        'component' => 'Magento_Catalog/js/custom-options-type',
                        'selectType' => 'select',
                        'sortOrder' => $sortOrder,
                        'disableLabel' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    protected function getFileColumn()
    {
        $fileContainer['arguments']['data']['config'] = [
            'componentType' => Container::NAME,
            'formElement' => Container::NAME,
            'component' => 'Magento_Ui/js/form/components/group',
            'label' => __('File'),
            'dataScope' => '',
        ];

        $fileUploader['arguments']['data']['config'] = [
            'formElement' => 'fileUploader',
            'componentType' => 'fileUploader',
            'component' => 'Magento_Downloadable/js/components/file-uploader',
            'elementTmpl' => 'Magento_Downloadable/components/file-uploader',
            'fileInputName' => 'pin_code',
            'uploaderConfig' => [
                'url' => $this->urlBuilder->getUrl(
                    'pin/pin_file/upload',
                    ['type' => 'pin_code', '_secure' => true]
                ),
            ],
            'dataScope' => 'pin_file',
            'validation' => [
                'required-entry' => false,
            ],
        ];

        return $this->arrayManager->set(
            'children',
            $fileContainer,
            [
                'links_file' => $fileUploader
            ]
        );
    }
}
