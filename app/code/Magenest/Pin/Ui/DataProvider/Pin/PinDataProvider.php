<?php
/**
 * Created by PhpStorm.
 * User: ninhvu
 * Date: 25/04/2018
 * Time: 13:33
 */

namespace Magenest\Pin\Ui\DataProvider\Pin;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Api\Search\ReportingInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Psr\Log\LoggerInterface;

class PinDataProvider extends AbstractDataProvider
{
    /**
     * @var \Magenest\Pin\Model\PinFactory
     */
    protected $pinFactory;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * LicenseDataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Magenest\Pin\Model\PinFactory $pinFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Magenest\Pin\Model\ResourceModel\Pin\CollectionFactory $pinFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $pinFactory->create();
    }

    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
        $fields = ['created_at', 'status', 'product_id'];
        if (in_array($filter->getField(), $fields)) {
            $filter->setField("main_table.{$filter->getField()}");
        }
        parent::addFilter($filter);
    }
}