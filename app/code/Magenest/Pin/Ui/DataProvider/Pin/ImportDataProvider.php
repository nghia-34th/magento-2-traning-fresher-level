<?php
/**
 * Created by PhpStorm.
 * User: ninhvu
 * Date: 02/05/2018
 * Time: 13:02
 */

namespace Magenest\Pin\Ui\DataProvider\Pin;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\Api\Search\ReportingInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Psr\Log\LoggerInterface;

class ImportDataProvider extends \Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider
{
    /**
     * @var
     */
    protected $importFactory;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * LicenseDataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param ReportingInterface $reporting
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param RequestInterface $request
     * @param FilterBuilder $filterBuilder
     * @param LoggerInterface $logger
     * @param \Magenest\Pin\Model\ResourceModel\Import\Collection $importFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        ReportingInterface $reporting,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        RequestInterface $request,
        FilterBuilder $filterBuilder,
        LoggerInterface $logger,
        \Magenest\Pin\Model\ResourceModel\Import\Collection $importFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $reporting, $searchCriteriaBuilder, $request, $filterBuilder, $meta, $data);
        $this->_logger = $logger;
        $collection = $importFactory;
        $this->collection = $collection;
        $this->request = $request;
    }

    public function getSearchResult()
    {
        $result = parent::getSearchResult();
        $result->getSelect()->joinleft(
            ['dmdm' => $result->getTable('magenest_pin_code')],
            'main_table.pin_id = dmdm.id',
            ['product_id', 'product_name', "code", "status", "comments", 'pin_type']
        );
        return $result;
    }
}
