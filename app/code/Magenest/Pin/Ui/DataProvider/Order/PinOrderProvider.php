<?php
/**
 * Created by PhpStorm.
 * User: ninhvu
 * Date: 04/11/2017
 * Time: 16:49
 */

namespace Magenest\Pin\Ui\DataProvider\Order;

use Magento\Framework\App\RequestInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magenest\Pin\Model\PurchasedFactory;
use Psr\Log\LoggerInterface;

/**
 * Class DataProvider
 *
 * @api
 *
 * @method
 * @since 100.1.0
 */
class PinOrderProvider extends AbstractDataProvider
{
    /**
     * @var PurchasedFactory
     */
    protected $_purchasedFactory;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var RequestInterface
     */
    protected $request;

    protected $array = ['id', 'order_increment_id', 'customer_id', 'pin_code', 'status', 'created_at', 'order_id'];

    /**
     * LicenseDataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param LoggerInterface $logger
     * @param PurchasedFactory $purchasedFactory
     * @param RequestInterface $request
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        LoggerInterface $logger,
        PurchasedFactory $purchasedFactory,
        RequestInterface $request,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->_logger = $logger;
        $this->_purchasedFactory = $purchasedFactory;
        $this->collection = $this->_purchasedFactory->create()->getCollection()->addFieldToSelect($this->array);
        $this->request = $request;
    }
}
