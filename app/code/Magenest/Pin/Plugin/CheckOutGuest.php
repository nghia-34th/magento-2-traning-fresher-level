<?php
/**
 * Created by PhpStorm.
 * User: ninhvu
 * Date: 12/01/2018
 * Time: 14:48
 */
namespace Magenest\Pin\Plugin;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;

class CheckOutGuest
{
    /**
     * @var \Magento\Quote\Api\GuestCartManagementInterface
     */
    protected $cartManagement;
    /**
     * @var \Magento\Checkout\Model\CartFactory
     */
    protected $_cartFactory;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    /**
     * @var
     */
    protected $_customerSession;
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @param \Magento\Quote\Api\GuestCartManagementInterface $cartManagement
     * @param \Magento\Checkout\Model\CartFactory $cartFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     */
    public function __construct(
        \Magento\Quote\Api\GuestCartManagementInterface $cartManagement,
        \Magento\Checkout\Model\CartFactory $cartFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {

        $this->_productFactory=$productFactory;
        $this->_scopeConfig=$scopeConfig;
        $this->_cartFactory=$cartFactory;
        $this->cartManagement=$cartManagement;
    }

    public function aroundSavePaymentInformationAndPlaceOrder(
        \Magento\Checkout\Model\GuestPaymentInformationManagement $object,
        callable $proceed,
        $cartId,
        $email,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        $config=$this->_scopeConfig->getValue('pin/pin/disable_guest_checkout');
        if ($config==1) {
            $quoteItems=$this->_cartFactory->create()->getItems();
            foreach ($quoteItems as $item) {
                /** @var  $item \Magento\Quote\Model\Quote\Item */
                $product = $item->getProduct();
                $product = $this->_productFactory->create()->load($product->getId());
                $gamecode = $product->getTypeId();
                if ($gamecode=='license') {
                    throw new CouldNotSaveException(
                        __('Please login to place order !'),
                        null
                    );
                }
            }
        }

        $object->savePaymentInformation($cartId, $email, $paymentMethod, $billingAddress);
        try {
            $orderId = $this->cartManagement->placeOrder($cartId);
        }catch (LocalizedException $exception){
            throw $exception;
        } catch (\Exception $e) {

            throw new CouldNotSaveException(
                __('An error occurred on the server. Please try to place the order again.'),
                $e
            );
        }
        return $orderId;
    }
}
