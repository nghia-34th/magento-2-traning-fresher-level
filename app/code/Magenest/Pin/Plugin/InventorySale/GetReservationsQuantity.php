<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 05/01/2019
 * Time: 16:17
 */

namespace Magenest\Pin\Plugin\InventorySale;

use Magenest\Pin\Model\Pin;
use Magento\Framework\Exception\LocalizedException;

class GetReservationsQuantity
{
    const LICENSE = 'license';
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magenest\Pin\Model\PinFactory
     */
    protected $pinFactory;

    protected $productRepository;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param \Magenest\Pin\Model\PinFactory $pinFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magenest\Pin\Model\PinFactory $pinFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->productFactory = $productFactory;
        $this->pinFactory = $pinFactory;
    }

    /**
     * @inheritdoc
     */
    public function afterExecute(
        \Magento\InventoryReservations\Model\ResourceModel\GetReservationsQuantity $object,
        $result,
        string $sku,
        int $stockId
    ) {
        $product = $this->productFactory->create()->getCollection()->addFieldToFilter('sku', $sku)->getFirstItem();
        if (!$product->getId()) {
            return $result;
        }
        if ($product->getTypeId() != self::LICENSE) {
            return $result;
        }
        if (!$this->isQtyAutoUpdated()) {
            return $result;
        }

        $collection = $this->pinFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $product->getId())
            ->addFieldToFilter('status', Pin::STATUS_DELIVERED);
        return $collection->getSize();
    }

    protected function isQtyAutoUpdated()
    {
        $isAutoQtyEnable =(bool)$this->scopeConfig->getValue('pin/inventory/auto_update');
        return $isAutoQtyEnable;
    }
}
