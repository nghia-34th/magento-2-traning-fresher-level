<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 03/01/2019
 * Time: 16:02
 */

namespace Magenest\Pin\Plugin\InventorySale;

use Magenest\Pin\Model\Pin;

class GetProductSalableQty
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magenest\Pin\Model\PinFactory
     */
    protected $pinFactory;

    /**
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magenest\Pin\Model\PinFactory $pinFactory
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magenest\Pin\Model\PinFactory $pinFactory
    ) {
        $this->pinFactory = $pinFactory;
        $this->scopeConfig = $scopeConfig;
        $this->productRepository = $productRepository;
    }

    public function afterExecute(
        \Magento\InventorySales\Model\GetProductSalableQty $object,
        $result,
        string $sku,
        int $stockId
    ) {
        if ($this->isQtyControlByLicense($sku)) {
            return $this->getSalableQtyOfLicense($sku);
        }
        return $result;
    }

    protected function isQtyControlByLicense($sku)
    {
        $product = $this->productRepository->get($sku);
        $isLicenseProduct = $product->getTypeId() == 'license';
        if (!$isLicenseProduct) return false;
        $isAutoQtyEnable = $this->scopeConfig->getValue('pin/inventory/auto_update') == 1;
        if (!$isAutoQtyEnable) return false;
        return true;
    }

    protected function getSalableQtyOfLicense($sku)
    {
        $product = $this->productRepository->get($sku);
        $productId = $product->getId();
        $collection = $this->pinFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('status', Pin::STATUS_ENABLED);
        return $collection->getSize();
    }
}
