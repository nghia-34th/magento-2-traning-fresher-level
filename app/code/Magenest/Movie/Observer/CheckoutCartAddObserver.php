<?php

namespace Magenest\Movie\Observer;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Serialize\SerializerInterface;

class CheckoutCartAddObserver implements ObserverInterface
{
    private RequestInterface $_request;
    private SerializerInterface $serializer;

    public function __construct(RequestInterface $request, SerializerInterface $serializer)
    {
        $this->_request = $request;
        $this->serializer = $serializer;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        $postValue = $this->_request->getParams();

        if (isset($postValue['shipping_time'])) {
            $item = $observer->getQuoteItem();

            $shipping_time = [];
            $shipping_time[] = [
                'label' => 'Shipping Time',
                'value' => $postValue['shipping_time'],
            ];

            if (count($shipping_time) > 0) {
                $item->addOption([
                    'product_id' => $item->getProductId(),
                    'code' => 'additional_options',
                    'value' => $this->serializer->serialize($shipping_time),
                ]);
            }
        }
    }
}
