<?php

namespace Magenest\Movie\Observer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class CustomerAfterSave implements ObserverInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepository;

    public function __construct(LoggerInterface $logger, CustomerRepositoryInterface $customerRepository)
    {
        $this->logger = $logger;
        $this->_customerRepository = $customerRepository;
    }

    public function execute(Observer $observer)
    {
        $customer = $observer->getCustomer()->setFirstName('Magenest');
        $this->_customerRepository->save($customer);
    }
}
