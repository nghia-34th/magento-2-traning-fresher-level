<?php

namespace Magenest\Movie\Observer;

use Magenest\Movie\Model\CourseFactory;
use Magenest\Movie\Model\ResourceModel\Course;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ProductBeforeSave implements ObserverInterface
{
    public function __construct(RequestInterface $request, CourseFactory $courseFactory, Course $resourceCourse)
    {
        $this->request = $request;
        $this->courseFactory = $courseFactory;
        $this->resourceCourse = $resourceCourse;
    }

    public function execute(Observer $observer)
    {
        $product = $observer->getProduct();
        $course = $this->courseFactory->create();

        if ($product->getEntityId()) {
            if ($texts = $product->getText()) {
                foreach ($texts as $text) {
                    $data = [
                        'product' => $product->getEntityId(),
                        'type' => 'text',
                        'value' => $text['value'],
                    ];

                    $object = $course->setData($data);
                    $this->resourceCourse->save($object);
                }
            }

            if ($images = $product->getImages()) {
                foreach ($images as $image) {
                    $data = [
                        'product' => $product->getEntityId(),
                        'type' => 'image',
                        'value' => $image['pin_file']['0']['file'],
                    ];

                    $object = $course->setData($data);
                    $this->resourceCourse->save($object);
                }
            }
        }
    }
}
