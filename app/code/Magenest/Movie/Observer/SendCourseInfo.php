<?php

namespace Magenest\Movie\Observer;

use Magenest\Movie\Model\ResourceModel\Course\Collection;
use Magento\Framework\App\Area;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\StoreManagerInterface;

class SendCourseInfo implements ObserverInterface
{
    public function __construct(
        StoreManagerInterface $storeManager,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        Collection $courseCollection
    ) {
        $this->storeManager = $storeManager;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->course = $courseCollection;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
//        $templateOptions = array('area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $this->storeManager->getStore()->getId());
//        $templateVars = array(
//            'store' => $this->storeManager->getStore(),
//            'customer_name' => 'John Doe',
//            'message'    => 'Hello World!!.'
//        );
//        $from = array('email' => "test@webkul.com", 'name' => 'Name of Sender');
//        $this->inlineTranslation->suspend();
//        $to = array('hijefe5764@lubde.com');
//        $transport = $this->_transportBuilder->setTemplateIdentifier('course_doc_template')
//            ->setTemplateOptions($templateOptions)
//            ->setTemplateVars($templateVars)
//            ->setFromByScope($from)
//            ->addTo($to)
//            ->getTransport();
//        $transport->sendMessage();
//        $this->inlineTranslation->resume();
    }
}
