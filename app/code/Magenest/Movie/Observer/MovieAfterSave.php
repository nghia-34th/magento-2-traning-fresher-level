<?php

namespace Magenest\Movie\Observer;

use Magenest\Movie\Model\Movie;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class MovieAfterSave implements ObserverInterface
{
    public function __construct(Movie $movie)
    {
        $this->movie = $movie;
    }

    public function execute(Observer $observer)
    {
        $observer->getMovie()->setRating(0);
        $model = $observer->getMovie()->toArray();
        $this->movie->setData($model)->save();
    }
}
