<?php

namespace Magenest\Movie\Observer;

use Magento\Config\Model\Config\Factory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ConfigBeforeSave implements ObserverInterface
{
    /**
     * Backend Config Model Factory
     *
     * @var \Magento\Config\Model\Config\Factory
     */
    protected $_configFactory;

    public function __construct(Factory $configFactory)
    {
        $this->_configFactory = $configFactory;
    }

    public function execute(Observer $observer)
    {
        $field_value = $observer->getDataByKey('data_object')->getValue();
        if ($field_value == 'Ping'){
            $observer->getDataByKey('data_object')->setValue('Pong');
        }
    }
}
