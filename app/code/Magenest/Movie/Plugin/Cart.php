<?php

namespace Magenest\Movie\Plugin;

use Magento\Framework\App\RequestInterface;

class Cart
{
    public function __construct(RequestInterface $request)
    {
        $this->request = $request;
    }

    public function aroundAddProduct(
        \Magento\Checkout\Model\Cart $subject,
        callable $proceed,
        $productInfo,
        $requestInfo = null
    ) {
        $quote = $proceed($productInfo, $requestInfo);
        $quote_items = $quote->getItems()->getItems();
        $item_key = array_key_last($quote_items);
        $item = $quote_items[$item_key];

        $shipping_time = $this->request->getParam('shipping_time');
        if (isset($shipping_time) && $shipping_time != '') {
            $option = array();
            $option[] = [
                'label' => 'Shipping Time',
                'value' => $shipping_time,
            ];

            if (count($option) > 0) {
                $item->addOption([
                    'product_id' => $item->getProductId(),
                    'code' => 'additional_options',
                    'value' => json_encode($option),
                ]);
            }
        }
    }
}
