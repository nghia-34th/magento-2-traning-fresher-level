<?php

namespace Magenest\Movie\Plugin;

class Image
{
    public function aroundGetItemData(
        \Magento\Checkout\CustomerData\AbstractItem $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote\Item $item
    ){
        $data = $proceed($item);
        $objectManager =\Magento\Framework\App\ObjectManager::getInstance();
        $helperImport = $objectManager->get('\Magento\Catalog\Helper\Image');

        $product = $item->getChildren()[0]->getProduct();
        $src = $helperImport->init($product, 'product_page_image_small')
            ->setImageFile($product->getSmallImage()) // image,small_image,thumbnail
            ->resize(380)
            ->getUrl();
        $data['product_name'] = $item->getChildren()[0]->getName();
        $data['product_image']['src'] = $src;
        return $data;
    }
}
