<?php

namespace Magenest\Movie\Plugin;

use Magento\Framework\App\RequestInterface;

class Movie
{
    public function __construct(RequestInterface $request)
    {
        $this->request = $request;
    }

    public function beforeExecute()
    {
        $request = $this->request->getPostValue();
        $data = $request['contact'];
        $data['rating'] = 0;
        $this->request->setPostValue('contact', $data);
    }
}
