<?php

namespace Magenest\Movie\Plugin;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\RequestInterface;

class Customer
{
    protected $customerRepository;

    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        RequestInterface $request
    ) {
        $this->customerRepository = $customerRepository;
        $this->request = $request;
    }

    public function beforeExecute()
    {
        $request = $this->request->getPostValue();
        $customer_data = $request['customer'];
        $customer_data['firstname'] = 'Magenest';
        $this->request->setPostValue('customer', $customer_data);
    }
}
