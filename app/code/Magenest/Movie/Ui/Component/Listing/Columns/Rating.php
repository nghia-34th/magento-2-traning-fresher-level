<?php

namespace Magenest\Movie\Ui\Component\Listing\Columns;

use Magento\Ui\Component\Listing\Columns\Column;

class Rating extends Column
{
    public function prepareDataSource(array $dataSource)
    {
        if(isset($dataSource['data']['items'])){
            foreach($dataSource['data']['items'] as &$item){
                if(isset($item['rating'])){
                    $data = [];
                    for($i = 0; $i < 5; $i++)
                        if($i < $item['rating'])
                            $data[] = 'selected';
                        else
                            $data[] = 'notSelected';

                    $item['rating'] = $data;
                }
            }
        }
        return $dataSource;
    }
}
