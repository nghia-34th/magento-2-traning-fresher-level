var config = {
    config: {
        map: {
            'Magento_Ui/js/lib/validation/rules': {
                'Magenest_Movie/js/validation-mixin': true
            }
        },
        mixins: {
            'mage/validation': {
                'Magenest_Movie/js/validation': true
            }
        }
    }
}
