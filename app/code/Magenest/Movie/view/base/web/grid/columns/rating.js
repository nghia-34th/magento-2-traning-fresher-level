define([
    'Magento_Ui/js/grid/columns/column',
    'text!Magenest_Movie/template/grid/cells/rating/rating.html',
], function (Column){
    'use strict';

    return Column.extend({
        defaults: {
            fieldClass: {
                'data-grid-thumbnail-cell': true
            }
        },
        /**
         * Initialization method
         */
        initialize: function () {
            this._super();
        }
    });
})
