define([
    'jquery',
    'ko',
], function($,ko) {
    this.color = ko.observable();

    $('document').ready(function() {
        $("#bg-switcher").change(function() {
            var color = $("#bg-switcher option:selected").val();
            $("body").css("background-color", color);
        });
    });
});
