require([
        'Magento_Ui/js/lib/validation/validator',
        'jquery',
        'mage/translate'
], function(validator, $) {
    validator.addRule(
        'validate-phone-number',
        function(value) {
            //remove white spaces
            var phone = value.replace(/\s/g,'');

            //Numeric check
            if (isNaN(phone)) {
                this.phoneErrorMessage = $.mage.__('Phone must be numeric.');

                return false;
            }

            //Digit check
            var maxDigit = 10;

            if (phone.length > maxDigit) {
                this.phoneErrorMessage = $.mage.__('Phone digit must not exceed 10.');

                return false;
            }

            //Valid phone code check
            var code = phone.substr(0,2);
            if (code !== '84' && code.substr(0,1) !== '0') {
                this.phoneErrorMessage = $.mage.__('Not a valid phone number.');

                return false;
            }

            return  phone;

        }, function () {
            return this.phoneErrorMessage;
        }
    )
});
