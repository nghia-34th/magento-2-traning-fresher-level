<?php

namespace Magenest\Movie\Controller;

use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterInterface;

class Router implements RouterInterface
{
    private $actionFactory;
    private $response;

    public function __construct(
        ActionFactory $actionFactory,
        ResponseInterface $response

    ) {
        $this->actionFactory = $actionFactory;
        $this->_response = $response;
    }

    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');
        /** Apply Url logic here */
        $this->_response->setRedirect('rewrite!!!');

        return $this->actionFactory->create(\Magento\Framework\App\Action\Forward::class);
    }
}
