<?php

namespace Magenest\Movie\Controller\Adminhtml\Index;

use Magenest\Movie\Model\Movie;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

class Save extends Action
{
    public function __construct(Context $context, Movie $movie)
    {
        $this->movie = $movie;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $model = $this->movie->setData($data['contact']);

        try {
            $model->save();
            $this->messageManager->addSuccessMessage('Movie created!');
        } catch (\Exception $exception){
            $this->messageManager->addErrorMessage('Failed to create a movie');
        }

        $this->_eventManager->dispatch('edit_movie_after_save', ['movie' => $model]);

        return $this->_redirect('movie/index');
    }
}
