<?php

namespace Magenest\Movie\Controller\Adminhtml\Sales;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Model\ResourceModel\Order\Collection as Order;
use Magento\Sales\Model\ResourceModel\Order\Item\Collection;

class Export extends Action
{
    public function __construct(
        Context $context,
        Order $order,
        Collection $item,
        PageFactory $pageFactory
    ) {
        $this->order = $order;
        $this->item = $item;
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
//        dd($this->item->getData());

        return $this->pageFactory->create();
    }
}
