<?php

namespace Magenest\Movie\Controller\Adminhtml\Actor;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
    public function execute()
    {
        $view = $this->resultPageFactory->create();
        $view->setActiveMenu('Magenest_Movie::actor_grid');
        $view->addBreadcrumb(__('Movie'), __('Movie'));
        $view->addBreadcrumb(__('Actor'), __('Actor'));
        $view->getConfig()->getTitle()->prepend(__('Actor'));

        return $view;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Movie::actor_grid');
    }
}
