<?php

namespace Magenest\Movie\Controller\Adminhtml\Banner;

use Magenest\Movie\Model\Banner;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

class Save extends Action
{
    public function __construct(Context $context, Banner $banner)
    {
        $this->banner = $banner;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue()['banner_form'];

        $image_info = $data['upload_image'][0];
        $data['upload_image'] = 'http://' . $image_info['cookie']['domain'] . '/media/banner/tmp/' . $image_info['file'];

        try {
            $model = $this->banner->setData($data);
            $model->save();
            $this->messageManager->addSuccessMessage('Banner created!');
        } catch (\Exception $exception){
            $this->messageManager->addErrorMessage('Failed to create a banner');
        }

        return $this->_redirect('movie/banner');
    }
}
