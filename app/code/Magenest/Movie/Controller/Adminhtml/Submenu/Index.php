<?php

namespace Magenest\Movie\Controller\Adminhtml\Submenu;

use Magenest\Movie\Model\Module;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Module\FullModuleList;
use Magento\Framework\Module\ModuleList;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\View\Result\PageFactory;
use phpDocumentor\Reflection\Types\This;

class Index extends Action
{
    public function __construct(Context $context, PageFactory $pageFactory, Reader $fullModuleList, Module $module)
    {
        $this->pageFactory = $pageFactory;
        $this->moduleList = $fullModuleList;
        $this->module = $module;
        parent::__construct($context);
    }

    public function execute()
    {
//        $this->module->getModules('app/code');

        return $this->pageFactory->create();
    }
}
