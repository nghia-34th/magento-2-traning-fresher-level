<?php

namespace Magenest\Movie\Controller\Adminhtml\Blog;

use Magenest\Movie\Model\ResourceModel\Blog\Collection;
use Magenest\Movie\Model\Blog;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

class Save extends Action
{
    public function __construct(Context $context, Collection $collection_blog, Blog $blog)
    {
        $this->collection = $collection_blog;
        $this->blog = $blog;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue()['blog_form'];
        $exist = $this->collection->addFieldToFilter('url_rewrite', $data['url_rewrite'])->getData();

        if ($exist) {
            $this->messageManager->addErrorMessage('url_rewrite key already exist.');
            return $this->_redirect('movie/blog');
        }

        try {
            $model = $this->blog->setData($data);
            $model->save();
            $this->messageManager->addSuccessMessage('Movie created!');
        } catch (\Exception $exception){
            $this->messageManager->addErrorMessage('Failed to create a blog');
        }

        return $this->_redirect('movie/blog');
    }
}
