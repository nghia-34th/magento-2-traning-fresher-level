<?php

namespace Magenest\Movie\Controller\Index;
use \Magento\Framework\App\Action\Action;

class Redirect extends Action
{

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $this->_redirect('movie');
    }
}
