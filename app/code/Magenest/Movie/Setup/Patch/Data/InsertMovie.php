<?php

namespace Magenest\Movie\Setup\Patch\Data;
use Magenest\Movie\Model\Movie;
use \Magento\Framework\Setup\Patch\DataPatchInterface;

class InsertMovie implements DataPatchInterface
{
    public function __construct(Movie $movie)
    {
        $this->movie = $movie;
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {
        $data = [
            [
                'name' => 'Transformer',
                'description' => 'A transformer movie',
                'rating' => 10,
                'director_id' => 1
            ],
            [
                'name' => 'Rowan Atkinson',
                'description' => 'this is a description!',
                'rating' => 6,
                'director_id' => 5
            ],
            [
                'name' => 'Tom Cruise',
                'description' => 'Best sitcom ever',
                'rating' => 100,
                'director_id' => 3
            ]
        ];

        foreach ($data as $datum) {
            $this->movie->setData($datum)->save();
        }
    }
}
