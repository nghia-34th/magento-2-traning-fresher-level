<?php

namespace Magenest\Movie\Setup\Patch\Data;
use Magenest\Movie\Model\MovieActor;
use \Magento\Framework\Setup\Patch\DataPatchInterface;

class InsertMovieActor implements DataPatchInterface
{
    public function __construct(MovieActor $movie_actor)
    {
        $this->movie_actor = $movie_actor;
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {
        $data = [
            [
                'movie_id' => 1,
                'actor_id' => 1
            ],
            [
                'movie_id' => 1,
                'actor_id' => 4
            ],
            [
                'movie_id' => 1,
                'actor_id' => 5
            ],
            [
                'movie_id' => 1,
                'actor_id' => 6
            ],
            [
                'movie_id' => 1,
                'actor_id' => 7
            ],
            [
                'movie_id' => 2,
                'actor_id' => 3
            ],
            [
                'movie_id' => 2,
                'actor_id' => 2
            ],
            [
                'movie_id' => 2,
                'actor_id' => 10
            ],
            [
                'movie_id' => 3,
                'actor_id' => 4
            ],
            [
                'movie_id' => 3,
                'actor_id' => 8
            ],
            [
                'movie_id' => 3,
                'actor_id' => 9
            ],
        ];

        foreach ($data as $datum) {
            $this->movie_actor->setData($datum)->save();
        }
    }
}
