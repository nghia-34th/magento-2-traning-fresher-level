<?php

namespace Magenest\Movie\Setup\Patch\Data;

use Psr\Log\LoggerInterface;
use Magento\Customer\Model\ResourceModel\Attribute as AttributeResource;
use Magento\Customer\Setup\CustomerSetup;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class VnRegion implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var CustomerSetup
     */
    private $customerSetup;

    /**
     * @var AttributeResource
     */
    private $attributeResource;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Constructor
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeResource $attributeResource
     * @param LoggerInterface $logger
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerSetupFactory $customerSetupFactory,
        AttributeResource $attributeResource,
        LoggerInterface $logger
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customerSetup = $customerSetupFactory->create(['setup' => $moduleDataSetup]);
        $this->attributeResource = $attributeResource;
        $this->logger = $logger;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    public function apply()
    {
        // Remove customer attribute if already created
        $this->customerSetup->removeAttribute('customer_address', 'vn_region');

        // Add customer attribute with settings
        $this->customerSetup->addAttribute(
            'customer_address',
            'vn_region',
            [
                'label' => 'Region (VN)',
                'required' => 0,
                'position' => 200,
                'type' => 'int',
                'input' => 'select',
                'source' => 'Magenest\Movie\Model\Config\Source\VnRegion',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'system' => 0,
                'user_defined' => 1,
                'is_used_in_grid' => 0,
                'is_visible_in_grid' => 0,
                'is_filterable_in_grid' => 0,
                'is_searchable_in_grid' => 0,
            ]
        );

        // Get the newly created attribute's model
        $attribute = $this->customerSetup->getEavConfig()->getAttribute('customer_address', 'vn_region');

        // Make attribute visible in Admin customer form
        $attribute->setData('used_in_forms', [
            'customer_address_edit',
            'customer_register_address'
        ]);

        $this->attributeResource->save($attribute);
    }
}
