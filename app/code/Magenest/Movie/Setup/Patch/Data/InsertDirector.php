<?php

namespace Magenest\Movie\Setup\Patch\Data;

use Magenest\Movie\Model\Director;
use \Magento\Framework\Setup\Patch\DataPatchInterface;

class InsertDirector implements DataPatchInterface
{
    public function __construct(Director $director)
    {
        $this->director = $director;
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {
        $data = [
            ['name' => 'Michael Bay'],
            ['name' => 'Keanu Reeves'],
            ['name' => 'Travis Knight'],
            ['name' => 'Steve Harvey'],
            ['name' => 'Tim Burton']
        ];

        foreach ($data as $datum) {
            $this->director->setData($datum)->save();
        }
    }
}
