<?php

namespace Magenest\Movie\Setup\Patch\Data;

use Magenest\Movie\Model\Actor;
use \Magento\Framework\Setup\Patch\DataPatchInterface;

class InsertActor implements DataPatchInterface
{
    public function __construct(Actor $actor)
    {
        $this->actor = $actor;
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {
        $data = [
            ['name' => 'Robert Jr'],
            ['name' => 'Rowan Atkinson'],
            ['name' => 'Jenna Fischer'],
            ['name' => 'Dwayne Johnson'],
            ['name' => 'Samuel Jackson'],
            ['name' => 'Tom Hanks'],
            ['name' => 'Will Smith'],
            ['name' => 'Johnny Depp'],
            ['name' => 'Morgan Freeman'],
            ['name' => 'Tom Cruise']
        ];

        foreach ($data as $datum) {
            $this->actor->setData($datum)->save();
        }
    }
}
