<?php

namespace Magenest\Movie\Model\Resolver;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magenest\Movie\Model\ResourceModel\Movie\CollectionFactory as MovieFactory;

class Movie implements ResolverInterface
{
    public function __construct(MovieFactory $movieFactory)
    {
        $this->movie = $movieFactory;
    }

    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        try {
            $data = $this->getAllMovies();
            return $data;
        } catch (NoSuchEntityException $exception) {
            throw new GraphQlNoSuchEntityException(__($exception->getMessage()));
        } catch (LocalizedException $exception) {
            throw new GraphQlNoSuchEntityException(__($exception->getMessage()));
        }
    }

    private function getAllMovies()
    {
        $movie = $this->movie->create();
        return $movie->getData();
    }
}
