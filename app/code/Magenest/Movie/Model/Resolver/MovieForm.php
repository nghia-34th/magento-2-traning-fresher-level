<?php

namespace Magenest\Movie\Model\Resolver;

use Magenest\Movie\Model\Movie;
use Magenest\Movie\Model\ResourceModel\Director\CollectionFactory as Director;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class MovieForm implements ResolverInterface
{
    public function __construct(Movie $movie, Director $director)
    {
        $this->movie = $movie;
        $this->director = $director;
    }
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if ($data = $this->saveMovie($args['input'])) {
            $data['message'] = 'Movie successfully created.';
            return $data;
        } else {
            return ['message' => 'Failed to create a movie.'];
        }
    }

    private function saveMovie($data)
    {
        $collection = $this->director->create();
        $director = $collection->addFieldToFilter('name', $data['director_name'])->getData();
        if (count($director) != 0) {
            $data['director_id'] = $director[0]['director_id'];
            unset($data['director_name']);
            $movie = $this->movie->setData($data);
            $movie->save();

            return $movie->getData();
        }
    }
}
