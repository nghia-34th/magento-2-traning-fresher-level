<?php

namespace Magenest\Movie\Model;

use Magenest\Movie\Model\ResourceModel\Banner as ResourceModel;
use Magento\Framework\Model\AbstractModel;

class Banner extends AbstractModel
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'magenest_banner_model';

    /**
     * Initialize magento model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
