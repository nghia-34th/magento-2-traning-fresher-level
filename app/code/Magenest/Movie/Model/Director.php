<?php

namespace Magenest\Movie\Model;

class Director extends \Magento\Framework\Model\AbstractModel
{
    public function _construct()
    {
        $this->_init(\Magenest\Movie\Model\ResourceModel\Director::class);
    }
}
