<?php

namespace Magenest\Movie\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class VnRegion implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'value' => null,
                'label' => __('--Please Select--')
            ],
            [
                'value' => 1,
                'label' => __('North')
            ],
            [
                'value' => 2,
                'label' => __('Central')
            ],
            [
                'value' => 3,
                'label' => __('South')
            ]
        ];
    }
}
