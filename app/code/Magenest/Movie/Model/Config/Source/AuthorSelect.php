<?php

namespace Magenest\Movie\Model\Config\Source;

use Magento\User\Model\ResourceModel\User\Collection;

class AuthorSelect implements \Magento\Framework\Option\ArrayInterface
{
    public function __construct(Collection $author){
        $this->author = $author;
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        $collection = $this->author->addFieldToSelect('*');
        $options = [];

        foreach ($collection as $value) {
            $options[] = [
                'label' => $value->getFirstname() . ' ' . $value->getLastname(),
                'value' => $value->getUserId()
            ];
        }

        return $options;
    }
}
