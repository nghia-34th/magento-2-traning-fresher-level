<?php

namespace Magenest\Movie\Model\Config\Source;


class Status implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => "Enable",
                'value' => 1
            ],
            [
                'label' => "Disable",
                'value' => 0
            ]
        ];
    }
}
