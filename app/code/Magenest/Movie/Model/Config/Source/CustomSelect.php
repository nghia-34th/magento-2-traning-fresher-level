<?php

namespace Magenest\Movie\Model\Config\Source;

class CustomSelect implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => "show",
                'value' => 1
            ],
            [
                'label' => "not-show",
                'value' => 2
            ]
        ];
    }
}
