<?php

namespace Magenest\Movie\Model\Config\Source;

use Magenest\Movie\Model\ResourceModel\Director\Collection;
use Magento\Framework\Data\OptionSourceInterface;

class DirectorSelect implements \Magento\Framework\Option\ArrayInterface
{
    public function __construct(Collection $director){
        $this->director = $director;
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        $collection = $this->director->addFieldToSelect('*');
        $options = [];

        foreach ($collection as $value) {
            $options[] = [
                'label' => $value->getName(),
                'value' => $value->getId()
            ];
        }

        return $options;
    }
}
