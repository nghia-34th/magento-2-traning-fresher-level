<?php

namespace Magenest\Movie\Model;

use Magenest\Movie\Model\ResourceModel\Category as ResourceModel;
use Magento\Framework\Model\AbstractModel;

class Category extends AbstractModel
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'magenerst_category_model';

    /**
     * Initialize magento model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
