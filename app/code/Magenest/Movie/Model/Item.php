<?php

namespace Magenest\Movie\Model;

class Item extends \Magento\Framework\Model\AbstractModel
{
    public function _construct()
    {
        $this->_init(\Magenest\Movie\Model\ResourceModel\Item::class);
    }
}
