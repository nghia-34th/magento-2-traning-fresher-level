<?php

namespace Magenest\Movie\Model;

use Magenest\Movie\Model\ResourceModel\BlogCategory as ResourceModel;
use Magento\Framework\Model\AbstractModel;

class BlogCategory extends AbstractModel
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'magenest_blog_category_model';

    /**
     * Initialize magento model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
