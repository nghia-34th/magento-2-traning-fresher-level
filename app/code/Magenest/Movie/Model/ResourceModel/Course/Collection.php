<?php

namespace Magenest\Movie\Model\ResourceModel\Course;

use Magenest\Movie\Model\Course as Model;
use Magenest\Movie\Model\ResourceModel\Course as ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'course_documents_collection';

    /**
     * Initialize collection model.
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}
