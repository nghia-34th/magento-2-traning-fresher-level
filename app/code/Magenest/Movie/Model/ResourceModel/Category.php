<?php

namespace Magenest\Movie\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Category extends AbstractDb
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'magenerst_category_resource_model';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('magenerst_category', 'id');
        $this->_useIsObjectNew = true;
    }
}
