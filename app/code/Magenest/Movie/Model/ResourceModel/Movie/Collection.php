<?php

namespace Magenest\Movie\Model\ResourceModel\Movie;

use Magenest\Movie\Model\Movie;
use Magenest\Movie\Model\ResourceModel\Movie as ResourceMovie;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    public function _construct() {
        $this->_init(Movie::class, ResourceMovie::class);
    }

    protected function _initSelect()
    {
        $this->getSelect()->joinLeft( 'magenest_director',
            'main_table.director_id = magenest_director.director_id',
            ['main_table.name as movie_name', 'magenest_director.name as director_name']
        );

        return parent::_initSelect();
    }
}
