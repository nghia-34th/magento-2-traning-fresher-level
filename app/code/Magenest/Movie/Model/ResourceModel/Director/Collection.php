<?php

namespace Magenest\Movie\Model\ResourceModel\Director;

use Magenest\Movie\Model\Director;
use Magenest\Movie\Model\ResourceModel\Director as ResourceDirector;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    public function _construct() {
        $this->_init(Director::class, ResourceDirector::class);
    }
}
