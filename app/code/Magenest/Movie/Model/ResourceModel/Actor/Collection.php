<?php

namespace Magenest\Movie\Model\ResourceModel\Actor;

use Magenest\Movie\Model\Actor;
use Magenest\Movie\Model\ResourceModel\Actor as ResourceActor;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    public function _construct() {
        $this->_init(Actor::class, ResourceActor::class);
    }
}
