<?php

namespace Magenest\Movie\Model\ResourceModel\BlogCategory;

use Magenest\Movie\Model\BlogCategory as Model;
use Magenest\Movie\Model\ResourceModel\BlogCategory as ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'magenest_blog_category_collection';

    /**
     * Initialize collection model.
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}
