<?php

namespace Magenest\Movie\Model\ResourceModel\Category;

use Magenest\Movie\Model\Category as Model;
use Magenest\Movie\Model\ResourceModel\Category as ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'magenerst_category_collection';

    /**
     * Initialize collection model.
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}
