<?php

namespace Magenest\Movie\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Course extends AbstractDb
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'course_documents_resource_model';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('course_documents', 'id');
        $this->_useIsObjectNew = true;
    }
}
