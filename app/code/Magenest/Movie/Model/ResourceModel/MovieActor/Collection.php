<?php

namespace Magenest\Movie\Model\ResourceModel\MovieActor;

use Magenest\Movie\Model\MovieActor;
use Magenest\Movie\Model\ResourceModel\MovieActor as ResourceMovieActor;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    public function _construct()
    {
        $this->_init(MovieActor::class,ResourceMovieActor::class);
    }

    public function _initSelect()
    {
        $this->getSelect()->joinLeft(
            ['movie' => $this->getTable('magenest_movie')],
            'main_table.movie_id = movie.movie_id'
        )->joinLeft(
            ['actor' => $this->getTable('magenest_actor')],
            'main_table.actor_id = actor.actor_id',
            'actor.name as actor_name'
        );

        return parent::_initSelect();
    }
}
