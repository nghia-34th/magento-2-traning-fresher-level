<?php

namespace Magenest\Movie\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Blog extends AbstractDb
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'magenest_blog_resource_model';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('magenest_blog', 'id');
        $this->_useIsObjectNew = true;
    }
}
