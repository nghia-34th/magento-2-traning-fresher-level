<?php

namespace Magenest\Movie\Model;

use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Module\ModuleList;

class Module
{
    public function __construct(ModuleList $moduleList, Reader $moduleReader)
    {
        $this->moduleList = $moduleList;
        $this->moduleReader = $moduleReader;
    }

    public function getModules($from = null)
    {
        $result = [];
        $modules = $this->moduleList->getNames();
        foreach ($modules as $module) {
            $dir = $this->moduleReader->getModuleDir(null, $module);
            if ($from != null) {
                if (strpos($dir, $from)) {
                    $result[] = $module;
                }
            } else {
                $result[] = $module;
            }
        }
        return $result;
    }
}
