<?php

namespace Magenest\Movie\Model;

use Magenest\Movie\Model\ResourceModel\Course as ResourceModel;
use Magento\Framework\Model\AbstractModel;

class Course extends AbstractModel
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'course_documents_model';

    /**
     * Initialize magento model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
