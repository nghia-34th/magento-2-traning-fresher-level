<?php

namespace Magenest\Movie\Block\Adminhtml;

use Magenest\Movie\Model\Module;
use Magento\Backend\Block\Template;

class DisplayModule extends Template
{
    public function __construct(Template\Context $context, Module $module, array $data = [])
    {
        $this->module = $module;
        parent::__construct($context, $data);
    }

    public function getModules()
    {
        $modules = [
            'magento' => count($this->module->getModules('vendor')),
            'non_magento' => count($this->module->getModules('app/code'))
        ];

        return $modules;
    }
}
