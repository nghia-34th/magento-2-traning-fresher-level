<?php

namespace Magenest\Movie\Block\Adminhtml;

use Magento\Backend\Block\Template;
use Magento\Customer\Model\ResourceModel\Customer\Collection as Customer;
use Magento\Catalog\Model\ResourceModel\Product\Collection as Product;
use Magento\Sales\Model\ResourceModel\Order\Collection as Order;
use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection as Invoice;
use Magento\Sales\Model\ResourceModel\Order\Creditmemo\Collection as Creditmemo;

class StoreDetail extends Template
{
    public function __construct(
        Template\Context $context,
        Customer $customer,
        Product $product,
        Order $order,
        Invoice $invoice,
        Creditmemo $creditmemo,
        array $data = []
    ) {
        $this->customer = $customer;
        $this->product = $product;
        $this->order = $order;
        $this->invoice = $invoice;
        $this->creditmemo = $creditmemo;
        parent::__construct($context, $data);
    }

    public function getDetail()
    {
        $data = [
            'customer' => $this->customer->getSize(),
            'product' => $this->product->getSize(),
            'order' => $this->order->getSize(),
            'invoice' => $this->invoice->getSize(),
            'creditmemo' => $this->creditmemo->getSize()
        ];

        return $data;
    }
}
