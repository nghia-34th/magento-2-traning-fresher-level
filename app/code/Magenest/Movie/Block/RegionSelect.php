<?php

namespace Magenest\Movie\Block;

use Magenest\Movie\Model\Config\Source\VnRegion;
use Magento\Customer\Block\Address\Edit;

class RegionSelect extends Edit
{

    /**
     * Returns country html select
     *
     * @param null|string $defValue
     * @param string $name
     * @param string $id
     * @param string $title
     * @return string
     */
    public function getVnRegionHtmlSelect($defValue = '', $name = 'vietnam_region', $id = 'vietnam_region', $title = 'VN Region')
    {
        \Magento\Framework\Profiler::start('TEST: ' . __METHOD__, ['group' => 'TEST', 'method' => __METHOD__]);
        $this->region = new VnRegion();
        $options = $this->region->toOptionArray();
        $html = $this->getLayout()->createBlock(
            \Magento\Framework\View\Element\Html\Select::class
        )->setName(
            $name
        )->setId(
            $id
        )->setTitle(
            $this->escapeHtmlAttr(__($title))
        )->setValue(
            $defValue
        )->setOptions(
            $options
        )->setExtraParams(
            'data-validate="{\'validate-select\':true}"'
        )->getHtml();

        \Magento\Framework\Profiler::stop('TEST: ' . __METHOD__);
        return $html;
    }
}
