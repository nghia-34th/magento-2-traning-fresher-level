<?php

namespace Magenest\Movie\Block;
use Magenest\Movie\Model\ResourceModel\Movie\CollectionFactory as MovieCollection;
use Magenest\Movie\Model\ResourceModel\MovieActor\CollectionFactory as MovieActorCollection;
use Magento\Framework\View\Element\Template;

class DisplayMovie extends Template
{
    private $_movieCollectionFactory;
    private $_movieActorCollectionFactory;

    public function __construct(
        Template\Context $context,
        MovieCollection $movieCollectionFactory,
        MovieActorCollection $movieActorCollectionFactory,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->_movieCollectionFactory = $movieCollectionFactory;
        $this->_movieActorCollectionFactory = $movieActorCollectionFactory;
    }

    public function getMovies(){
        $movie = $this->_movieCollectionFactory->create();

        return $movie;
    }

    public function getCast(){
        $cast = $this->_movieActorCollectionFactory->create();

        return $cast;
    }

    public function getLandingsUrl()
    {
        return $this->getUrl('movie');
    }

    public function getRedirectUrl()
    {
        return $this->getUrl('movie/index/redirect');
    }
}
