<?php

namespace Magenest\Movie\Block;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface as Config;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface as StoreManager;

class Promotion extends Template
{
    public function __construct(
        Context $context,
        StoreManager $storeManager,
        Config $config,
        Session $session,
        array $data = [])
    {
        $this->_storeManager = $storeManager;
        $this->config = $config;
        $this->session = $session;
        parent::__construct($context, $data);
    }

    public function getCurrentGroup()
    {
        return $this->session->getCustomerGroupId();
    }

    public function getPromotionInfo()
    {
        $promo = $this->config->getValue('front/promotion');
        $promo['group'] = explode(',', $promo['group']);
        $promo['image'] = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'test' . '/' . $promo['image'];
        return $promo;
    }
}
