<?php

namespace Magenest\Movie\Block\System\Config;

use Magenest\Movie\Model\ResourceModel\Movie\Collection;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Block\Template\Context;

class Movie extends Field
{
    public function __construct(Context $context, Collection $movie, array $data = []) {
        parent::__construct($context, $data);
        $this->movie = $movie;
    }

    /**
     * Get the button and scripts contents
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $element->setValue($this->movie->getSize());
        $element->setReadonly('true');
        $html = $element->getElementHtml();

        return $html;
    }
}
