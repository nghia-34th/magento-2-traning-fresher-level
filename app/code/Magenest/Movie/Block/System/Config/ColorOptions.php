<?php

namespace Magenest\Movie\Block\System\Config;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magenest\Movie\Block\Adminhtml\Form\Field\CustomColumn;
use Magento\Framework\View\Element\AbstractBlock;

class ColorOptions extends AbstractFieldArray
{
    private $colorRenderer;
    /**
     * Prepare rendering the new field by adding all the needed columns
     */
    protected function _prepareToRender()
    {
        $this->addColumn('color_name',
            [
                'label' => __('Color'),
                'class' => 'required-entry'
            ]);
        $this->addColumn('color_code',
            [
                'label' => __('Color Code'),
                'class' => 'required-entry',
                'renderer' => $this->getColorRenderer(),
            ]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    protected function getColorRenderer()
    {
        if (!$this->colorRenderer) {
            $this->colorRenderer = $this->getLayout()->createBlock(
                ColorRenderer::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->colorRenderer;
    }
}
