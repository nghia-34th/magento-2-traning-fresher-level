<?php

namespace Magenest\Movie\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magenest\Movie\Model\ResourceModel\Banner\Collection;

class Banner extends Template implements BlockInterface
{
    protected $_template = 'widget/banner.phtml';

    public function __construct(
        Template\Context $context,
        Collection $banner,
        array $data = [])
    {
        $this->banner = $banner;
        parent::__construct($context, $data);
    }

    public function getBannerInfo()
    {
        return $this->banner->getData();
    }
}
