<?php

namespace Magenest\Movie\Block;

use Magento\Framework\App\Config\ScopeConfigInterface as Config;
use Magento\Framework\View\Element\Template;

class BackgroundSwitcher extends Template
{
    public function __construct(
        Template\Context $context,
        Config $config,
        array $data = [])
    {
        $this->config = $config;
        parent::__construct($context, $data);
    }

    public function getAvailableColors()
    {
        return json_decode($this->config->getValue('front/colors/option'), true);
    }
}
